//attempt to combine voltage reader + robot movement

#include <stdio.h> 
#include <at89lp51rd2.h>
#include <stdlib.h>
#include <math.h>

// ~C51~  
#define CLK 22118400L
#define BAUD 115200L
#define BRG_VAL (0x100-(CLK/(32L*BAUD)))
#define FREQ 100000L
//#define TIMER0_RELOAD_VALUE (65536L-(CLK/(12L*FREQ)))
#define TIMER0_RELOAD_VALUE 0

//These variables are used in the ISR
volatile unsigned char pwmcount;
volatile int pwm1;
volatile int pwm2;
volatile int pwm3;
volatile int pwm4;
volatile float tentativepwm1;
volatile float tentativepwm2;
volatile float tentativepwm3;
volatile float tentativepwm4;
volatile float distance;
volatile float distance1;
volatile float distance2;

volatile int i;


volatile int direction; // boolean variable for direction, 0 for CW, 1 for CCW
volatile int orientation; //turn 180 forward and backward
volatile int speed = 1;
volatile float offsetspeed; //offset for 0<speed<5
volatile float rpm;

void Turn180(float speed);
void waithalfus(void);
void follow(void);
void GetDistances(void);

float convertToD(float max);
float convertToDAMP(float max);

void waithalfus(void){
    _asm    
        ;For a 22.1184MHz crystal one machine cycle takes 12/22.1184MHz=0.5425347us
        ;mov TR0, #1
        ;mov R7, #9
        ;mov TR0, #0
        push AR1
        mov R1, #1
        pop AR1
        ret
    _endasm;
}

unsigned char _c51_external_startup(void)
{
	// Configure ports as a bidirectional with internal pull-ups.
	P0M0=0;	P0M1=0;
	P1M0=0;	P1M1=0;
	P2M0=0;	P2M1=0;
	P3M0=0;	P3M1=0;
	AUXR=0B_0001_0001; // 1152 bytes of internal XDATA, P4.4 is a general purpose I/O
	P4M0=0;	P4M1=0;
    
    // Instead of using a timer to generate the clock for the serial
    // port, use the built-in baud rate generator.
    
    PCON|=0x80;
	SCON = 0x52;
    BDRCON=0;
    BRL=BRG_VAL;
    BDRCON=BRR|TBCK|RBCK|SPD;
    
    
    // Initialize timer 0 for ISR 'pwmcounter()' below
	TR0=0; // Stop timer 0
	TMOD=0x01; // 16-bit timer
	// Use the autoreload feature available in the AT89LP51RB2
	// WARNING: There was an error in at89lp51rd2.h that prevents the
	// autoreload feature to work.  Please download a newer at89lp51rd2.h
	// file and copy it to the crosside\call51\include folder.
	TH0=RH0=TIMER0_RELOAD_VALUE/0x100;
	TL0=RL0=TIMER0_RELOAD_VALUE%0x100;
	TR0=1; // Start timer 0 (bit 4 in TCON)
	ET0=1; // Enable timer 0 interrupt
	EA=1;  // Enable global interrupts
    
    
    pwmcount=0;




   

    
    return 0;
}
/*
// Interrupt 1 is for timer 0.  This function is executed every time
// timer 0 overflows: 100 us.
void pwmcounter (void) interrupt 1
{
	if(++pwmcount>99) 
		pwmcount=0;
	P1_0=(pwm1>pwmcount)?1:0;
	P1_1=(pwm2>pwmcount)?1:0;
	P1_2=(pwm3>pwmcount)?1:0;
	P1_3=(pwm4>pwmcount)?1:0;
}*/

void SPIWrite(unsigned char value)
{
	SPSTA&=(~SPIF); // Clear the SPIF flag in SPSTA
	SPDAT=value;
	while((SPSTA & SPIF)!=SPIF); //Wait for transmission to end
}

unsigned int GetADC(unsigned char channel)
{
	unsigned int adc;

	// initialize the SPI port to read the MCP3004 ADC attached to it.
	SPCON&=(~SPEN); // Disable SPI
	SPCON=MSTR|CPOL|CPHA|SPR1|SPR0|SSDIS;
	SPCON|=SPEN; // Enable SPI
	
	P1_4=0; // Activate the MCP3004 ADC.
	SPIWrite(channel|0x18);	// Send start bit, single/diff* bit, D2, D1, and D0 bits.
	for(adc=0; adc<10; adc++); // Wait for S/H to setup
	SPIWrite(0x55); // Read bits 9 down to 4
	adc=((SPDAT&0x3f)*0x100);
	SPIWrite(0x55);// Read bits 3 down to 0
	P1_4=1; // Deactivate the MCP3004 ADC.
	adc+=(SPDAT&0xf0); // SPDR contains the low part of the result. 
	adc>>=4;
		
	return adc;
}

//         LP51B    MCP3004
//---------------------------
// MISO  -  P1.5  - pin 10
// SCK   -  P1.6  - pin 11
// MOSI  -  P1.7  - pin 9
// CE*   -  P1.4  - pin 8
// 4.8V  -  VCC   - pins 13, 14
// 0V    -  GND   - pins 7, 12
// CH0   -        - pin 1
// CH1   -        - pin 2
// CH2   -        - pin 3
// CH3   -        - pin 4


void maxvoltage ( float *par, int i )
{
    if ( (GetADC(i)*4.95)/1023.0 > *par)
        *par = (GetADC(i)*4.95)/1023.0;
    return;
}

void GetDistances(void){
    int i;
	int counter1 = 0, counter2 = 0;
	float temp1 = 0.001, temp2 = 0.001;
	
	//Initialization
	float max1 = 0.0, max2 = 0.0;

	//give adc time to register and find peak V
	for(i = 0; i < 1200; i++)
	{
		maxvoltage(&max1, 0);
		maxvoltage(&max2, 1);
	}
		
		//checks need for amplificiation for v1, and does so
	if (max1 < 0.2)
	{
		for(i = 0; i < 1200; i++)
		{
			maxvoltage(&max1, 2);
		}
		max1 = max1/5.0;
		distance1 = 6.9001*(1/max1);
       //	distance1 = convertToD(max1);
	}

	if (max1 <0.001)
	{
		max1 = temp1;
       	distance1 = 9.3*6.9001*(1/max1);
		counter1++;
	}
	
	//checks need for amplificiation for v2, and does so
	if (max2 < 0.2)
	{
		for(i = 0; i < 1200; i++)
		{
			maxvoltage(&max2, 3);
		}
		max2 = max2/3.0;
        distance2 = 6.9001*(1/max2);
	}
	
	if (max2 < 0.001)
	{
		max2 = temp2;
        distance2 = 9.3*6.9001*(1/max2);
		counter2++;
	}
	
		temp1 = max1;
		temp2 = max2;
	
	if (counter1 = counter2 && counter1 > 2)
	{	
		temp1 = 0.0;
		temp2 = 0.0;
	}
	
		printf("Peak V1 = %4.3f ", max1);
		printf("Peak V2 = %4.3f ", max2);
		printf("Distance1 = %4.3f  ", distance1);
		printf("Distance2 = %4.3f ", distance2);
		printf("\n");
}

/*
float convertToD(float max){
	return 6.9001*(1/max);
}
*/
/*
float convertToDAMP(float max){
	return 64.171*(1/max);
}
*/
/*
//Robot logic lecture 1 page 21
void follow(void) {
    if (distance1 - distance  1) {
        //Turning 45 degrees to the right (move motor 1 forward)
        pwm1 = 0;
        pwm2 = 1;
        pwm3 = 0;
        pwm4 = 0;
    }
    if (distance2 - distance < 10) {
        //Turning 45 degrees to the left (move motor 2 forward)
        pwm1 = 0;
        pwm2 = 0;
        pwm3 = 1;
        pwm4 = 0;
    }
    if (distance1 - distance > 10) {
        //Turning 45 degrees to the left (move motor 1 backward)
        pwm1 = 1;
        pwm2 = 0;
        pwm3 = 0;
        pwm4 = 0;
    }
    if (distance2 - distance > 10) {
        //Turning 45 degrees to the right (move motor 2 backward)
        pwm1 = 0;
        pwm2 = 0;
        pwm3 = 0;
        pwm4 = 1;
    }
    
    P1_0 = pwm1;
	P1_1 = pwm2;
	P1_2 = pwm3;
	P1_3 = pwm4;

}*/


void follow(void) {
if (orientation == FORWARD) {
	    if ((distance1-10 > distance) && (distance1 > distance2)) {
	        //Turning 45 degrees to the right (move motor 1 forward)
	        pwm1 = 0;
	        pwm2 = 1;
	        pwm3 = 0;
	        pwm4 = 0;
	    }
	    if ((distance2-10 > distance) && (distance2 > distance1)) {
	        //Turning 45 degrees to the left (move motor 2 forward)
	        pwm1 = 0;
	        pwm2 = 0;
	        pwm3 = 1;
	        pwm4 = 0;
	    }
	    if ((distance1+10 < distance) && (distance1 < distance2)) {
	        //Turning 45 degrees to the left (move motor 1 backward)
	        pwm1 = 1;
	        pwm2 = 0;
	        pwm3 = 0;
	        pwm4 = 0;
	    }
	    if ((distance2+10 < distance) && (distance2 < distance1)) {
	        //Turning 45 degrees to the right (move motor 2 backward)
	        pwm1 = 0;
	        pwm2 = 0;
	        pwm3 = 0;
	        pwm4 = 1;
	    }
	    
	    //try to move forwards
	    
	    if ((distance1+10 > distance) && (distance2+10 > distance) && (distance1 - distance2 < 10 )) {
	        //move forward straight
	        pwm1 = 1;
	        pwm2 = 0;
	        pwm3 = 0;
	        pwm4 = 1;
	    }//try to move forwards
	    
	    if ((distance1+10 < distance) && (distance2+10 < distance) && (distance1 - distance2 < 10 )) {
	        //Turning 45 degrees to the right (move motor 2 backward)
	        pwm1 = 0;
	        pwm2 = 1;
	        pwm3 = 1;
	        pwm4 = 0;
	    }//try to move backwards
	    
	    
	    
	    
}
 else if (orientation == BACKWARD) {
        if ((distance1-10 > distance) && (distance1 > distance2)) {
            //Turning 45 degrees to the left (move motor 2 forward)
            pwm1 = 1;
            pwm2 = 0;
            pwm3 = 0;
            pwm4 = 0;
        }
        else if ((distance2-10 > distance) && (distance2 > distance1)) {
            //Turning 45 degrees to the right(move motor 1 forward)
            pwm1 = 0;
            pwm2 = 0;
            pwm3 = 0;
            pwm4 = 1;
        }
        else if ((distance1+10 < distance) && (distance1 < distance2)) {
            //Turning 45 degrees to the left (move motor 2 backward)
            pwm1 = 0;
            pwm2 = 1;
            pwm3 = 0;
            pwm4 = 0;
        }
        else if ((distance2+10 < distance) && (distance2 < distance1)) {
            //Turning 45 degrees to the right (move motor 2 backward)
            pwm1 = 0;
            pwm2 = 0;
            pwm3 = 1;
            pwm4 = 0;
        }

    }

    // will only stop if distance 1 is not greater than distance with 10 buffer, is not less than distance with 10 buffer
    // and distance 2 is not 
    
    P1_0 = pwm1;
	P1_1 = pwm2;
	P1_2 = pwm3;
	P1_3 = pwm4;

}




void main (void)
{
	distance = 100;
	
    pwm1=0; //at zero until user enters duty cycle
    pwm2=0;
    pwm3=0;
    pwm4=0;    
	
	printf("Hello World! \n");
    /* MOVE FORWARD
    pwm1=0; 
    pwm2=100;
    pwm3=100;
    pwm4=0;   
	*/

    while(1){
    
        //Initialization
		float max1 = 0.0, max2 = 0.0;
		float temp1= 0.001, temp2 = 0.001;
		//printf("Initialization \n ");

		for(i = 0; i < 200; i++){
			//	printf("Hello\n");
				maxvoltage(&max1, 0);
			//	printf("    Hello\n");
				maxvoltage(&max2, 1);
		}
				
		distance1 = 6.9001*(1/max1);
		distance2 = 6.9001*(1/max2);
		
		//printf("Im stuck at line 351 \n");
		printf("Peak V1 = %4.3f ", max1);
		printf("Peak V2 = %4.3f ", max2);
		printf("\n");
		//printf("D1 = %4.3f ", distance1);
		//printf("D2 = %4.3f ", distance2);
		printf("\n");
        follow();
        
        
	    
	    
    }
}    