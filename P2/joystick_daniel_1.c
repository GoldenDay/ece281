#include <stdio.h>
#include <at89lp51rd2.h>
#include <stdlib.h>
#include <math.h>

// ~C51~ 
#define CLK 22118400L
#define BAUD 115200L
#define BRG_VAL (0x100-(CLK/(32L*BAUD)))

#define FREQ 10000L
#define TIMER0_RELOAD_VALUE (65536L-(CLK/(12L*FREQ)))

// Red 		VCC
// Black	Ground
// Grey 	Forward
// Yellow 	Back
// Orange 	Right
// Blue 	Left
// White	L1 / 180
// Green	L2 / p_park

#define P_JF P1_0
#define P_JB P1_1
#define P_JR P1_2
#define P_JL P1_3
#define P_BP P1_4
#define P_B8 P1_5

#define P_OS P1_7

unsigned char _c51_external_startup(void)
{
	// Configure ports as a bidirectional with internal pull-ups.
	P0M0=0;	P0M1=0;
	P1M0=0;	P1M1=0;
	P2M0=0;	P2M1=0;
	P3M0=0;	P3M1=0;
	AUXR=0B_0001_0001; // 1152 bytes of internal XDATA, P4.4 is a general purpose I/O
	P4M0=0;	P4M1=0;
    
    // Initialize the serial port and baud rate generator
    PCON|=0x80;
	SCON = 0x52;
    BDRCON=0;
    BRL=BRG_VAL;
    BDRCON=BRR|TBCK|RBCK|SPD;
    
    return 0;
}

void waithalfus(void){
	_asm
	    push AR1
	    mov R1, #1
	    pop AR1
	    ret
    _endasm;
}

void mov_forward ( void )
{
	while (P_JF == 1)
	{
		P1_0=0;
		P1_1=1;
		P1_2=1;
		P1_3=0;
	}
	return;
}

void mov_backward ( void )
{
	while (P_JB == 1)
	{
		P1_0=1;
		P1_1=0;
		P1_2=0;
		P1_3=1;
	}
	return; 
}

void turn_R ( void )
{
	while (P_JR == 1)
	{
		P1_0=0;
		P1_1=1;
		P1_2=0;
		P1_3=1;
	}
	return;
}

void turn_L ( void )
{
	while (P_JL == 1)
	{
		P1_0=1;
		P1_1=0;
		P1_2=1;
		P1_3=0;
	}
	return; 
}

void turn_180 ( void )
{
	int wait;
	
	P1_0=0;
	P1_1=1;
	P1_2=0;
	P1_3=1;
    
    for (wait = 0; wait < 165; wait++) 
    {
        waithalfus();
    }
    
	return;
}

void p_park_0 ( void )
{
	//Turning 45 degrees to the right
    int wait;
	P1_0=0;
	P1_1=1;
	P1_2=0;
	P1_3=0;

    for (wait = 0; wait < 113; wait++) 
    {
        waithalfus();
    }

    //move forward
	P1_0=0;
	P1_1=1;
	P1_2=1;
	P1_3=0;

    for (wait = 0; wait < 223; wait++) 
    {
        waithalfus();
    }

    //Turning 45 degrees to the left
	P1_0=0;
	P1_1=0;
	P1_2=1;
	P1_3=0;

    for (wait = 0; wait < 110; wait++) 
    {
        waithalfus();
    }

    //move backwards

	P1_0=1;
	P1_1=0;
	P1_2=0;
	P1_3=1;

    for (wait = 0; wait < 125; wait++) 
    {
        waithalfus();
    }

	return;
}

//backward orientation
void p_park_1 ( void )
{
 	//Turning 45 degrees to the right
    int wait;
    
	P1_0=0;
	P1_1=0;
	P1_2=0;
	P1_3=1;
    
    for (wait = 0; wait < 113; wait++) 
    {
        waithalfus();
    }
    
    //move forward
	P1_0=1;
	P1_1=0;
	P1_2=0;
	P1_3=1;
    
    for (wait = 0; wait < 223; wait++) 
    {
        waithalfus();
    }
    
    //Turning 45 degrees to the left
	P1_0=0;
	P1_1=0;
	P1_2=1;
	P1_3=0;
    
    for (wait = 0; wait < 110; wait++) 
    {
        waithalfus();
    }
    
    //move backwards
	P1_0=0;
	P1_1=1;
	P1_2=1;
	P1_3=0;

    for (wait = 0; wait < 125; wait++) 
   	{
        waithalfus();
    }

	return;
}

void refresh ( void )
{
	P1_0=0;
	P1_1=0;
	P1_2=0;
	P1_3=0;
}

void manual_overide ( void )
{	
	int orientation = 0;
	
	while(P_OS != 0)
	{
		if (orientation == 0)
		{
			if (P_BP == 0)
				p_park_0();
			else if (P_B8 == 1)
			{
				turn_180();
				orientation = 1;
			}	
			else if (P_JF == 1)
				mov_forward();
			else if (P_JB == 1)
				mov_backward();
			else if (P_JR == 1)
				turn_R();
			else if (P_JL == 1)
				turn_L();
			else
				refresh();
		}
		
		else
		{
			if (P_BP == 1)
				p_park_1();
			else if (P_B8 == 1)
			{
				turn_180();
				orientation = 0;
			}
			else if (P_JF == 1)
				mov_backward();
			else if (P_JB == 1)
				mov_forward();
			else if (P_JR == 1)
				turn_R();
			else if (P_JL == 1)
				turn_L();
			else
				refresh();
		}
	}
	return;
}

void main ( void )
{
	while(1) 
	{
		manual_overide();
	}
}
