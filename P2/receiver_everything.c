// Combines motor controls with voltage/distance readings from the MCP3004 ADC.
// Includes bit-bang reception.
// SPI functions copied over from lp51b_SPI.c by Dr. Jesus Calvino-Fraga

#include <stdio.h> 
#include <at89lp51rd2.h>
#include <math.h>

// ~C51~  
#define CLK 22118400L
#define BAUD 115200L
#define BRG_VAL (0x100-(CLK/(32L*BAUD)))
// ~constants~
#define BUFFER 0.05
#define LR_BUFFER 0.2
#define FORWARD 1
#define BACKWARD 0
// byte constants for receiver functions
#define CLOSER 25
#define FURTHER 50
#define DIST_MIN 1.5 //minimum distance setting (voltage)
#define DIST_MAX 4.0 //maximum distance setting (voltage)
#define SWITCH 75	//turn 180 degrees
#define PARALELL_PARK 100
#define ITERATIONS 6000 //MUST BE SAME ON TRANSMMITTER

volatile float voltage_R;
volatile float voltage_L;

volatile float distanceV; //voltage at distance robot must keep from transmitter (V)
volatile int orientation;

//Functions
void getVoltage(float *par, int i);
void getMaxVoltages(void);
void getDistances(void);
void follow(void);
void testmove(void);


void wait_bit_time(void);
void wait_one_and_half_bit_time(void);
//
int rx_byte (int min, int channel);
//
void turn_180 ( void );
void p_park ( void );
int waithalfs (void);
void refresh ( void );
void wait_time(int iter);
void wait_check(void);
void waithalfus(void);

unsigned char _c51_external_startup(void)
{
	// Configure ports as a bidirectional with internal pull-ups.
	P0M0=0;	P0M1=0;
	P1M0=0;	P1M1=0;
	P2M0=0;	P2M1=0;
	P3M0=0;	P3M1=0;
	AUXR=0B_0001_0001; // 1152 bytes of internal XDATA, P4.4 is a general purpose I/O
	P4M0=0;	P4M1=0;
    
    // Instead of using a timer to generate the clock for the serial
    // port, use the built-in baud rate generator.
    PCON|=0x80;
    SCON = 0x52;
    BDRCON=0;
    BRL=BRG_VAL;
    BDRCON=BRR|TBCK|RBCK|SPD;
    
    return 0;
}

void SPIWrite(unsigned char value)
{
	SPSTA&=(~SPIF); // Clear the SPIF flag in SPSTA
	SPDAT=value;
	while((SPSTA & SPIF)!=SPIF); //Wait for transmission to end
}

unsigned int GetADC(unsigned char channel)
{
	
	unsigned int adc;

	// initialize the SPI port to read the MCP3004 ADC attached to it.
	SPCON&=(~SPEN); // Disable SPI
	SPCON=MSTR|CPOL|CPHA|SPR1|SPR0|SSDIS;
	SPCON|=SPEN; // Enable SPI
	
	P1_4=0; // Activate the MCP3004 ADC.
	SPIWrite(channel|0x18);	// Send start bit, single/diff* bit, D2, D1, and D0 bits.
	for(adc=0; adc<10; adc++); // Wait for S/H to setup
	SPIWrite(0x55); // Read bits 9 down to 4
	adc=((SPDAT&0x3f)*0x100);
	SPIWrite(0x55);// Read bits 3 down to 0
	P1_4=1; // Deactivate the MCP3004 ADC.
	adc+=(SPDAT&0xf0); // SPDR contains the low part of the result. 
	adc>>=4;
		
	return adc;
}


/*      LP51B    MCP3004
---------------------------
MISO  -  P1.5  - pin 10
SCK   -  P1.6  - pin 11
MOSI  -  P1.7  - pin 9
CE*   -  P1.4  - pin 8
4.8V  -  VCC   - pins 13, 14
0V    -  GND   - pins 7, 12
CH0   -        - pin 1
CH1   -        - pin 2
CH2   -        - pin 3
CH3   -        - pin 4
*/

void getVoltage ( float *par, int i )
{
	*par = (GetADC(i)*4.95)/1023.0;
	return;
}


void getMaxVoltages(void)
{
	getVoltage ( &voltage_R, 1);
	getVoltage ( &voltage_L, 0);
	return;
}


//TODO: measure voltages at preset distances
void getDistances(void)
{
	//distance_R = ( 9.0 / voltage_R );
	//distance_L = ( 9.0 / voltage_L );
	return;
}


//distance_R = distance1, distance_L = distance 2, BUFFER, FORWARD, BACKWARD defined as a constants
void follow(void) 
{
	if (orientation == FORWARD) {
	    if ((voltage_L+BUFFER < distanceV) && (voltage_L+BUFFER < voltage_R)) {
	        //Turning 45 degrees to the right (move motor 1 forward)
	        P1_0 = 0;
            P1_1 = 1;
            P1_2 = 0;
            P1_3 = 0;
	    }
	    if ((voltage_R+BUFFER < distanceV) && (voltage_R+BUFFER < voltage_L)) {
	        //Turning 45 degrees to the left (move motor 2 forward)
	        P1_0 = 0;
            P1_1 = 0;
            P1_2 = 1;
            P1_3 = 0;
	    }
	    if ((voltage_L-BUFFER > distanceV) && (voltage_L > voltage_R+BUFFER-0.015)) {
	        //Turning 45 degrees to the left (move motor 1 backward)
	        P1_0 = 1;
            P1_1 = 0;
            P1_2 = 0;
            P1_3 = 0;
	    }
	    if ((voltage_R-BUFFER > distanceV) && (voltage_R > voltage_L+BUFFER-0.015)) {
	        //Turning 45 degrees to the right (move motor 2 backward)
	        P1_0 = 0;
            P1_1 = 0;
            P1_2 = 0;
            P1_3 = 1;
	    }
	    
	    //try to move forwards
	    if ((voltage_R-BUFFER < distanceV) && (voltage_L-BUFFER < distanceV) && ( (voltage_R - voltage_L < LR_BUFFER ) || (voltage_L - voltage_R < LR_BUFFER ) ) ) {
	        //move forward straight
	        P1_0 = 0;
            P1_1 = 1;
            P1_2 = 1;
            P1_3 = 0;
	    }
	    
	   if ((voltage_R-BUFFER > distanceV) && (voltage_L-BUFFER > distanceV) && ( (voltage_R - voltage_L < LR_BUFFER+0.025 ) || (voltage_L - voltage_R < LR_BUFFER+0.025 ) ) )  {
		  	P1_0 = 1;
			P1_1 = 0;
			P1_2 = 0;
			P1_3 = 1;
		} // move backwards since too close
		
		/*
		if ((voltage_R-distanceV < 0.01) && (voltage_L-distanceV < 0.01) && ( (voltage_R - voltage_L < 0.01 ) || (voltage_L - voltage_R < 0.01 ) ) )  {
		  	P1_0 = 0;
			P1_1 = 0;
			P1_2 = 0;
			P1_3 = 0;
		} // stay stil if voltages are equal to distance and are equal to one another
		*/
		
	    
	    
	}
 	else if (orientation == BACKWARD) {
        if ((voltage_R-BUFFER > distanceV) && (voltage_R > voltage_L)) {
            //Turning 45 degrees to the left (move motor 2 forward)
            P1_0 = 1;
            P1_1 = 0;
            P1_2 = 0;
            P1_3 = 0;
        }
        if ((voltage_L-BUFFER > distanceV) && (voltage_L > voltage_R)) {
            //Turning 45 degrees to the right(move motor 1 forward)
            P1_0 = 0;
            P1_1 = 0;
            P1_2 = 0;
            P1_3 = 1;
        }
        if ((voltage_R+BUFFER < distanceV) && (voltage_R < voltage_L)) {
            //Turning 45 degrees to the left (move motor 2 backward)
            P1_0 = 0;
            P1_1 = 1;
            P1_2 = 0;
            P1_3 = 0;
        }
        if ((voltage_L+BUFFER < distanceV) && (voltage_L < voltage_R)) {
            //Turning 45 degrees to the right (move motor 2 backward)
            P1_0 = 0;
            P1_1 = 0;
            P1_2 = 1;
            P1_3 = 0;
        }
//try to move forwards
	    if ((voltage_R+BUFFER > distanceV) && (voltage_L+BUFFER > distanceV) && (voltage_R - voltage_L < LR_BUFFER )) {
	        //move forward straight
	        P1_0 = 0;
            P1_1 = 1;
            P1_2 = 1;
            P1_3 = 0;
	    }
	    
	    if ((voltage_R+BUFFER < distanceV) && (voltage_L+BUFFER < distanceV) && (voltage_R - voltage_L < LR_BUFFER )) {
	        //Turning 45 degrees to the right (move motor 2 backward)
	        P1_0 = 1;
            P1_1 = 0;
            P1_2 = 0;
            P1_3 = 1;
	    }//try to move backwards

	}
    // will only stop if distance 1 is not greater than distance buffer, is not less than distance with buffer
    // and distance 2 is not     
}

//RECEIVING FUNCTIONS
void waithalfus(void){
    _asm    
        ;For a 22.1184MHz crystal one machine cycle takes 12/22.1184MHz=0.5425347us
        ;mov TR0, #1
        ;mov R7, #9
        ;mov TR0, #0
        push AR1
        mov R1, #1
        pop AR1
        ret
    _endasm;
}


void wait_bit_time()
{
	int n=ITERATIONS;
	while (n>0)
	{
		n--;
	}
	return;
}

void wait_time(int iter)
{
	int n=iter;
	while (n>0)
	{
		n--;
	}
	return;
}

void wait_one_and_half_bit_time()
{
	int n=1.5*ITERATIONS;
	while (n>0)
	{
		n--;
	}
	return;
}

void wait_check()
{
	int n=.5*ITERATIONS;
	while (n>0)
	{
		n--;
	}
	return;
}

int rx_byte (int min, int channel)
{
	int j, val;
	int v;

	//skip the start bit
	val = 0;
	wait_one_and_half_bit_time();
	for (j=0; j<8; j++)
	{
		v=(GetADC(channel)*4.95/1023.0);					//read voltage in
		val|=(v>min)?(0x01<<j):0x00;	//if voltage is greater than "min" then the returned val gets a bit at the right position
		wait_bit_time();
	}
	//wait for stop bits
	wait_one_and_half_bit_time();
	return val;
}

//MOVE FUNCTIONS
void turn_180 ( void )
{
	    int wait;
    P1_0 = 0;
    P1_1 = 1;
    P1_2 = 0;
    P1_3 = 1;
    
    for (wait = 0; wait < 165; wait++) {
        waithalfus();
    }
    
    P1_0 = 0;
    P1_1 = 0;
    P1_2 = 0;
    P1_3 = 0;
    
    return;
}

void p_park ( void )
{
	int wait;
    P1_0 = 0;
    P1_1 = 1;
    P1_2 = 0;
    P1_3 = 0;

    for (wait = 0; wait < 113; wait++) {
        waithalfus();
    }

    //move forward

    P1_0 = 0;
    P1_1 = 1;
    P1_2 = 1;
    P1_3 = 0;

    for (wait = 0; wait < 223; wait++) {
        waithalfus();
    }

    //Turning 45 degrees to the left
    P1_0 = 0;
    P1_1 = 0;
    P1_2 = 1;
    P1_3 = 0;

    for (wait = 0; wait < 110; wait++) {
        waithalfus();
    }

    //move backwards

    P1_0 = 1;
    P1_1 = 0;
    P1_2 = 0;
    P1_3 = 1;

    for (wait = 0; wait < 125; wait++) {
        waithalfus();
    }


	P1_0 = 0;
	P1_1 = 0;
	P1_2 = 0;
	P1_3 = 0;

}

void refresh ( void )
{
	P1_0=0;
	P1_1=0;
	P1_2=0;
	P1_3=0;
}

void testmove ( void ) //move backwards
{
	P1_0=1;
	P1_1=0;
	P1_2=0;
	P1_3=1;
}

//MAIN FUNCTION
void main (void)
{
	printf("\n\nEECE 281 Electromagnetic Tether Robot. Reading Peak Voltages from the MCP3004.\n\n");
	distanceV = 4.0; //initial distance voltage (V)
	orientation = FORWARD;

	while (1)
	{
		int byte = 0;
		int iter;
		float toprint;
		iter = 6000;
		toprint = GetADC(1)*4.95/1023.0;
		//printf("Voltage = %f\n", toprint);

		if ((GetADC(1)*4.95)/1023.0 <.5 )					//as soon as there's a start bit...
		{
			wait_check();					//wait half a bit to make sure it actually is zero...
			if((GetADC(1)*4.95)/1023.0 < .5)
			{
				byte = rx_byte(1, 1);		//read the rest of the byte!
				printf("Byte: %i\n", byte);
			}


			if ((byte == 251) || (byte == 253) || (byte == 249))  {
				printf("Button 1 Pressed: ROTATE 180");// do a 180
				turn_180();
			}
			else if ((byte == 24) || (byte == 140) || (byte == 136)) {
				printf("Button 2 Pressed: PARALLEL PARK");
				p_park(); // parallel park
			}
			else if ((byte == 55) || (byte == 177) || (byte == 103) || (byte == 39)) {
				distanceV = 4.0;
				printf("Button 3 Pressed: CLOSER");
				//testmove();
				if(distanceV > 4.5){
					distanceV += 1.0; //decrease fixed distance (increase voltage)
					printf("Come CLOSER\n");
				}
			}
			else if ((byte == 198) || (byte == 226) || (byte == 230)) {
				distanceV = 0.5;
				printf("Button 4 Pressed: FURTHER");
				if(distanceV < 0.5){
					distanceV -= 1.0; //increase fixed distance (decrease voltage)
					printf("Go Further\n");
				}
			}
			else if ((byte == 241) || (byte == 113) || (byte == 30)) {
				printf("Button 5 Pressed");
			} 
		}
		
		getMaxVoltages();
		follow(); //start movement
	
	}
}
