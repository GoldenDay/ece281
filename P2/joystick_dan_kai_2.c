#include <stdio.h>
#include <at89lp51rd2.h>
#include <stdlib.h>
#include <math.h>

// ~C51~ 
#define CLK 22118400L
#define BAUD 115200L
#define BRG_VAL (0x100-(CLK/(32L*BAUD)))

#define FREQ 10000L
#define TIMER0_RELOAD_VALUE (65536L-(CLK/(12L*FREQ)))

// Red 		VCC
// Black	Ground
// Grey 	Forward			P0_0
// Yellow 	Back			P0_1
// Orange 	Right			P0_2
// Blue 	Left			P0_3
// White	L1 / 180		P0_4
// Green	L2 / p_park_J	P0_5

#define P_JF P0_0
#define P_JB P0_1
#define P_JR P0_2
#define P_JL P0_3
#define P_B8 P0_4
#define P_BP P0_5

#define P_OS P0_7

unsigned char _c51_external_startup(void)
{
	// Configure ports as a bidirectional with internal pull-ups.
	P0M0=0;	P0M1=0;
	P1M0=0;	P1M1=0;
	P2M0=0;	P2M1=0;
	P3M0=0;	P3M1=0;
	AUXR=0B_0001_0001; // 1152 bytes of internal XDATA, P4.4 is a general purpose I/O
	P4M0=0;	P4M1=0;
    
    // Initialize the serial port and baud rate generator
    PCON|=0x80;
	SCON = 0x52;
    BDRCON=0;
    BRL=BRG_VAL;
    BDRCON=BRR|TBCK|RBCK|SPD;
    
    return 0;
}

void refresh ( void )
{
	P1_0=0;
	P1_1=0;
	P1_2=0;
	P1_3=0;
}

int wiathalfs_J (void)
{
	_asm	
		;For a 22.1184MHz crystal one machine cycle 
		;takes 12/22.1184MHz=0.5425347us
	    mov R2, #1
	L3:	mov R1, #248
	L2:	mov R0, #184
	L1:	djnz R0, L1 ; 2 machine cycles-> 2*0.5425347us*184=200us
	    djnz R1, L2 ; 200us*250=0.05s
	    djnz R2, L3 ; 0.05s*5=0.25s
	    ret
    _endasm;
    
    return 1;
}

void mov_forward_0 ( void )
{
	while (P_JF == 1)
	{
		P1_0=0;
		P1_1=1;
		P1_2=1;
		P1_3=0;
	}
	return;
}

void mov_backward_0 ( void )
{
	while (P_JB == 1)
	{
		P1_0=1;
		P1_1=0;
		P1_2=0;
		P1_3=1;
	}
	return; 
}

void mov_forward_1 ( void )
{
	while (P_JB == 1)
	{
		P1_0=0;
		P1_1=1;
		P1_2=1;
		P1_3=0;
	}
	return;
}

void mov_backward_1 ( void )
{
	while (P_JF == 1)
	{
		P1_0=1;
		P1_1=0;
		P1_2=0;
		P1_3=1;
	}
	return; 
}

void turn_R ( void )
{
	while (P_JR == 1)
	{
		P1_0=0;
		P1_1=1;
		P1_2=0;
		P1_3=1;
	}
	return;
}

void turn_L ( void )
{	
	while (P_JL == 1)
	{
		P1_0=1;
		P1_1=0;
		P1_2=1;
		P1_3=0;
	}
	return; 
}

void turn_180_J ( void )
{
	int wait;
	P1_0=0;
	P1_1=1;
	P1_2=0;
	P1_3=1;
	
	for (wait = 0; wait < 25; wait++)
	{	
		wiathalfs_J();
    }
    refresh();
	return;
}

void p_park_J ( void )
{
	int wait;
	//Turning 45 degrees left
	P1_0=1;
	P1_1=0;
	P1_2=1;
	P1_3=0;

	for (wait = 0; wait < 7; wait++)
	{	
		wiathalfs_J();
    }
	
    //move back
	P1_0=1;
	P1_1=0;
	P1_2=0;
	P1_3=1;

	for (wait = 0; wait < 15; wait++)
	{	
		wiathalfs_J();
    }

    //Turning 45 degrees to the right
	P1_0=0;
	P1_1=0;
	P1_2=0;
	P1_3=1;

	for (wait = 0; wait < 12; wait++)
	{	
		wiathalfs_J();
    }

    //move backwards
	P1_0=1;
	P1_1=0;
	P1_2=0;
	P1_3=1;
	
	for (wait = 0; wait < 7; wait++)
	{	
		wiathalfs_J();
    }

    //move forwards
	P1_0=0;
	P1_1=1;
	P1_2=1;
	P1_3=0;
	
	for (wait = 0; wait < 10; wait++)
	{	
		wiathalfs_J();
    }
	
	refresh();
	return;
}

void manual_overide ( void )
{	
	int orientation = 0;
	
	while(P_OS != 0)
	{
		if (orientation == 0)
		{
			if (P_BP == 1)
				p_park_J();
			if (P_B8 == 1)
			{
				turn_180_J();
				orientation = 1;
			}	
			if (P_JF == 1)
				mov_forward_0();
			if (P_JB == 1)
				mov_backward_0();
			if (P_JR == 1)
				turn_R();
			if (P_JL == 1)
				turn_L();
			
			refresh();
		}
		
		else
		{
			if (P_BP == 1)
				p_park_J();
			if (P_B8 == 1)
			{
				turn_180_J();
				orientation = 0;
			}
			if (P_JB == 1)
				mov_forward_1();
			if (P_JF == 1)
				mov_backward_1();
			if (P_JR == 1)
				turn_R();
			if (P_JL == 1)
				turn_L();
				
			refresh();
		}
		
		refresh();
	}
	return;
}

void main ( void )
{
	manual_overide();
}
