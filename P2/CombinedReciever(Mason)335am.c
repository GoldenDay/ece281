// Combines motor controls with voltage/distance readings from the MCP3004 ADC.
// Includes bit-bang reception.
// SPI functions copied over from lp51b_SPI.c by Dr. Jesus Calvino-Fraga

#include <stdio.h> 
#include <at89lp51rd2.h>
#include <math.h>

// ~C51~  
#define CLK 22118400L
#define BAUD 115200L
#define BRG_VAL (0x100-(CLK/(32L*BAUD)))
// ~constants~
#define BUFFER 0.05
#define LR_BUFFER 0.2
#define FORWARD 1
#define BACKWARD 0
// byte constants for receiver functions
#define CLOSER 25
#define FURTHER 50
#define DIST_MIN 10 //minimum distance setting (voltage)
#define DIST_MAX 40 //maximum distance setting (voltage)
#define SWITCH 75	//turn 180 degrees
#define PARALELL_PARK 100

#define ITERATIONS 20000 //MUST BE SAME ON TRANSMMITTER

volatile float voltage_R;
volatile float voltage_L;

volatile float distanceV; //voltage at distance robot must keep from transmitter (V)
volatile int orientation;

//Functions
void getVoltage(float *par, int i);
void getMaxVoltages(void);
void getDistances(void);
void follow(void);
void testmove(void);
void wait_one_and_half_bit_time(void);


unsigned char _c51_external_startup(void)
{
	// Configure ports as a bidirectional with internal pull-ups.
	P0M0=0;	P0M1=0;
	P1M0=0;	P1M1=0;
	P2M0=0;	P2M1=0;
	P3M0=0;	P3M1=0;
	AUXR=0B_0001_0001; // 1152 bytes of internal XDATA, P4.4 is a general purpose I/O
	P4M0=0;	P4M1=0;
    
    // Instead of using a timer to generate the clock for the serial
    // port, use the built-in baud rate generator.
    PCON|=0x80;
    SCON = 0x52;
    BDRCON=0;
    BRL=BRG_VAL;
    BDRCON=BRR|TBCK|RBCK|SPD;
    
    return 0;
}

void SPIWrite(unsigned char value)
{
	SPSTA&=(~SPIF); // Clear the SPIF flag in SPSTA
	SPDAT=value;
	while((SPSTA & SPIF)!=SPIF); //Wait for transmission to end
}

unsigned int GetADC(unsigned char channel)
{
	
	unsigned int adc;

	// initialize the SPI port to read the MCP3004 ADC attached to it.
	SPCON&=(~SPEN); // Disable SPI
	SPCON=MSTR|CPOL|CPHA|SPR1|SPR0|SSDIS;
	SPCON|=SPEN; // Enable SPI
	
	P1_4=0; // Activate the MCP3004 ADC.
	SPIWrite(channel|0x18);	// Send start bit, single/diff* bit, D2, D1, and D0 bits.
	for(adc=0; adc<10; adc++); // Wait for S/H to setup
	SPIWrite(0x55); // Read bits 9 down to 4
	adc=((SPDAT&0x3f)*0x100);
	SPIWrite(0x55);// Read bits 3 down to 0
	P1_4=1; // Deactivate the MCP3004 ADC.
	adc+=(SPDAT&0xf0); // SPDR contains the low part of the result. 
	adc>>=4;
		
	return adc;
}


/*      LP51B    MCP3004
---------------------------
MISO  -  P1.5  - pin 10
SCK   -  P1.6  - pin 11
MOSI  -  P1.7  - pin 9
CE*   -  P1.4  - pin 8
4.8V  -  VCC   - pins 13, 14
0V    -  GND   - pins 7, 12
CH0   -        - pin 1
CH1   -        - pin 2
CH2   -        - pin 3
CH3   -        - pin 4
*/

void wait_bit_time()
{
	int n=ITERATIONS;
	while (n>0)
	{
		n--;
	}
	return;
}

void wait_time(int iter)
{
	int n=iter;
	while (n>0)
	{
		n--;
	}
	return;
}

void wait_check()
{
	int n=.5*ITERATIONS;
	while (n>0)
	{
		n--;
	}
	return;
}

void wait1s (void)
{
	_asm	
		;For a 22.1184MHz crystal one machine cycle 
		;takes 12/22.1184MHz=0.5425347us
	    mov R2, #20
	L6:	mov R1, #248
	L5:	mov R0, #184
	L4:	djnz R0, L4 ; 2 machine cycles-> 2*0.5425347us*184=200us
	    djnz R1, L5 ; 200us*250=0.05s
	    djnz R2, L6 ; 0.05s*20=1s
	    ret
    _endasm;
}


void wait_one_and_half_bit_time()
{
	int n=1.5*ITERATIONS;
	while (n>0)
	{
		n--;
	}
	return;
}

int waithalfs (void)
{
	_asm	
	;For a 22.1184MHz crystal one machine cycle
	;takes 12/22.1184MHz=0.5425347us
	mov R2, #1
	L3:	mov R1, #248
	L2:	mov R0, #184
	L1:	djnz R0, L1 ; 2 machine cycles-> 2*0.5425347us*184=200us
	djnz R1, L2 ; 200us*250=0.05s
	djnz R2, L3 ; 0.05s*5=0.25s
	ret
	    _endasm;
    
    return 1;
}

void getVoltage ( float *par, int i )
{
	*par = (GetADC(i)*4.95)/1023.0;
	return;
}


void getMaxVoltages(void)
{
	getVoltage ( &voltage_R, 1);
	getVoltage ( &voltage_L, 0);
	return;
}


//TODO: measure voltages at preset distances
void getDistances(void)
{
	//distance_R = ( 9.0 / voltage_R );
	//distance_L = ( 9.0 / voltage_L );
	return;
}


//distance_R = distance1, distance_L = distance 2, BUFFER, FORWARD, BACKWARD defined as a constants
void follow(void) 
{
	if (orientation == FORWARD) {
	    if ((voltage_L+BUFFER < distanceV) && (voltage_L < voltage_R)) {
	        //Turning 45 degrees to the right (move motor 1 forward)
	        P1_0 = 0;
            P1_1 = 1;
            P1_2 = 0;
            P1_3 = 0;
	    }
	    if ((voltage_R+BUFFER < distanceV) && (voltage_R < voltage_L)) {
	        //Turning 45 degrees to the left (move motor 2 forward)
	        P1_0 = 0;
            P1_1 = 0;
            P1_2 = 1;
            P1_3 = 0;
	    }
	    if ((voltage_L-BUFFER > distanceV) && (voltage_L > voltage_R)) {
	        //Turning 45 degrees to the left (move motor 1 backward)
	        P1_0 = 1;
            P1_1 = 0;
            P1_2 = 0;
            P1_3 = 0;
	    }
	    if ((voltage_R-BUFFER > distanceV) && (voltage_R > voltage_L)) {
	        //Turning 45 degrees to the right (move motor 2 backward)
	        P1_0 = 0;
            P1_1 = 0;
            P1_2 = 0;
            P1_3 = 1;
	    }
	    
	    //try to move forwards
	    if ((voltage_R-BUFFER < distanceV) && (voltage_L-BUFFER < distanceV) && ( (voltage_R - voltage_L < LR_BUFFER ) || (voltage_L - voltage_R < LR_BUFFER ) ) ) {
	        //move forward straight
	        P1_0 = 0;
            P1_1 = 1;
            P1_2 = 1;
            P1_3 = 0;
	    }
	    
	   if ((voltage_R-BUFFER > distanceV) && (voltage_L-BUFFER > distanceV) && ( (voltage_R - voltage_L < LR_BUFFER ) || (voltage_L - voltage_R < LR_BUFFER ) ) )  {
		  	P1_0 = 1;
			P1_1 = 0;
			P1_2 = 0;
			P1_3 = 1;
		} // move backwards since too close
	    
	    
	}
 	else if (orientation == BACKWARD) {
        if ((voltage_R-BUFFER > distanceV) && (voltage_R > voltage_L)) {
            //Turning 45 degrees to the left (move motor 2 forward)
            P1_0 = 1;
            P1_1 = 0;
            P1_2 = 0;
            P1_3 = 0;
        }
        if ((voltage_L-BUFFER > distanceV) && (voltage_L > voltage_R)) {
            //Turning 45 degrees to the right(move motor 1 forward)
            P1_0 = 0;
            P1_1 = 0;
            P1_2 = 0;
            P1_3 = 1;
        }
        if ((voltage_R+BUFFER < distanceV) && (voltage_R < voltage_L)) {
            //Turning 45 degrees to the left (move motor 2 backward)
            P1_0 = 0;
            P1_1 = 1;
            P1_2 = 0;
            P1_3 = 0;
        }
        if ((voltage_L+BUFFER < distanceV) && (voltage_L < voltage_R)) {
            //Turning 45 degrees to the right (move motor 2 backward)
            P1_0 = 0;
            P1_1 = 0;
            P1_2 = 1;
            P1_3 = 0;
        }
//try to move forwards
	    if ((voltage_R+BUFFER > distanceV) && (voltage_L+BUFFER > distanceV) && (voltage_R - voltage_L < LR_BUFFER )) {
	        //move forward straight
	        P1_0 = 0;
            P1_1 = 1;
            P1_2 = 1;
            P1_3 = 0;
	    }
	    
	    if ((voltage_R+BUFFER < distanceV) && (voltage_L+BUFFER < distanceV) && (voltage_R - voltage_L < LR_BUFFER )) {
	        //Turning 45 degrees to the right (move motor 2 backward)
	        P1_0 = 1;
            P1_1 = 0;
            P1_2 = 0;
            P1_3 = 1;
	    }//try to move backwards

	}
    // will only stop if distance 1 is not greater than distance buffer, is not less than distance with buffer
    // and distance 2 is not     
}


int rx_byte (int min, int channel)
{
	int j, val;
	int v;
	
	//skip the start bit
	val = 0;
	wait_one_and_half_bit_time();
	for (j=7; j>=0; j--)
	{
		printf("Bit %i\n", j);
		v=(GetADC(channel)*4.95/1023.0);					//read voltage in
		printf("v: %i, val: %i\n", v, val);
		val|=(v>=min)?(0x01<<j):0x00;	//if voltage is greater than "min" then the returned val gets a bit at the right position
		wait_bit_time();
	}
	//wait for stop bits
	wait_one_and_half_bit_time();
	return val;
}

void turn_180 ( void )
{
    P1_0 = 0;
    P1_1 = 1;
    P1_2 = 0;
    P1_3 = 1;
    
    wait1s();
    
    P1_0 = 0;
    P1_1 = 0;
    P1_2 = 0;
    P1_3 = 0;
    
    return;
}

void p_park ( void )
{
    P1_0 = 0;
    P1_1 = 1;
    P1_2 = 0;
    P1_3 = 0;

   wait1s();

    //move forward

    P1_0 = 0;
    P1_1 = 1;
    P1_2 = 1;
    P1_3 = 0;

    wait1s();

    //Turning 45 degrees to the left
    P1_0 = 0;
    P1_1 = 0;
    P1_2 = 1;
    P1_3 = 0;

    wait1s();

    //move backwards

    P1_0 = 1;
    P1_1 = 0;
    P1_2 = 0;
    P1_3 = 1;

	wait1s();


	P1_0 = 0;
	P1_1 = 0;
	P1_2 = 0;
	P1_3 = 0;

}

void refresh ( void )
{
	P1_0=0;
	P1_1=0;
	P1_2=0;
	P1_3=0;
}



void testmove(void) {
//moves backward
if ((voltage_R-BUFFER > distanceV) && (voltage_L-BUFFER > distanceV) && (voltage_R - voltage_L < LR_BUFFER )) {
  	P1_0 = 1;
	P1_1 = 0;
	P1_2 = 0;
	P1_3 = 1;
	}
}

void main (void)
{
	printf("\n\nEECE 281 Electromagnetic Tether Robot. Reading Peak Voltages from the MCP3004.\n\n");
	distanceV = 2; //initial distance voltage (V)
	orientation = FORWARD;

	while (1)
	{
		int byte = 0;
		
		int iter;
		float toprint;
		iter = 6000;
		toprint = GetADC(1)*4.95/1023.0;
		
		printf("GetADC: %f\n", toprint);
		wait_time(iter);
		
		follow(); //start movement
		
		if ((GetADC(1)*4.95)/1023.0 <.5 )				//as soon as there's a start bit (0)...
		{		
			wait_check();					//wait half a bit to make sure it actually is zero...
			
			if((GetADC(1)*4.95)/1023.0 < .5)
			{
				byte = rx_byte(1, 1);		//read the rest of the byte!
				printf("Byte: %i\n", byte);
			}
			
				if ((byte == 199) || (byte == 142))  
				{
					printf("Button 1 Pressed");
					turn_180();
					if (orientation == FORWARD)
						orientation = BACKWARD;
					else
						orientation = FORWARD;
				}
				else if (byte == 99) 
				{
					printf("Button 2 Pressed");
					wait1s();
					wait1s();
				}
				else if ((byte == 228) || (byte == 236)) 
				{
					printf("Button 3 Pressed");
					wait1s();
					wait1s();
				}
				else if (byte == 49) 
				{
					printf("Button 4 Pressed");
					wait1s();
					wait1s();
				}
				else if (byte == 159) 
				{
					printf("Button 5 Pressed");
					wait1s();
					wait1s();
				}
		}
	
	}
}
