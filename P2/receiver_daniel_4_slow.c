// Program for receiving and reading electromagnetic signals.
// This reads the peak voltages produced by the Tank Circuit using
// the MCP3004 10-bit ADC.
//
// SPI functions copied over from lp51b_SPI.c by Dr. Jesus Calvino-Fraga

#include <stdio.h> 
#include <at89lp51rd2.h>

// ~C51~  
#define CLK 22118400L
#define BAUD 115200L
#define BRG_VAL (0x100-(CLK/(32L*BAUD)))

unsigned char _c51_external_startup(void)
{
	// Configure ports as a bidirectional with internal pull-ups.
	P0M0=0;	P0M1=0;
	P1M0=0;	P1M1=0;
	P2M0=0;	P2M1=0;
	P3M0=0;	P3M1=0;
	AUXR=0B_0001_0001; // 1152 bytes of internal XDATA, P4.4 is a general purpose I/O
	P4M0=0;	P4M1=0;
    
    // Instead of using a timer to generate the clock for the serial
    // port, use the built-in baud rate generator.
    PCON|=0x80;
	SCON = 0x52;
    BDRCON=0;
    BRL=BRG_VAL;
    BDRCON=BRR|TBCK|RBCK|SPD;
    
    return 0;
}

void SPIWrite(unsigned char value)
{
	SPSTA&=(~SPIF); // Clear the SPIF flag in SPSTA
	SPDAT=value;
	while((SPSTA & SPIF)!=SPIF); //Wait for transmission to end
}

unsigned int GetADC(unsigned char channel)
{
	unsigned int adc;

	// initialize the SPI port to read the MCP3004 ADC attached to it.
	SPCON&=(~SPEN); // Disable SPI
	SPCON=MSTR|CPOL|CPHA|SPR1|SPR0|SSDIS;
	SPCON|=SPEN; // Enable SPI
	
	P1_4=0; // Activate the MCP3004 ADC.
	SPIWrite(channel|0x18);	// Send start bit, single/diff* bit, D2, D1, and D0 bits.
	for(adc=0; adc<10; adc++); // Wait for S/H to setup
	SPIWrite(0x55); // Read bits 9 down to 4
	adc=((SPDAT&0x3f)*0x100);
	SPIWrite(0x55);// Read bits 3 down to 0
	P1_4=1; // Deactivate the MCP3004 ADC.
	adc+=(SPDAT&0xf0); // SPDR contains the low part of the result. 
	adc>>=4;
		
	return adc;
}

void maxvoltage ( float *par, int i )
{
	if ( (GetADC(i)*4.95)/1023.0 > *par)
		*par = (GetADC(i)*4.95)/1023.0;
	return;
}

int wait1s (void)
{
	_asm	
		;For a 22.1184MHz crystal one machine cycle 
		;takes 12/22.1184MHz=0.5425347us
	    mov R2, #20
	L3:	mov R1, #248
	L2:	mov R0, #184
	L1:	djnz R0, L1 ; 2 machine cycles-> 2*0.5425347us*184=200us
	    djnz R1, L2 ; 200us*250=0.05s
	    djnz R2, L3 ; 0.05s*5=0.25s
	    ret
    _endasm;
    
    return 1;
}

//         LP51B    MCP3004
//---------------------------
// MISO  -  P1.5  - pin 10
// SCK   -  P1.6  - pin 11
// MOSI  -  P1.7  - pin 9
// CE*   -  P1.4  - pin 8
// 4.8V  -  VCC   - pins 13, 14
// 0V    -  GND   - pins 7, 12
// CH0   -        - pin 1
// CH1   -        - pin 2
// CH2   -        - pin 3
// CH3   -        - pin 4

void main (void)
{
	int i, c;
	int counter1 = 0, counter2 = 0;
	float temp1 = 0.001, temp2 = 0.001;
	
	printf("\n\nAT89LP51RB2 SPI Peak Detector Test using the MCP3004. V1 = Right, V2 = Left \n\n");
	
	while (1)
	{
		for(c = 0; c < 3; c++)
		{
			//Initialization
			float max1 = 0.0, max2 = 0.0;
		
			//give adc time to register and find peak V
			for(i = 0; i < 1200; i++)
			{
				maxvoltage(&max1, 0);
				maxvoltage(&max2, 1);
			}
			
			//checks need for amplificiation for v1, and does so
			if (max1 < 0.2)
			{
				for(i = 0; i < 1200; i++)
				{
					maxvoltage(&max1, 2);
				}
				max1 = max1/5.0;
			}
	
			if (max1 <0.001)
			{
				max1 = temp1;
				counter1++;
			}
			
			//checks need for amplificiation for v2, and does so
			if (max2 < 0.2)
			{
				for(i = 0; i < 1200; i++)
				{
					maxvoltage(&max2, 3);
				}
				max2 = max2/3.0;
			}
			
			if (max2 < 0.001)
			{
				max2 = temp2;
				counter2++;
			}
		
			printf("Peak V1 = %4.3f ", max1);
			printf("Peak V2 = %4.3f ", max2);
			printf("\n");
			
			temp1 = max1;
			temp2 = max2;

		}
		
		if (counter1 = counter2 && counter1 > 2)
		{	
			temp1 = 0.0;
			temp2 = 0.0;
		}
	}
}
