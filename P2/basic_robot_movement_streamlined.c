#include <stdio.h>
#include <at89lp51rd2.h>
#include <stdlib.h>
#include <math.h>

// ~C51~ 
 
#define CLK 22118400L
#define BAUD 115200L
#define BRG_VAL (0x100-(CLK/(32L*BAUD)))

//We want timer 0 to interrupt every 100 microseconds ((1/10000Hz)=100 us)
#define FREQ 10000L
#define TIMER0_RELOAD_VALUE (65536L-(CLK/(12L*FREQ)))


/* ANSI colors */
#define	COLOR_BLACK		0
#define	COLOR_RED		1
#define	COLOR_GREEN		2
#define	COLOR_YELLOW	3
#define	COLOR_BLUE		4
#define	COLOR_MAGENTA	5
#define	COLOR_CYAN		6
#define	COLOR_WHITE		7



/* Some ANSI escape sequences */
#define CURSOR_ON "\x1b[?25h"
#define CURSOR_OFF "\x1b[?25l"
#define CLEAR_SCREEN "\x1b[2J"
#define GOTO_YX "\x1B[%d;%dH"
#define CLR_TO_END_LINE "\x1B[K"
/* Black foreground, white background */
#define BKF_WTB "\x1B[0;30;47m"
#define FORE_BACK "\x1B[0;3%d;4%dm"
#define FONT_SELECT "\x1B[%dm"

/*
� � � � � � � � � � � � � � � �

� � � � � � � � � � � � � � � �

� � � � � � � � � � � � � � � �
*/

//These variables are used in the ISR
volatile unsigned char pwmcount;
volatile float pwm1;
volatile float pwm2;
volatile float pwm3;
volatile float pwm4;
volatile float tentativepwm1;
volatile float tentativepwm2;
volatile float tentativepwm3;
volatile float tentativepwm4;

volatile int direction; // boolean variable for direction, 0 for CW, 1 for CCW
volatile float speed;
volatile float offsetspeed; //offset for 0<speed<5
volatile float rpm;

void rotate(int direction, float speed);
void move(int direction, float speed);
void waithalfus(void);
void parrallelPark(void);
void rotate180(void);

unsigned char _c51_external_startup(void)
{
	// Configure ports as a bidirectional with internal pull-ups.
	P0M0=0;	P0M1=0;
	P1M0=0;	P1M1=0;
	P2M0=0;	P2M1=0;
	P3M0=0;	P3M1=0;
	AUXR=0B_0001_0001; // 1152 bytes of internal XDATA, P4.4 is a general purpose I/O
	P4M0=0;	P4M1=0;
    
    // Initialize the serial port and baud rate generator
    PCON|=0x80;
	SCON = 0x52;
    BDRCON=0;
    BRL=BRG_VAL;
    BDRCON=BRR|TBCK|RBCK|SPD;
	
	// Initialize timer 0 for ISR 'pwmcounter()' below
	TR0=0; // Stop timer 0
	TMOD=0x01; // 16-bit timer
	// Use the autoreload feature available in the AT89LP51RB2
	// WARNING: There was an error in at89lp51rd2.h that prevents the
	// autoreload feature to work.  Please download a newer at89lp51rd2.h
	// file and copy it to the crosside\call51\include folder.
	TH0=RH0=TIMER0_RELOAD_VALUE/0x100;
	TL0=RL0=TIMER0_RELOAD_VALUE%0x100;
	TR0=1; // Start timer 0 (bit 4 in TCON)
	ET0=1; // Enable timer 0 interrupt
	EA=1;  // Enable global interrupts
	
	pwmcount=0;
    
    return 0;
}

// Interrupt 1 is for timer 0.  This function is executed every time
// timer 0 overflows: 100 us.
void pwmcounter (void) interrupt 1
{
	if(++pwmcount>99) 
		pwmcount=0;
	P1_0=(pwm1>pwmcount)?1:0;
	P1_1=(pwm2>pwmcount)?1:0;
	P1_2=(pwm3>pwmcount)?1:0;
	P1_3=(pwm4>pwmcount)?1:0;
}

void main (void)
{
	pwm1=0; //at zero until user enters duty cycle
	pwm2=0;
	pwm3=0;
	pwm4=0;
	
	
	while(1) {
	
		scanf("%d \n", &direction);
		
		
		rotate(direction, 100);
		
		/*
		while(1){
			//connect P1_2 to a push button that changes direction when pressed.
			while(P0_5 == 0);
			while(P0_5 == 1){
			waithalfus();
				if(P0_6 == 0 && P0_5 == 1){
					direction = direction ^ 1; //switch direction XOR
					rotate(direction, speed);
					printf( GOTO_YX , 2, 44);
					if (direction == 0) {
							printf(" CW");
					}
					else {
							printf("CCW");
					}
					break;
				}
				else{
					if( (speed < 100) && ((speed+10) <= 100) )
						speed += 10;
					else
						speed = 0; //overflow to 0% after 100
					rotate(direction, speed);
					printf( GOTO_YX , 4, 44);
					printf("%3.0f", speed);
					rpm = 0.0028*powf((const float)speed, (const float)2.15);
					printf( GOTO_YX , 6, 44);
					printf("%3.1f", rpm);
					break;
				}
				
			}
			*/
			
			//UPDATE DATA
			
			

			/*
			//HOLD PAUSE BUTTON
			while(P1_3 == 1){
				TR0=0; //Pause timer 0
			}
			TR0=1; //Re-Enable timer 0
			
			*/
			/*
			//LEVEL INCREASE
			if(P0_6 == 1){
				if( (speed < 100) && ((speed+10) <= 100) )
					speed += 10;
				else
					speed = 0; //overflow to 0% after 100
				rotate(direction, speed);
			}
			*/
			
			/*
			//LEVEL DECREASE
			if(P1_5 == 1){
				if( (speed > 0) && ((speed-10) >= 0) )
					speed -= 10;
				else
					speed = 100; //overflow to 0% after 100
				rotate(direction, speed);
			}
			*/
		}
		
		
	
		/*
		printf("Please enter a duty cycle between 0 to 100 for channel 1 at P1.0 \n");
		scanf("%f \n", &tentativepwm1);
		
		while ((tentativepwm1 > 100) || (tentativepwm1 < 0)) {
			printf("Please re-enter an appropriate duty cycle between 0 to 100 for channel 1 at P1.0 \n");
			scanf("%f \n", &tentativepwm1);
		}		
		pwm1 = tentativepwm1;
		printf("Channel 1 duty cycle is at %.1f", pwm1);
		printf("%% \n");
		 
		printf("Please enter a duty cycle between 0 to 100 for channel 2 at P1.1 \n");
		scanf("%f \n", &tentativepwm2);
		while ((tentativepwm2 > 100) || (tentativepwm2 < 0)) {
			printf("Please re-enter an appropriate duty cycle between 0 to 100 for channel 2 at P1.1 \n");
			scanf("%f \n", &tentativepwm2);
		}
		pwm2 = tentativepwm2;
		printf("Channel 2 duty cycle is at %.1f", pwm2);
		printf("%% \n");
		
		printf("Please enter a duty cycle between 0 to 100 for channel 3 at P1.2 \n");
		scanf("%f \n", &tentativepwm3);
		
		while ((tentativepwm3 > 100) || (tentativepwm3 < 0)) {
			printf("Please re-enter an appropriate duty cycle between 0 to 100 for channel 3 at P1.2 \n");
			scanf("%f \n", &tentativepwm3);
		}		
		pwm3 = tentativepwm3;
		printf("Channel 3 duty cycle is at %.1f", pwm3);
		printf("%% \n");
		 
		printf("Please enter a duty cycle between 0 to 100 for channel 4 at P1.3 \n");
		scanf("%f \n", &tentativepwm4);
		while ((tentativepwm4 > 100) || (tentativepwm4 < 0)) {
			printf("Please re-enter an appropriate duty cycle between 0 to 100 for channel 4 at P1.3 \n");
			scanf("%f \n", &tentativepwm4);
		}
		pwm4 = tentativepwm4;
		printf("Channel 4 duty cycle is at %.1f", pwm4);
		printf("%% \n");
		*/
		

		
		
		
	//}

	
}

void rotate(int direction, float speed) 
{
	if (direction == 0) {
		//Turning Clockwise/Right
		pwm1 = 0;
		pwm2 = speed;
		pwm3 = 0;
		pwm4 = speed;
	}
	
	else if (direction == 1) {
		//Turning Counter-clockwise/Left
		pwm1 = speed;
		pwm2 = 0;
		pwm3 = speed;
		pwm4 = 0;	
	}
	
	else if (direction == 2) {
		//Turning forwards
		pwm1 = speed;
		pwm2 = 0;
		pwm3 = 0;
		pwm4 = speed;	
	}
	
	else if (direction == 3) {
		//Turning backwards
		pwm1 = 0;
		pwm2 = speed;
		pwm3 = speed;
		pwm4 = 0;	
	}
}

void move(int direction, float speed) 
{
	if (direction == 0) {
		//Go backwards
		pwm1 = 0;
		pwm2 = speed;
		pwm3 = speed;
		pwm4 = 0;
	}
	
	else {
		//Go forwards
		pwm1 = speed;
		pwm2 = 0;
		pwm3 = 0;
		pwm4 = speed;	
	}
}


void waithalfus(void){
	_asm	
		;For a 22.1184MHz crystal one machine cycle takes 12/22.1184MHz=0.5425347us
	    ;mov TR0, #1
	    ;mov R7, #9
	    ;mov TR0, #0
	    push AR1
	    mov R1, #1
	    pop AR1
	    ret
    _endasm;
}

/**
 * Basic Parrallel Park Operation:
 * makes the car perform a basic parrallel park.
 * constraint: must be able to park in a space within 1.5x of its own length
 */
void parrallelPark(void){
    //TODO: implement me

}

/**
 * Basic 180 degrees rotation:
 * Car performs a 180 degree turn on the spot
 */
void rotate180(void){
    //TODO: implement me
    pwm1 = 50;
    pwm3 = 50;
}