// Optimized by having everythign written out in one fuction.
// Program for receiving and reading electromagnetic signals.
// This reads the peak voltages produced by the Tank Circuit using
// the MCP3004 10-bit ADC.
//
// SPI functions copied over from lp51b_SPI.c by Dr. Jesus Calvino-Fraga

#include <stdio.h> 
#include <at89lp51rd2.h>

// ~C51~  
#define CLK 22118400L
#define BAUD 115200L
#define BRG_VAL (0x100-(CLK/(32L*BAUD)))

volatile float voltage_R;
volatile float voltage_L;

volatile float distance_R;
volatile float distance_L;

unsigned char _c51_external_startup(void)
{
	// Configure ports as a bidirectional with internal pull-ups.
	P0M0=0;	P0M1=0;
	P1M0=0;	P1M1=0;
	P2M0=0;	P2M1=0;
	P3M0=0;	P3M1=0;
	AUXR=0B_0001_0001; // 1152 bytes of internal XDATA, P4.4 is a general purpose I/O
	P4M0=0;	P4M1=0;
    
    // Instead of using a timer to generate the clock for the serial
    // port, use the built-in baud rate generator.
    PCON|=0x80;
	SCON = 0x52;
    BDRCON=0;
    BRL=BRG_VAL;
    BDRCON=BRR|TBCK|RBCK|SPD;
    
    return 0;
}

void SPIWrite(unsigned char value)
{
	SPSTA&=(~SPIF); // Clear the SPIF flag in SPSTA
	SPDAT=value;
	while((SPSTA & SPIF)!=SPIF); //Wait for transmission to end
}

unsigned int GetADC(unsigned char channel)
{
	unsigned int adc;

	// initialize the SPI port to read the MCP3004 ADC attached to it.
	SPCON&=(~SPEN); // Disable SPI
	SPCON=MSTR|CPOL|CPHA|SPR1|SPR0|SSDIS;
	SPCON|=SPEN; // Enable SPI
	
	P1_4=0; // Activate the MCP3004 ADC.
	SPIWrite(channel|0x18);	// Send start bit, single/diff* bit, D2, D1, and D0 bits.
	for(adc=0; adc<10; adc++); // Wait for S/H to setup
	SPIWrite(0x55); // Read bits 9 down to 4
	adc=((SPDAT&0x3f)*0x100);
	SPIWrite(0x55);// Read bits 3 down to 0
	P1_4=1; // Deactivate the MCP3004 ADC.
	adc+=(SPDAT&0xf0); // SPDR contains the low part of the result. 
	adc>>=4;
		
	return adc;
}

//         LP51B    MCP3004
//---------------------------
// MISO  -  P1.5  - pin 10
// SCK   -  P1.6  - pin 11
// MOSI  -  P1.7  - pin 9
// CE*   -  P1.4  - pin 8
// 4.8V  -  VCC   - pins 13, 14
// 0V    -  GND   - pins 7, 12
// CH0   -        - pin 1
// CH1   -        - pin 2
// CH2   -        - pin 3
// CH3   -        - pin 4


void getVoltage ( float *par, int i )
{
	float temp = 0.01;
	int c;
	
	*par = 0.001;
	
	while( temp != *par )
	{
		for(c = 0; c < 1200; c++)
		{
			if ( (GetADC(i)*4.95)/1023.0 > *par)
				*par = (GetADC(i)*4.95)/1023.0;
		}
		
		temp = *par;
		
		printf("\r temp of *par = %4.3f and from %d", temp, i);
		
		for(c = 0; c < 1200; c++)
		{
			if ( (GetADC(i)*4.95)/1023.0 > *par)
				*par = (GetADC(i)*4.95)/1023.0;
		}
	}		
	return;
}

void getMaxVoltage ()
{
	getVoltage ( &voltage_R, 0);
	getVoltage ( &voltage_L, 1);
	
	if (voltage_R < 0.1)
	{
		getVoltage ( &voltage_R, 2);
		voltage_R = voltage_R / 5.0;
	}	
	
	if (voltage_L < 0.1)
	{
		getVoltage ( &voltage_L, 3);
		voltage_L = voltage_L / 3.0;
	}
	
	return;
}

void getDistance()
{
	distance_R = ( 9.0 / voltage_R );
	distance_L = ( 9.0 / voltage_L );
	return;
}

void main (void)
{
	printf("\n\nAT89LP51RB2 SPI Peak Detector Test using the MCP3004. \n\n");
	
	while (1)
	{
		getMaxVoltage();
		getDistance();
				
		printf("\n\n");
		printf("Peak V_R = %4.3f    ", voltage_R);
		printf("Peak V_L = %4.3f", voltage_L);
		printf("\n");
		printf("Distance_R = %4.3f ", distance_R);
		printf("Distance_L = %4.3f", distance_L);
		printf("\n\n");
	}
}
