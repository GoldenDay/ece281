#include <stdio.h>
#include <at89lp51rd2.h>
#include <stdlib.h>
#include <math.h>

// ~C51~ 
 
#define CLK 22118400L
#define BAUD 115200L
#define BRG_VAL (0x100-(CLK/(32L*BAUD)))

//We want timer 0 to interrupt every 100 microseconds ((1/10000Hz)=100 us)
#define FREQ 10000L
#define TIMER0_RELOAD_VALUE (65536L-(CLK/(12L*FREQ)))


/* ANSI colors */
#define	COLOR_BLACK		0
#define	COLOR_RED		1
#define	COLOR_GREEN		2
#define	COLOR_YELLOW	3
#define	COLOR_BLUE		4
#define	COLOR_MAGENTA	5
#define	COLOR_CYAN		6
#define	COLOR_WHITE		7



/* Some ANSI escape sequences */
#define CURSOR_ON "\x1b[?25h"
#define CURSOR_OFF "\x1b[?25l"
#define CLEAR_SCREEN "\x1b[2J"
#define GOTO_YX "\x1B[%d;%dH"
#define CLR_TO_END_LINE "\x1B[K"
/* Black foreground, white background */
#define BKF_WTB "\x1B[0;30;47m"
#define FORE_BACK "\x1B[0;3%d;4%dm"
#define FONT_SELECT "\x1B[%dm"

#define SPEED 100
#define FORWARD 1
#define BACKWARD 0


/*
� � � � � � � � � � � � � � � �

� � � � � � � � � � � � � � � �

� � � � � � � � � � � � � � � �
*/

//These variables are used in the ISR
volatile unsigned char pwmcount;
volatile float pwm1;
volatile float pwm2;
volatile float pwm3;
volatile float pwm4;
volatile float tentativepwm1;
volatile float tentativepwm2;
volatile float tentativepwm3;
volatile float tentativepwm4;
volatile float distance;
volatile float distance1;
volatile float distance2;


volatile int direction; // boolean variable for direction, 0 for CW, 1 for CCW
volatile int orientation; //turn 180 forward and backward
volatile float speed;
volatile float offsetspeed; //offset for 0<speed<5
volatile float rpm;

void Turn180(float speed);
void rotate(char direction, float speed, int orientation);
void move(int direction, float speed);
void waithalfus(void);
void follow(int d1, int d2);
float getD();
void setD();
float getD1();
float getD2();

unsigned char _c51_external_startup(void)
{
	// Configure ports as a bidirectional with internal pull-ups.
	P0M0=0;	P0M1=0;
	P1M0=0;	P1M1=0;
	P2M0=0;	P2M1=0;
	P3M0=0;	P3M1=0;
	AUXR=0B_0001_0001; // 1152 bytes of internal XDATA, P4.4 is a general purpose I/O
	P4M0=0;	P4M1=0;
    
    // Initialize the serial port and baud rate generator
    PCON|=0x80;
	SCON = 0x52;
    BDRCON=0;
    BRL=BRG_VAL;
    BDRCON=BRR|TBCK|RBCK|SPD;
	
	// Initialize timer 0 for ISR 'pwmcounter()' below
	TR0=0; // Stop timer 0
	TMOD=0x01; // 16-bit timer
	// Use the autoreload feature available in the AT89LP51RB2
	// WARNING: There was an error in at89lp51rd2.h that prevents the
	// autoreload feature to work.  Please download a newer at89lp51rd2.h
	// file and copy it to the crosside\call51\include folder.
	TH0=RH0=TIMER0_RELOAD_VALUE/0x100;
	TL0=RL0=TIMER0_RELOAD_VALUE%0x100;
	TR0=1; // Start timer 0 (bit 4 in TCON)
	ET0=1; // Enable timer 0 interrupt
	EA=1;  // Enable global interrupts
	
	pwmcount=0;
    
    return 0;
}

// Interrupt 1 is for timer 0.  This function is executed every time
// timer 0 overflows: 100 us.
void pwmcounter (void) interrupt 1
{
	if(++pwmcount>99) 
		pwmcount=0;
	P1_0=(pwm1>pwmcount)?1:0;
	P1_1=(pwm2>pwmcount)?1:0;
	P1_2=(pwm3>pwmcount)?1:0;
	P1_3=(pwm4>pwmcount)?1:0;
}

void main (void)
{
	pwm1=0; //at zero until user enters duty cycle
	pwm2=0;
	pwm3=0;
	pwm4=0;    
    orientation = FORWARD;
	
	
	while(1) {
	
		scanf("%c \n", &direction);
		
        
        if (direction == 'j'){ // if j is pressed then do 180
            Turn180(SPEED);
            if(orientation == FORWARD)
                orientation = BACKWARD; //orientation is backwards now
            else
                orientation = FORWARD; // if pressed again, then go back to forward
        }
            rotate(direction, SPEED, orientation);

		}
	
}

void rotate(char direction, float speed, int orientation)
{
    if(orientation == FORWARD){
        if (direction == 'd') {
            //Turning Clockwise/Right
            pwm1 = 0;
            pwm2 = speed;
            pwm3 = 0;
            pwm4 = speed;
        }
	
        else if (direction == 'a') {
            //Turning Counter-clockwise/Left
            pwm1 = speed;
            pwm2 = 0;
            pwm3 = speed;
            pwm4 = 0;
        }
	
        else if (direction == 's') {
            //Turning forwards
            pwm1 = speed;
            pwm2 = 0;
            pwm3 = 0;
            pwm4 = speed;
        }
	
        else if (direction == 'w') {
            //Turning backwards
            pwm1 = 0;
            pwm2 = speed;
            pwm3 = speed;
            pwm4 = 0;
        }
        else if (direction == ' ') {
            //Stop
            pwm1 = 0;
            pwm2 = 0;
            pwm3 = 0;
            pwm4 = 0;
        }
	
        else if (direction == 'k') { // do a parallel park
            //Turning 45 degrees to the right
            int wait;
            pwm1 = 0;
            pwm2 = speed;
            pwm3 = 0;
            pwm4 = 0;
		
            for (wait = 0; wait < 113; wait++) {
                waithalfus();
            }
		
            //move forward

            pwm1 = 0;
            pwm2 = speed;
            pwm3 = speed;
            pwm4 = 0;
		
            for (wait = 0; wait < 223; wait++) {
                waithalfus();
            }
		
            //Turning 45 degrees to the left
            pwm1 = 0;
            pwm2 = 0;
            pwm3 = speed;
            pwm4 = 0;
		
            for (wait = 0; wait < 110; wait++) {
                waithalfus();
            }
		
            //move backwards

            pwm1 = speed;
            pwm2 = 0;
            pwm3 = 0;
            pwm4 = speed;
		
            for (wait = 0; wait < 125; wait++) {
                waithalfus();
            }
		
		
			pwm1 = 0;
			pwm2 = 0;
			pwm3 = 0;
			pwm4 = 0;
		
			}
        else{
            pwm1 = 0;
            pwm2 = 0;
            pwm3 = 0;
            pwm4 = 0;
    	}
    }
    else {
        //BACKWARDS ORIENTATION
        if (direction == 'd') {//backwards right turn is same as forward right turn
            //Turning Clockwise/Right
            pwm1 = 0;
            pwm2 = speed;
            pwm3 = 0;
            pwm4 = speed;
        }
        
        else if (direction == 'a') {//backwards left turn is same as forward left turn

            //Turning Counter-clockwise/Left
            pwm1 = speed;
            pwm2 = 0;
            pwm3 = speed;
            pwm4 = 0;
        }
        
        else if (direction == 's') {//backwards reverse is opposite to forward backward
            //Turning forwards
            pwm1 = 0;
            pwm2 = speed;
            pwm3 = speed;
            pwm4 = 0;
        }
        
        else if (direction == 'w') {//backwards forward is opposite to forward forward
            //Turning backwards
            pwm1 = speed;
            pwm2 = 0;
            pwm3 = 0;
            pwm4 = speed;
        }
        else if (direction == ' ') {
            //Stop
            pwm1 = 0;
            pwm2 = 0;
            pwm3 = 0;
            pwm4 = 0;
        }
        
        else if (direction == 'k') { // do a parallel park
            //Turning 45 degrees to the right
            int wait;
            pwm1 = 0;
            pwm2 = 0;
            pwm3 = 0;
            pwm4 = speed;
            
            for (wait = 0; wait < 113; wait++) {
                waithalfus();
            }
            
            //move forward
            //opposite of forward forward
            pwm1 = speed;
            pwm2 = 0;
            pwm3 = 0;
            pwm4 = speed;
            
            for (wait = 0; wait < 223; wait++) {
                waithalfus();
            }
            
            //Turning 45 degrees to the left
            pwm1 = 0;
            pwm2 = 0;
            pwm3 = speed;
            pwm4 = 0;
            
            for (wait = 0; wait < 110; wait++) {
                waithalfus();
            }
            
            //move backwards

            pwm1 = 0;
            pwm2 = speed;
            pwm3 = speed;
            pwm4 = 0;
		
            for (wait = 0; wait < 125; wait++) {
                waithalfus();
            }
		
		
			pwm1 = 0;
			pwm2 = 0;
			pwm3 = 0;
			pwm4 = 0;
		
            
        }
        else{
            pwm1 = 0;
            pwm2 = 0;
            pwm3 = 0;
            pwm4 = 0;
        }
    }

}

//Robot logic lecture 1 page 21
//global variable for set distance
void follow(int d1, int d2) {
    if (orientation == FORWARD) {
    	if (d1 > distance) {
    		//Turning 45 degrees to the right (move motor 1 forward)
    		pwm1 = 0;
    		pwm2 = speed;
    		pwm3 = 0;
    		pwm4 = 0;
    	}
    	else if (d2 > distance) {
    		//Turning 45 degrees to the left (move motor 2 forward)
    		pwm1 = 0;
    		pwm2 = 0;
    		pwm3 = speed;
    		pwm4 = 0;
    	}
    	else if (d1 < distance) {
    		//Turning 45 degrees to the left (move motor 1 backward)
    		pwm1 = speed;
    		pwm2 = 0;
    		pwm3 = 0;
    		pwm4 = 0;
    	}
    	else if (d2 < distance) {
    		//Turning 45 degrees to the right (move motor 2 backward)
    		pwm1 = 0;
    		pwm2 = 0;
    		pwm3 = 0;
    		pwm4 = speed;
    	}
    }
    else if (orientation == BACKWARD) {
        if (d1 > distance) {
            //Turning 45 degrees to the left (move motor 2 forward)
            pwm1 = speed;
            pwm2 = 0;
            pwm3 = 0;
            pwm4 = 0;
        }
        else if (d2 > distance) {
            //Turning 45 degrees to the right(move motor 1 forward)
            pwm1 = 0;
            pwm2 = 0;
            pwm3 = 0;
            pwm4 = speed;
        }
        else if (d1 < distance) {
            //Turning 45 degrees to the left (move motor 2 backward)
            pwm1 = 0;
            pwm2 = speed;
            pwm3 = 0;
            pwm4 = 0;
        }
        else if (d2 < distance) {
            //Turning 45 degrees to the right (move motor 2 backward)
            pwm1 = 0;
            pwm2 = 0;
            pwm3 = speed;
            pwm4 = 0;
        }

    }
}

void Turn180(float speed){
    //Turning 180
    int wait;
    pwm1 = 0;
    pwm2 = speed;
    pwm3 = 0;
    pwm4 = speed;
    
    for (wait = 0; wait < 165; wait++) {
        waithalfus();
    }
    
    pwm1 = 0;
    pwm2 = 0;
    pwm3 = 0;
    pwm4 = 0;
    
    return;
}
/*
 * reads the inputs from the analog board buttons and converts them into signals.
 * button are temporary placeholders for actual button pin numbers
 * on = 1, 0ff = 0
 */
float getD(void) {
    int button1 = 1;
    int button2 = 2;
    int button3 = 3;
    int button4 = 4;
	float distance;
    
    if( button1 == 1)
        distance = 20;
    
    else if( button2 == 1)
        distance = 30;
    
    else if ( button3 == 1)
        distance = 40;
    
    else if (button4 == 1)
        distance = 50;
    
    else
        distance = 10; //******DISTANCE CANNOT BE ZERO OR THE CAR WILL GO TOWARD THE RECEIVER. needs atleast a minimum distance. (temporatily 10)
	
    return distance;
}

float getD1() {
	return distance1;
}

float getD2() {
	return distance2;
}

void move(int direction, float speed) 
{
	if (direction == 0) {
		//Go backwards
		pwm1 = 0;
		pwm2 = speed;
		pwm3 = speed;
		pwm4 = 0;
	}
	
	else {
		//Go forwards
		pwm1 = speed;
		pwm2 = 0;
		pwm3 = 0;
		pwm4 = speed;	
	}
}
float GetDistance(void){
    return 1;
}


void waithalfus(void){
	_asm	
		;For a 22.1184MHz crystal one machine cycle takes 12/22.1184MHz=0.5425347us
	    ;mov TR0, #1
	    ;mov R7, #9
	    ;mov TR0, #0
	    push AR1
	    mov R1, #1
	    pop AR1
	    ret
    _endasm;
}


