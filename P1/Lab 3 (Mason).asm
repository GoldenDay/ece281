$MODDE2
org 0000H
   ljmp MyProgram
   
dseg at 30h
x:    	ds 2
y:    	ds 2
z:		ds 2
bcd:  	ds 3
op:		ds 1

bseg
mf:		dbit 1		

FREQ   EQU 33333333
BAUD   EQU 115200
T2LOAD EQU 65536-(FREQ/(32*BAUD))

CSEG

MISO   EQU  P0.0 
MOSI   EQU  P0.1 
SCLK   EQU  P0.2
CE_ADC EQU  P0.3
CE_EE  EQU  P0.4
CE_RTC EQU  P0.5 

myLUT:
    DB 0C0H, 0F9H, 0A4H, 0B0H, 099H        ; 0 TO 4
    DB 092H, 082H, 0F8H, 080H, 090H        ; 4 TO 9
    
Load_y MAC
	mov y+0, #low (%0) 
	mov y+1, #high(%0) 
ENDMAC

Display:
	mov dptr, #myLUT
	; Display Digit 0
    mov A, bcd+0
    anl a, #0fh
    movc A, @A+dptr
    mov HEX0, A
	; Display Digit 1
    mov A, bcd+0
    swap a
    anl a, #0fh
    movc A, @A+dptr
    mov HEX1, A
    ret

INIT_SPI:
    orl P0MOD, #00000110b 		; Set SCLK, MOSI as outputs
    anl P0MOD, #11111110b 		; Set MISO as input
    clr SCLK              		; For mode (0,0) SCLK is zero
	ret
	
DO_SPI_G:
	push acc
    mov R1, #0            		; Received byte stored in R1
    mov R2, #8            		; Loop counter (8-bits)
DO_SPI_G_LOOP:
    mov a, R0             		; Byte to write is in R0
    rlc a                 		; Carry flag has bit to write
    mov R0, a
    mov MOSI, c
    setb SCLK            	 	; Transmit
    mov c, MISO           		; Read received bit
    mov a, R1             		; Save received bit in R1
    rlc a
    mov R1, a
    clr SCLK
    djnz R2, DO_SPI_G_LOOP
    pop acc
    ret

Delay:
	mov R3, #20
Delay_loop:
	djnz R3, Delay_loop
	ret

; Configure the serial port and baud rate using timer 2
InitSerialPort:
	clr TR2 ; Disable timer 2
	mov T2CON, #30H ; RCLK=1, TCLK=1 
	mov RCAP2H, #high(T2LOAD)  
	mov RCAP2L, #low(T2LOAD)
	setb TR2 ; Enable timer 2
	mov SCON, #52H
	ret

; Send a character through the serial port
putchar:
    JNB TI, putchar
    CLR TI
    MOV SBUF, a
    RET

; Send a constant-zero-terminated string through the serial port
hyperterminal:
    CLR A
    
    ;mov a, r0
    ;jb acc.0, voltage 
    
    mov dptr, #Numbers
    
	; Display Digit 1
    mov A, bcd+0
    swap a
    anl a, #0fh
    orl a, #30h
    lcall putchar

	; Display Digit 0
    mov A, bcd+0
    anl a, #0fh
    orl a, #30h
    lcall putchar

	mov a, #0
	movc a, @a+dptr
	lcall putchar
	
	;mov a, #1
	;movc a, @a+dptr
	;lcall putchar
	;jmp later
	
;voltage:
	; Display Digit 1
    ;mov A, bcd+0
    ;swap a
    ;anl a, #0fh
    ;orl a, #30h
    ;lcall putchar

	; Display Digit 0
    ;mov A, bcd+0
    ;anl a, #0fh
    ;orl a, #30h
    ;lcall putchar   

;later:
	
    ret
    
; Channel to read passed in register b
Read_ADC_Channel:
	clr CE_ADC
	mov R0, #00000001B 			; Start bit:1
	lcall DO_SPI_G
	
	mov a, b
	swap a
	anl a, #0F0H
	setb acc.7 					; Single mode (bit 7).
	
	mov R0, a 					; Select channel
	lcall DO_SPI_G
	mov a, R1          			; R1 contains bits 8 and 9
	anl a, #03H
	mov R7, a
	
	mov R0, #55H 				; It doesn't matter what we transmit...
	lcall DO_SPI_G
	mov a, R1    				; R1 contains bits 0 to 7
	mov R6, a
	setb CE_ADC
	ret
 
Numbers:
	DB  '\n'
	DB	'\r'
    DB  '.' 
    DB  '2 ','\n', '\r' 
    DB  '3 ','\n', '\r' 
    DB  '4 ','\n', '\r' 
    DB  '5 ','\n', '\r' 
    DB  '6 ','\n', '\r' 
    DB  '7 ','\n', '\r' 
    DB  '8 ','\n', '\r' 
    DB  '9 ','\n', '\r'

MyProgram:
    MOV SP, #7FH
    mov LEDRA, #0
    mov LEDRB, #0
    mov LEDRC, #0
    mov LEDG, #0
    
    orl P0MOD, #00111000b 		; make all CEs outputs

	setb CE_ADC
	setb CE_EE
	clr  CE_RTC 				; RTC CE is active high

	lcall INIT_SPI    

forever:
    
    mov b, #0  					; Read channel 0
	lcall Read_ADC_Channel
	
	lcall InitSerialPort
    
    mov mf, #0
    mov r0, #0
    
    mov x+1, r7
    mov x+0, r6
    
    Load_y(62)
	lcall mul16
	mov R4, x+1

	mov x+1, R7
	mov x+0, R6

	Load_y(63)
	lcall mul16
	mov R5, x+1
	
	mov x+0, R4
	mov x+1, #0
	mov y+0, R5
	mov y+1, #0
	lcall add16
	
	Load_y(273)
	lcall sub16
	
	mov z+0, x+0
	mov z+1, x+1
	
	Load_y(0)
	lcall x_lteq_y
	mov a, mf
	jz skipzero
	mov x+0, #0
	mov x+1, #0

skipzero:
	
	lcall hex2bcd
    lcall hyperterminal

	lcall display
	
	;mov r0, #1
	
	;mov x+0, z+0
	;mov x+1, z+1
	
	;Load_y(3)
	;lcall mul16
	
	;Load_y(25)
	;lcall xchg_xy
	;lcall div16
	
	;lcall hex2bcd
	;lcall hyperterminal
	
	
	lcall waithalfsec
	lcall waithalfsec				

    sjmp forever

WaitHalfSec:
	mov R2, #90
L6:	mov R1, #250
L5:	mov R0, #250
L4:	djnz R0, L4 ; 3 machine cycles-> 3*30ns*250=22.5us
	djnz R1, L5 ; 22.5us*250=5.62ms
	djnz R2, L5 ; 5.62ms*90=0.5s (approximately)
	ret
	
WaitOneSec:
	mov R2, #180
L9:	mov R1, #250
L8:	mov R0, #250
L7:	djnz R0, L7 ; 3 machine cycles-> 3*30ns*250=22.5us
	djnz R1, L8 ; 22.5us*250=5.62ms
	djnz R2, L8 ; 5.62ms*90=0.5s (approximately)
	ret
    
   
;----------------------------------------------------
; Converts the 16-bit hex number in 'x' to a 
; 5-digit packed BCD in 'bcd' using the
; double-dabble algorithm.
;---------------------------------------------------
hex2bcd:
	push acc
	push psw
	push AR0
	
	clr a
	mov bcd+0, a ; Initialize BCD to 00-00-00 
	mov bcd+1, a
	mov bcd+2, a
	mov r0, #16  ; Loop counter.

hex2bcd_L0:
	; Shift binary left	
	mov a, x+1
	mov c, acc.7 ; This way x remains unchanged!
	mov a, x+0
	rlc a
	mov x+0, a
	mov a, x+1
	rlc a
	mov x+1, a
    
	; Perform bcd + bcd + carry using BCD arithmetic
	mov a, bcd+0
	addc a, bcd+0
	da a
	mov bcd+0, a
	mov a, bcd+1
	addc a, bcd+1
	da a
	mov bcd+1, a
	mov a, bcd+2
	addc a, bcd+2
	da a
	mov bcd+2, a

	djnz r0, hex2bcd_L0

	pop AR0
	pop psw
	pop acc
	ret
	
;------------------------------------------------
; bcd2hex:
; Converts the 5-digit packed BCD in 'bcd' to a 
; 16-bit hex number in 'x'
;------------------------------------------------

rrc_and_correct:
    rrc a
    push psw  ; Save carry (it is changed by the add instruction)
    jnb acc.7, nocor1
    add a, #(100H-30H) ; subtract 3 from packed BCD MSD
nocor1:
    jnb acc.3, nocor2
    add a, #(100H-03H) ; subtract 3 from packed BCD LSD
nocor2:
    pop psw   ; Restore carry
    ret

bcd2hex:
	push acc
	push psw
	push AR0
	push bcd+0
	push bcd+1
	push bcd+2
	mov R0, #16 ;Loop counter.
	
bcd2hex_L0:
    ; Divide BCD by two
    clr c
    mov a, bcd+2
    lcall rrc_and_correct
    mov bcd+2, a
    mov a, bcd+1
    lcall rrc_and_correct
    mov bcd+1, a
    mov a, bcd+0
    lcall rrc_and_correct
    mov bcd+0, a
    ;Shift R0-R1 right through carry
    mov a, x+1
    rrc a
    mov x+1, a
    mov a, x+0
    rrc a
    mov x+0, a
    djnz R0, bcd2hex_L0
    
    pop bcd+2
    pop bcd+1
    pop bcd+0
	pop AR0
	pop psw
	pop acc
	ret
	
;------------------------------------------------
; x = x - y
;------------------------------------------------
sub16:
	push acc
	push psw
	clr c
	mov a, x+0
	subb a, y+0
	mov x+0, a
	mov a, x+1
	subb a, y+1
	mov x+1, a
	pop psw
	pop acc
	ret

;------------------------------------------------
; x = x + y
;------------------------------------------------
add16:
	push acc
	push psw
	mov a, x+0
	add a, y+0
	mov x+0, a
	mov a, x+1
	addc a, y+1
	mov x+1, a
	pop psw
	pop acc
	ret
;------------------------------------------------
; x = x * y
;------------------------------------------------
mul16:
	push acc
	push b
	push psw
	push AR0
	push AR1
		
	; R0 = x+0 * y+0
	; R1 = x+1 * y+0 + x+0 * y+1
	
	; Byte 0
	mov	a,x+0
	mov	b,y+0
	mul	ab		; x+0 * y+0
	mov	R0,a
	mov	R1,b
	
	; Byte 1
	mov	a,x+1
	mov	b,y+0
	mul	ab		; x+1 * y+0
	add	a,R1
	mov	R1,a
	clr	a
	addc a,b
	mov	R2,a
	
	mov	a,x+0
	mov	b,y+1
	mul	ab		; x+0 * y+1
	add	a,R1
	mov	R1,a
	
	mov	x+1,R1
	mov	x+0,R0

	pop AR1
	pop AR0
	pop psw
	pop b
	pop acc
	
	ret
	
x_lt_y:
	push acc
	push psw
	clr c
	mov a, x+0
	subb a, y+0
	mov a, x+1
	subb a, y+1
	mov mf, c
	pop psw
	pop acc
	ret

;------------------------------------------------
; x = x / y
; This subroutine uses the 'paper-and-pencil' 
; method described in page 139 of 'Using the
; MCS-51 microcontroller' by Han-Way Huang.
;------------------------------------------------
div16:
	push acc
	push psw
	push AR0
	push AR1
	push AR2
	
	mov	R2,#16 ; Loop counter
	clr	a
	mov	R0,a
	mov	R1,a
	
div16_loop:
	; Shift the 32-bit of [R1, R0, x+1, x+0] left:
	clr c
	; First shift x:
	mov	a,x+0
	rlc a
	mov	x+0,a
	mov	a,x+1
	rlc	a
	mov	x+1,a
	; Then shift [R1,R0]:
	mov	a,R0
	rlc	a 
	mov	R0,a
	mov	a,R1
	rlc	a
	mov	R1,a
	
	; [R1, R0] - y
	clr c	     
	mov	a,R0
	subb a,y+0
	mov	a,R1
	subb a,y+1
	
	jc	div16_minus		; temp >= y?
	
	; -> yes;  [R1, R0] -= y;
	; clr c ; carry is always zero here because of the jc above!
	mov	a,R0
	subb a,y+0 
	mov	R0,a
	mov	a,R1
	subb a,y+1
	mov	R1,a
	
	; Set the least significant bit of x to 1
	orl	x+0,#1
	
div16_minus:
	djnz R2, div16_loop	; -> no
	
div16_exit:

	pop AR2
	pop AR1
	pop AR0
	pop psw
	pop acc
	
	ret
	
;------------------------------------------------
; mf=1 if x >= y
;------------------------------------------------
x_gteq_y:
	lcall x_eq_y
	jb mf, x_gteq_y_done
	ljmp x_gt_y
x_gteq_y_done:
	ret
	
xchg_xy:
	mov a, x+0
	xch a, y+0
	mov x+0, a
	mov a, x+1
	xch a, y+1
	mov x+1, a
	ret
	
;------------------------------------------------
; mf=1 if x > y
;------------------------------------------------
x_gt_y:
	push acc
	push psw
	clr c
	mov a, y+0
	subb a, x+0
	mov a, y+1
	subb a, x+1
	mov mf, c
	pop psw
	pop acc
	ret

;------------------------------------------------
; mf=1 if x = y
;------------------------------------------------
x_eq_y:
	push acc
	push psw
	clr mf
	clr c
	mov a, y+0
	subb a, x+0
	jnz x_eq_y_done
	mov a, y+1
	subb a, x+1
	jnz x_eq_y_done
	setb mf
x_eq_y_done:
	pop psw
	pop acc
	ret
	
x_lteq_y:
	lcall x_eq_y
	jb mf, x_lteq_y_done
	ljmp x_lt_y
x_lteq_y_done:
	ret

END
