; Uses clk_v3.asm template from EECE 259 notes 

; clk_v3.asm: Displays seconds, minutes, and hours in HEX2 to HEX7
; We can set the time by flipping SW0 and using KEY.3, KEY.2, KEY.1
; to increment the Hours, Minutes, and Seconds.

$MODDE2

org 0000H ; 00000000B
	ljmp myprogram
	
org 000BH ; 00001011B
	ljmp ISR_Timer0
	
org 002BH ;  timer 1 interupt service routine ; make a timer 2 for alarm
	ljmp ISR_Timer2
	
BSEG at 20H
AP_FLAG:	dbit 1	; flag for AM/PM, 1 for AM, 0 for PM
ALM_AP_FLAG: dbit 1
BUZZ_FLAG: dbit 1 ; flag for buzzer on/off
	
DSEG at 30H
count10ms: ds 1
seconds:   ds 1
minutes:   ds 1
hours:     ds 1
alm_seconds: ds 1
alm_minutes: ds 1
alm_hours: 	ds 1

CSEG

; Look-up table for 7-segment displays
myLUT:
    DB 0C0H, 0F9H, 0A4H, 0B0H, 099H
    DB 092H, 082H, 0F8H, 080H, 090H
    DB 0FFH ; All segments off
    
    
;FREQ_2 EQU 10000 ; this changes the pitch of the alarm but also makes the timer go up super fast
;	mov P0MOD, #00000011b ; P0.0, P0.1 are outputs.  P0.1 is used for testing Timer 2!
;	setb P0.0
XTAL           EQU 33333333
FREQ           EQU 100 ; change this to change the pitch of the alarm and the speed of the timer 
ALM_FREQ	   EQU 25000
TIMER0_RELOAD  EQU 65538-(XTAL/(12*FREQ))
TIMER2_RELOAD  EQU 65538-(XTAL/(12*ALM_FREQ))

Wait_HalfSec:
	mov R2, #90
N3: mov R1, #250
N2: mov R0, #250
N1: djnz R0, N1
	djnz R1, N2
	djnz R2, N3
	ret

Wait_Delay:
	mov R2, #90
L3: mov R1, #90
L2: mov R0, #90
L1: djnz R0, L1
	djnz R1, L2
	djnz R2, L3
	ret

ISR_Timer1_L0_Transition:
    ljmp ISR_Timer1_L0

ISR_Timer0:
	; Reload the timer
	
	
	;jnb BUZZ_FLAG, SKIP_BUZZ
	;jb BUZZ_FLAG, $
	
	
;SKIP_BUZZ:
    mov TH0, #high(TIMER0_RELOAD)
    mov TL0, #low(TIMER0_RELOAD)
    
    ;mov TH1, #high(TIMER0_RELOAD)
    ;mov TL1, #low(TIMER0_RELOAD)
    
    ; Save used register into the stack
    push psw
    push acc
    push dph
    push dpl
    
    jb SWA.0, ISR_Timer0_L0 ; Setting up time.  Do not increment anything
    jb SWA.1, ISR_Timer1_L0_Transition ; Set up alarm, don't increment either
    
    
    ; Increment the counter and check if a second has passed
    inc count10ms
    mov a, count10ms
    cjne A, #100, ISR_Timer0_L0
    mov count10ms, #0
    
    mov a, seconds
    add a, #1
    da a
    mov seconds, a
    cjne A, #60H, ISR_Timer0_L0
    mov seconds, #0

    mov a, minutes
    add a, #1
    da a
    mov minutes, a
    cjne A, #60H, ISR_Timer0_L0
    mov minutes, #0

    mov a, hours
    add a, #1
    da a
    mov hours, a
    

    cjne A, #12H, SKIP_AP ; change from A to P or vice versa if 12 o clock
    lcall CPL_AP_FLAG
    

    
SKIP_AP:    
    cjne A, #13H, ISR_Timer0_L0
    mov hours, #1
    
    
	; Adjust the time
ISR_Timer0_L0:
	
    jb ALM_AP_FLAG, CHK_1 ; checks that both AP flags for time and alarm match or not
    jnb ALM_AP_FLAG, CHK_2
CHK_1: 
	jb AP_FLAG, TRUE
	jnb AP_FLAG, SKIP_BUZZ
CHK_2:
    jnb AP_FLAG, TRUE
    jb AP_FLAG, SKIP_BUZZ
TRUE:
    mov a, alm_hours
	mov b, hours
	cjne a, b, SKIP_BUZZ ; continue down if equal, jump to not equal otherwise
	mov a, alm_minutes
	mov b, minutes
	cjne a, b, SKIP_BUZZ
	mov a, alm_seconds
	mov b, seconds
	cjne a, b, SKIP_BUZZ
	lcall ALARM_ON
	
SKIP_BUZZ:
	lcall DISPLAY_AP
	
	; Update the display.  This happens every 10 ms
	mov dptr, #myLUT
	
	; seconds
	mov a, seconds
	anl a, #0fH
	movc a, @a+dptr
	mov HEX2, a
	mov a, seconds
	swap a
	anl a, #0fH
	movc a, @a+dptr
	mov HEX3, a
	
	; minutes
	mov a, minutes
	anl a, #0fH
	movc a, @a+dptr
	mov HEX4, a
	mov a, minutes
	swap a
	anl a, #0fH
	movc a, @a+dptr
	mov HEX5, a

	; hours
	mov a, hours
	anl a, #0fH
	movc a, @a+dptr
	mov HEX6, a
	mov a, hours
	jb acc.4, ISR_Timer0_L1
	mov a, #0A0H
	
ISR_Timer0_L1:

	swap a
	anl a, #0fH
	movc a, @a+dptr
	mov HEX7, a

	; Restore used registers
	pop dpl
	pop dph
	pop acc
	pop psw    
	reti
	
;ISR_Timer1:
 ; 	push psw
   ; push acc
    ;push dph
    ;push dpl
    ;mov TH1, #high(TIMER0_RELOAD)
    ;mov TL1, #low(TIMER0_RELOAD)
	;setb LEDRA.0
	
	
	; Adjust the alarm
ISR_Timer1_L0:
	lcall ALM_DISPLAY_AP
	


	; Update the display.  This happens every 10 ms
	mov dptr, #myLUT
	
	; seconds
	mov a, alm_seconds
	anl a, #0fH
	movc a, @a+dptr
	mov HEX2, a
	mov a, alm_seconds
	swap a
	anl a, #0fH
	movc a, @a+dptr
	mov HEX3, a
	
	; minutes
	mov a, alm_minutes
	anl a, #0fH
	movc a, @a+dptr
	mov HEX4, a
	mov a, alm_minutes
	swap a
	anl a, #0fH
	movc a, @a+dptr
	mov HEX5, a

	; hours
	mov a, alm_hours
	anl a, #0fH
	movc a, @a+dptr
	mov HEX6, a
	mov a, alm_hours
	jb acc.4, ISR_Timer1_L1
	mov a, #0A0H
	

ISR_Timer1_L1:
	swap a
	anl a, #0fH
	movc a, @a+dptr
	mov HEX7, a

	; Restore used registers
	pop dpl
	pop dph
	pop acc
	pop psw    
	reti
	
ISR_Timer2:
  	push psw
    push acc
    push dph
    push dpl
    ;mov TH2, #high(TIMER0_RELOAD)
    ;mov TL2, #low(TIMER0_RELOAD)
    ; constantly check if buzzer is on
    
    jnb SWA.3, TURN_ALARM_OFF
    jb SWA.3, TURN_ALARM_ON
	
TURN_ALARM_ON:
	
	lcall ENABLE_ALARM
	sjmp CONTINUE_1

TURN_ALARM_OFF:
	lcall DISABLE_ALARM
	clr BUZZ_FLAG
	
CONTINUE_1:
	jnb SWA.4, KEEP_ON
    jb SWA.4, SHUT_OFF
    
    
    
	
KEEP_ON:
	sjmp CONTINUE_2

SHUT_OFF:
	clr BUZZ_FLAG
	lcall LCD_DISPLAY_TURNED_OFF
	

    
	
CONTINUE_2:
	jnb SWA.5, jump_over ; check for snooze
    jb SWA.5, SNZ_SHUT_OFF

jump_over:
    clr TF2
    jnb BUZZ_FLAG, DONT_BUZZ  
	cpl P0.0 ; buzz the alarm
	sjmp dont_buzz
	;lcall wait_delay
	;cpl p0.0


SNZ_SHUT_OFF:
	clr BUZZ_FLAG
	lcall LCD_DISPLAY_SNOOZE
	lcall Wait_HalfSec
	lcall Wait_HalfSec
	mov a, seconds
    add a, #1
    da a
    mov seconds, a
    cjne A, #60H, Z1
    mov seconds, #0
    
Z1:    
	lcall Wait_HalfSec
	lcall Wait_HalfSec
	mov a, seconds
    add a, #1
    da a
    mov seconds, a
    cjne A, #60H, Z2
    mov seconds, #0
    
Z2:
	lcall Wait_HalfSec
	lcall Wait_HalfSec
	mov a, seconds
    add a, #1
    da a
    mov seconds, a
    cjne A, #60H, Z3
    mov seconds, #0
    
Z3:
	lcall Wait_HalfSec
	lcall Wait_HalfSec ; 5 second snooze
	mov a, seconds
    add a, #1
    da a
    mov seconds, a
    cjne A, #60H, Z4
    mov seconds, #0
    
Z4:    
	lcall Wait_HalfSec 
	lcall Wait_HalfSec 
	mov a, seconds
    add a, #1
    da a
    mov seconds, a
    cjne A, #60H, CONTINUE_THIS	
    mov seconds, #0

	
CONTINUE_THIS:	
	setb BUZZ_FLAG
    clr TF2
    jnb BUZZ_FLAG, DONT_BUZZ  
	cpl P0.0 ; buzz the alarm
	;lcall wait_delay
	;cpl p0.0
	
	
	
DONT_BUZZ:
	pop dpl
	pop dph
	pop acc
	pop psw
	reti
	

	
Init_Timer0:	
	mov TMOD,  #00010001B ; GATE=0, C/T*=0, M1=0, M0=1: 16-bit timer
	clr TR0 ; Disable timer 0
	clr TF0
    mov TH0, #high(TIMER0_RELOAD)
    mov TL0, #low(TIMER0_RELOAD)
    setb TR0 ; Enable timer 0
    setb ET0 ; Enable timer 0 interrupt
    ret
  
; timer 1 isn't used in this program
Init_Timer1:
	;mov TMOD,  #00010000B ; GATE=0, C/T*=0, M1=0, M0=1: 16-bit timer
	clr TR1 ; Disable timer 1
	clr TF1
    mov TH1, #high(TIMER0_RELOAD)
    mov TL1, #low(TIMER0_RELOAD)
    setb TR1 ; Enable timer 1
    setb ET1 ; Enable timer 1 interrupt
    ret
    
Init_Timer2:
	;mov TMOD,  #00010000B ; GATE=0, C/T*=0, M1=0, M0=1: 16-bit timer
	mov T2CON, #00H ; Autoreload is enabled, work as a timer
    clr TR2
    clr TF2
    ; Set up timer 2 to interrupt every 10ms
    mov RCAP2H,#high(TIMER2_RELOAD) ; is like TH2, TL2
    mov RCAP2L,#low(TIMER2_RELOAD)
    setb TR2 ; Enable timer 2
    setb ET2 ; Enable timer 2 interrupt
    ret

myprogram:
	mov SP, #7FH
	
	lcall ALARM_OFF
	;lcall ALARM_ON ; testing alarm have it on
	
	
	mov P0MOD, #00000001B ; P0.0 is output.
	setb P0.0
	
	setb AP_FLAG
	setb ALM_AP_FLAG
	lcall DISPLAY_AP
	
	
	    
    ; Turn LCD on, and wait a bit.
    setb LCD_ON
    clr LCD_EN  ; Default state of enable must be zero
    lcall Wait40us
    
    mov LCD_MOD, #0xff ; Use LCD_DATA as output port
    clr LCD_RW ;  Only writing to the LCD in this code.
	
	mov a, #0ch ; Display on command
	lcall LCD_command
	mov a, #38H ; 8-bits interface, 2 lines, 5x7 characters
	lcall LCD_command
	mov a, #01H ; Clear screen (Warning, very slow command!)
	lcall LCD_command
    
    ; Delay loop needed for 'clear screen' command above (1.6ms at least!)
    mov R1, #40
	
	

	
	mov LEDRA,#0
	mov LEDRB,#0
	mov LEDRC,#0
	mov LEDG,#0
	
	
	mov seconds, #00H
	mov minutes, #00H
	mov hours, #12H
	
	mov alm_seconds, #00H
	mov alm_minutes, #00H
	mov alm_hours, #12H

	lcall Init_Timer0
	lcall Init_Timer1
	lcall Init_Timer2
    setb EA  ; Enable all interrupts

    ;jnb SWA.0, M0
    ;jnb SWA.1, Alarm_0 
    
    ; found the problem, if alarm is on top, then alarm changes fine, but time can't be changed
    ; solution is to use an interrupt service routine for this as well
    
    
; is checking only M's, not getting to alarms


	
STRT_CHK:
	;lcall DISPLAY_AP ; not sure if this is being repeated...it is...
	;lcall alarm_on
	
	
	clr LEDRA.1
	jnb SWA.0, CONTINUE ; will stay at M0 and not do anything if SW0 not enabled
	jb SWA.0, CHNG_TIME
	
CONTINUE:
	clr LEDRA.0
	jnb SWA.1, STRT_CHK
	jb SWA.1, CHNG_ALARM

CHNG_TIME:
	setb LEDRA.0
	jnb SWA.2, SKIP_1
	jb SWA.2, $	
		
	cpl AP_FLAG
	
SKIP_1:
	
	jb KEY.3, M1 ; if hour not pushed, check for minutes being pushed
	; active segment is 00001011B    
    jnb KEY.3, $ ; continue program only if hour button pushed
    ; the dollar sign indicates the value of the location counter in the active segment. 
    ; When using the $ symbol, keep in mind that its value changes with each instruction, 
    ; but only after that instruction has been completely evaluated. If you use $ as an 
    ; operand of an instruction or directive, it represents the address of the first byte 
    ; of that instruction.
    
    
    mov a, hours
	add a, #1
	da a
	mov hours, a
    cjne A, #13H, M1
    mov hours, #1

M1:	
	jb KEY.2, M2 ; if minutes not pushed, check if seconds pushed
    jnb KEY.2, $ ; if minutes pushed, continue and add to minutes
    mov a, minutes
	add a, #1
	da a
	mov minutes, a
    cjne A, #60H, M2 ; not equal to 60, continue and check if hours pushed
    mov minutes, #1 ; if equal to 60, reset to zero

M2:	
	jb KEY.1, RTRN ; repeat and check if hours pushed if seconds not pushed
	jnb KEY.1, $ ; continue only if seconds pushed
	mov a, seconds
	add a, #1
	da a
	mov seconds, a
    cjne A, #60H, RTRN
    mov seconds, #1
    
;CHNG_AP:
;	lcall CPL_AP_FLAG
;	ret
    
;CHNG_AP_FLAG: ; this MAY be redundant and useless
;	jnb SWA.2, RTRN ; repeat and check if hours pushed if seconds not pushed
;	jb SWA.2, $ ; continue only if seconds pushed
;	mov a, seconds
;	add a, #1
;	da a
;	mov seconds, a
 ;   cjne A, #60H, RTRN
  ;  mov seconds, #1

CHNG_ALARM:
	setb LEDRA.1
	lcall ALM_DISPLAY_AP
	jnb SWA.2, SKIP_ALM_1
	jb SWA.2, $	
		
	cpl ALM_AP_FLAG
	
SKIP_ALM_1:
	
	jb KEY.3, Alarm_1
    jnb KEY.3, $
    mov a, alm_hours
	add a, #1
	da a
	mov alm_hours, a
    cjne A, #13H, Alarm_1
    mov alm_hours, #1

Alarm_1:	
	jb KEY.2, Alarm_2
    jnb KEY.2, $
    mov a, alm_minutes
	add a, #1
	da a
	mov alm_minutes, a
    cjne A, #60H, Alarm_2
    mov alm_minutes, #0

Alarm_2:	
	jb KEY.1, RTRN
	jnb KEY.1, $
	mov a, alm_seconds
	add a, #1
	da a
	mov alm_seconds, a
    cjne A, #60H, RTRN
    mov alm_seconds, #0
    
RTRN:
	ljmp STRT_CHK
	
DISPLAY_AP:
    jb AP_FLAG, SET_AM
    jnb AP_FLAG, SET_PM
    ret

ALM_DISPLAY_AP:
    jb ALM_AP_FLAG, SET_ALM_AM
    jnb ALM_AP_FLAG, SET_ALM_PM
    ret

CPL_AP_FLAG:
	cpl AP_FLAG
	ret
	
SET_AM: ; must find bug where it is displaying timer_AP when in alarm mode
	mov HEX0, #11111111B
	mov HEX0, #10001000B ; displays A in hex 0
	ret

SET_PM:
	mov HEX0, #11111111B
	mov HEX0, #00001100B ; displays P in hex 0
	ret
	
CPL_ALM_AP_FLAG:
	cpl ALM_AP_FLAG
	ret
	
SET_ALM_AM:
	mov HEX0, #11111111B
	mov HEX0, #10001000B ; displays A in hex 0
	ret

SET_ALM_PM:
	mov HEX0, #11111111B
	mov HEX0, #00001100B ; displays P in hex 0
	ret
	
ALARM_ON:
	setb BUZZ_FLAG
	;setb P0.0
	;setb LEDRA.0
	ret
	
ALARM_OFF:
	clr BUZZ_FLAG
	ret
	
ENABLE_ALARM:	
	mov P0MOD, #00000001B
	setb LEDRA.3
	lcall lcd_display_alm_on ; lcd message
	ret
	
DISABLE_ALARM:
	mov P0MOD, #00000000B ; disables alarm output
	clr LEDRA.3
	lcall lcd_display_alm_off
	ret
	
	Wait40us:
	mov R0, #149
X1: 
	nop
	nop
	nop
	nop
	nop
	nop
	djnz R0, X1 ; 9 machine cycles-> 9*30ns*149=40us
    ret

LCD_command: ; display LCD
	mov	LCD_DATA, a
	clr	LCD_RS
	nop
	nop
	setb LCD_EN ; Enable pulse should be at least 230 ns
	nop
	nop
	nop
	nop
	nop
	nop
	clr	LCD_EN
	ljmp Wait40us

LCD_put:
	mov	LCD_DATA, A
	setb LCD_RS
	nop
	nop
	setb LCD_EN ; Enable pulse should be at least 230 ns
	nop
	nop
	nop
	nop
	nop
	nop
	clr	LCD_EN
	ljmp Wait40us
	    
    
LCD_DISPLAY_ALM_ON:
	lcall Wait40us
	djnz R1, LCD_DISPLAY_ALM_ON

	; Move to first column of first row	
	mov a, #80H
	lcall LCD_command
		
	; Display letter A
	mov a, #'A'
	lcall LCD_put
	
	mov a, #'l'
	lcall LCD_put
	
	mov a, #'a'
	lcall LCD_put
	
	mov a, #'r'
	lcall LCD_put
	
	mov a, #'m'
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #'O'
	lcall LCD_put
	
	mov a, #'n'
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	;lcall Wait_Delay
	ret
	
LCD_DISPLAY_ALM_OFF:

	lcall Wait40us
	djnz R1, LCD_DISPLAY_ALM_OFF

	; Move to first column of first row	
	mov a, #80H
	lcall LCD_command
		
	; Display letter A
	mov a, #'A'
	lcall LCD_put
	
	mov a, #'l'
	lcall LCD_put
	
	mov a, #'a'
	lcall LCD_put
	
	mov a, #'r'
	lcall LCD_put
	
	mov a, #'m'
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #'O'
	lcall LCD_put
	
	
	mov a, #'f'
	lcall LCD_put
	
	mov a, #'f'
	lcall LCD_put
	;lcall Wait_Delay
	
LCD_DISPLAY_SNOOZE:

	lcall Wait40us
	djnz R1, LCD_DISPLAY_SNOOZE

	; Move to first column of first row	
	mov a, #80H
	lcall LCD_command
		
	; Display letter A
	mov a, #'S'
	lcall LCD_put
	
	mov a, #'n'
	lcall LCD_put
	
	mov a, #'o'
	lcall LCD_put
	
	mov a, #'o'
	lcall LCD_put
	
	mov a, #'z'
	lcall LCD_put
	
	mov a, #'e'
	lcall LCD_put
	
	mov a, #'d'
	lcall LCD_put
	
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	
	ret
	
LCD_DISPLAY_TURNED_OFF:

	lcall Wait40us
	djnz R1, LCD_DISPLAY_TURNED_OFF

	; Move to first column of first row	
	mov a, #80H
	lcall LCD_command
		
	; Display letter A
	mov a, #'S'
	lcall LCD_put
	
	mov a, #'t'
	lcall LCD_put
	
	mov a, #'o'
	lcall LCD_put
	
	mov a, #'p'
	lcall LCD_put
	
	mov a, #'p'
	lcall LCD_put
	
	mov a, #'e'
	lcall LCD_put
	
	mov a, #'d'
	lcall LCD_put
	
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	lcall Wait_HalfSec
	lcall Wait_delay
	
	ret

END
