GETTEMPERATURE:
	mov x+0, #0
	mov x+1, #0
	mov bcd+0, #0
	mov bcd+1, #0
	mov bcd+2, #0
	mov bcd+3, #0
	mov bcd+4, #0
	mov bcd+5, #0
	mov bcd+6, #0
	mov y, #0
	lcall Get_Temperature_Hot
	lcall Get_Temperature_Cold
	lcall Get_Temperature_Total
	mov temp, totaltemp

	lcall ISRWaitQuarterSec
	;lcall waithalfsec
	
	lcall Display_temp

	lcall send_number

	ret


DO_SPI_G:
	push acc
    mov R1, #0            ; Received byte stored in R1
    mov R2, #8            ; Loop counter (8-bits)
DO_SPI_G_LOOP:
    mov a, R0             ; Byte to write is in R0
    rlc a                 ; Carry flag has bit to write
    mov R0, a
    mov MOSI, c
    setb SCLK             ; Transmit
    mov c, MISO           ; Read received bit
    mov a, R1             ; Save received bit in R1
    rlc a
    mov R1, a
    clr SCLK
    djnz R2, DO_SPI_G_LOOP
    pop acc
    ret

Delay:
	mov R3, #5
Delay_loop:
	djnz R3, Delay_loop
	ret
Read_ADC_Channel:
	clr CE_ADC
	mov R0, #00000001B ; Start bit:1
	lcall DO_SPI_G
	
	mov a, b ; clears A AND CLEARS B
	swap a
	anl a, #0F0H ; A STILL ZERO
	setb acc.7 ; Single mode (bit 7). A IS 1 NOW
	
	mov R0, a ;  Select channel R0 HAS ONE NOW
	lcall DO_SPI_G
	mov a, R1          ; R1 contains bits 8 and 9
	anl a, #03H ; ONLY GET LEAST 2 SIG BITS
	mov R7, a ; R7 HAS TWO MOST SIG BITS FROM 10 BIT ADC MCP
	
	mov R0, #55H ; It doesn't matter what we transmit...
	lcall DO_SPI_G
	mov a, R1    ; R1 contains bits 0 to 7
	mov R6, a ; WE DIDN'T DO THE ANL THING ABOVE SO WE GET ALL THE BITS
	; END OF THIS IS R6 HAS BITS 0 TO 7, R7 HAS BITS 8 AND 9
	setb CE_ADC ; STOP THIS AND REPEAT TO GET DA NEW VAL DOE
	ret

SendString:
    CLR A
    MOVC A, @A+DPTR
    JZ SSDone
    LCALL putchar
    INC DPTR
    SJMP SendString
SSDone:
    ret
    
send_number:
	push acc
	clr a
	
	mov dptr, #myLUT1
	
	;took out rest of digits
	
	;least significant digits
	mov a, bcd+1
	anl a, #0fh
	jz	noHundred
	movc a, @a+dptr
	;orl a, #30h ; Convert to ASCII
	lcall putchar

noHundred:
	mov a, bcd+0
	swap a
	anl a, #0fh
	movc a, @a+dptr
	;orl a, #30h ; Convert to ASCII
	lcall putchar
	
	mov a, bcd+0
	anl a, #0fh
	movc a, @a+dptr
	;orl a, #30h ; Convert to ASCII
	lcall putchar
	
	;linebreak
	mov dptr, #myLUT2
	mov a, #0
	movc a, @a+dptr
	;orl a, #30h
	lcall putchar
	
	;align left
	;mov dptr, #myLUT3
	;mov a, #0
	;movc a, @a+dptr
	;orl a, #30h
	;lcall putchar
	
	pop acc
	ret

putchar:
    JNB TI, putchar
    CLR TI
    MOV SBUF, a
    RET   
    
Get_Temperature_Hot:
	mov b, #1 ; Thermocouple is connected to channel 0
	push x
	;push acc
	lcall Read_ADC_Channel
	
	mov x+1, R7
	mov x+0, R6
	
	load_y(20)
	lcall mul16
		
	load_y(52)
	lcall div16
	
	load_y(3)
	lcall sub16
	
	mov hotTemp, x
	;mov hotTemp, #0  
	pop x
	ret
	
Get_Temperature_Cold:
	mov b, #0 ; LM335 temperature sensor is connected to channel 1 (will read from channel 1 now)
	push x
	lcall Read_ADC_Channel
	;lcall InitSerialPort
	
	;mov x+0, c
	mov x+1, R7
	mov x+0, R6
	

	
	
	
	
	; The temperature can be calculated as (ADC*500/1024)-273 (may overflow 16 bit operations)
	; or (ADC*250/512)-273 (may overflow 16 bit operations)
	; or (ADC*125/256)-273 (may overflow 16 bit operations)
	; or (ADC*62/256)+(ADC*63/256)-273 (Does not overflow 16 bit operations!)
	
	Load_y(62)
	lcall mul16
	mov R4, x+1

	mov x+1, R7
	mov x+0, R6

	Load_y(63)
	lcall mul16
	mov R5, x+1
	
	mov x+0, R4
	mov x+1, #0
	mov y+0, R5
	mov y+1, #0
	lcall add16
	
	Load_y(274) ; this is calibrated more accurately (note that it is 10x factor)
	lcall sub16
	
	mov coldTemp, x

	
	;lcall hex2bcd
	pop x
	
	ret

Get_Temperature_Total:
	push x
	push acc

	
	;lcall Get_Temperature_Hot
	;lcall Get_Temperature_Cold

	mov a, #0
	mov a, hotTemp ; hot temp is weird right now in get_temp_total, is fine by itself
	;mov a, coldTemp
	add a, coldTemp
	mov totaltemp, a
	mov x, totalTemp
	
	lcall hex2bcd

	pop acc
	pop x
	ret
	
Display_temp:
	mov dptr, #myLUT
	; Display Digit 0
    mov A, bcd+0
    anl a, #0fh
    movc A, @A+dptr
    mov HEX0, A
	; Display Digit 1
    mov A, bcd+0
    swap a
    anl a, #0fh
    movc A, @A+dptr
    mov HEX1, A
    ; Third digit needs help
    mov A, bcd+1
    anl a, #0fh
    movc A, @A+dptr
    mov HEX2, A
    ret 
;SFANCY LEDS;
TempuratureLED:
	mov LEDRA, 	#0
    mov LEDRB, 	#0
    mov LEDRC, 	#0
    mov LEDG, 	#0
check_eighteen_led:
    clr mf
	mov x, temp
	mov y, #228D
	lcall x_gteq_y
	jb mf,  eighteen_led_on
	jnb mf, check_seventeen_led
eighteen_led_on:
	mov a, #11111111B
	mov LEDRA, a
	mov a, #11111111B
	mov LEDRB, a
	mov LEDRC, #11B
	ljmp DoneLED
check_seventeen_led:
	clr mf
	mov x, temp
	mov y, #216D
	lcall x_gteq_y
	jb mf,  seventeen_led_on
	jnb mf,check_sixteen_led
seventeen_led_on:
	mov a, #11111110B
	mov LEDRA, a
	mov a, #11111111B
	mov LEDRB, a
	mov LEDRC, #11B
	ljmp DoneLED
check_sixteen_led:
    clr mf
	mov x, temp
	mov y, #204D
	lcall x_gteq_y
	jb mf, sixteen_led_on
	jnb mf, check_fifteen_led
sixteen_led_on:
	mov a, #11111100B
	mov LEDRA, a
	mov a, #11111111B
	mov LEDRB, a
	mov LEDRC, #11B
	ljmp DoneLED
check_fifteen_led:
    clr mf
	mov x, temp
	mov y, #192D
	lcall x_gteq_y
	jb mf, fifteen_led_on
	jnb mf, check_fourteen_led
fifteen_led_on:
	mov a, #11111000B
	mov LEDRA, a
	mov a, #11111111B
	mov LEDRB, a
	mov LEDRC, #11B
	ljmp DoneLED
check_fourteen_led:
    clr mf
	mov x, temp
	mov y, #180D
	lcall x_gteq_y
	jb mf, fourteen_led_on
	jnb mf, check_thirteen_led
fourteen_led_on:
	mov a, #11110000B
	mov LEDRA, a
	mov a, #11111111B
	mov LEDRB, a
	mov LEDRC, #11B
	ljmp DoneLED
check_thirteen_led:
    clr mf
	mov x, temp
	mov y, #168D
	lcall x_gteq_y
	jb mf, thirteen_led_on
	jnb mf, check_twelve_led
thirteen_led_on:
	mov a, #11100000B
	mov LEDRA, a
	mov a, #11111111B
	mov LEDRB, a
	mov LEDRC, #11B
	ljmp DoneLED
check_twelve_led:
    clr mf
	mov x, temp
	mov y, #156D
	lcall x_gteq_y
	jb mf, twelve_led_on
	jnb mf, check_eleven_led
twelve_led_on:
	mov a, #11000000B
	mov LEDRA, a
	mov a, #11111111B
	mov LEDRB, a
	mov LEDRC, #11B
	ljmp DoneLED
check_eleven_led:
    clr mf
	mov x, temp
	mov y, #144D
	lcall x_gteq_y
	jb mf, eleven_led_on
	jnb mf, check_ten_led
eleven_led_on:
	mov a, #10000000B
	mov LEDRA, a
	mov a, #11111111B
	mov LEDRB, a
	mov LEDRC, #11B
	ljmp DoneLED
check_ten_led:
	clr mf
	mov x, temp
	mov y, #132D
	lcall x_gteq_y
	jb mf, ten_led_on
	jnb mf, check_nine_led
ten_led_on:
	mov a, #00000000B
	mov LEDRA, a
	mov a, #11111111B
	mov LEDRB, a
	mov LEDRC, #11B
	ljmp DoneLED
check_nine_led:
	clr mf
	mov x, temp
	mov y, #120D
	lcall x_gteq_y
	jb mf, nine_led_on
	jnb mf, check_eight_led
nine_led_on:
	mov a, #00000000B
	mov LEDRA, a
	mov a, #11111110B
	mov LEDRB, a
	mov LEDRC, #11B
	ljmp DoneLED
	
check_eight_led:
	clr mf
	mov x, temp
	mov y, #108D
	lcall x_gteq_y
	jb mf, eight_led_on
	jnb mf, check_seven_led
eight_led_on:
	mov a, #00000000B
	mov LEDRA, a
	mov a, #11111100B
	mov LEDRB, a
	mov LEDRC, #11B
	ljmp DoneLED
	
check_seven_led:
	clr mf
	mov x, temp
	mov y, #96D
	lcall x_gteq_y
	jb mf, seven_led_on
	jnb mf, check_six_led
seven_led_on:
	mov a, #00000000B
	mov LEDRA, a
	mov a, #11111000B
	mov LEDRB, a
	mov LEDRC, #11B
	ljmp DoneLED
check_six_led:
	clr mf
	mov x, temp
	mov y, #84D
	lcall x_gteq_y
	jb mf, six_led_on
	jnb mf, check_five_led
six_led_on:
	mov a, #00000000B
	mov LEDRA, a
	mov a, #11110000B
	mov LEDRB, a
	mov LEDRC, #11B
	ljmp DoneLED
check_five_led:
    clr mf
	mov x, temp
	mov y, #72D
	lcall x_gteq_y
	jb mf, five_led_on
	jnb mf, check_four_led
five_led_on:
	mov a, #00000000B
	mov LEDRA, a
	mov a, #11100000B
	mov LEDRB, a
	mov LEDRC, #11B
	ljmp DoneLED
	
check_four_led:
    clr mf
	mov x, temp
	mov y, #60D
	lcall x_gteq_y
	jb mf, four_led_on
	jnb mf, check_three_led
four_led_on:
	mov a, #00000000B
	mov LEDRA, a
	mov a, #11000000B
	mov LEDRB, a
	mov LEDRC, #11B
	ljmp DoneLED
	
check_three_led:
    clr mf
	mov x, temp
	mov y, #48D
	lcall x_gteq_y
	jb mf, three_led_on
	jnb mf, check_two_led
three_led_on:
	mov a, #00000000B
	mov LEDRA, a
	mov a, #10000000B
	mov LEDRB, a
	mov LEDRC, #11B
	ljmp DoneLED
	
check_two_led:
    clr mf
	mov x, temp
	mov y, #36D
	lcall x_gteq_y
	jb mf, two_led_on
	jnb mf, check_one_led
two_led_on:
	mov a, #00000000B
	mov LEDRA, a
	mov LEDRB, a
	mov LEDRC, #11B
	ljmp DoneLED
	
check_one_led:
    clr mf
	mov x, temp
	mov y, #24D
	lcall x_gteq_y
	jb mf, one_led_on
	jnb mf, DoneLED
one_led_on:
	mov a, #00000000B
	mov LEDRA, a
	mov LEDRB, a
	mov LEDRC, #10B
check_zero_led:
    clr mf
	mov x, temp
	mov y, #12D
	lcall x_gteq_y
	jb mf, zero_led_on
	jnb mf, DoneLED
zero_led_on:
	mov a, #00000000B
	mov LEDRA, a
	mov LEDRB, a
	mov LEDRC, #00B
DoneLED:
	ret
Clr_loop:
	lcall Wait40us
	djnz R1, Clr_loop
	ret
	
;SET PARAMETERS_____________________________________________________________________


;_________________________________________________________________
ramptosoak:	
	mov x, rampsoaktemp
	lcall hex2bcd1
	lcall Display
	
	jb KEY.3, M1
	jnb KEY.3, $
	
	mov a, rampsoaktemp
	add a, #10
	mov rampsoaktemp, a
	cjne a, #255, M1
	mov peaktemp, #0
	
M1:

	jb KEY.2, M2
	jnb KEY.2, $
	
	mov a, rampsoaktemp
	add a, #3
	mov rampsoaktemp, a
	cjne a, #255, M2
	mov rampsoaktemp, #0

M2:

	jb KEY.1, M3
	jnb KEY.1, $
	
	mov a, rampsoaktemp
	add a, #1
	mov rampsoaktemp, a
	cjne a, #255, M3
	mov rampsoaktemp, #0

M3:
	
	mov x, rampsoaktemp
	lcall LCD_DISPLAY_RAMP
	jb SWA.0, ramptosoak
	ret

	
;_________________________________________________________________
	
	
	
soak:
	
	jb KEY.3, M4
	jnb KEY.3, $
	mov a, soakcounter
	add a, #60
	mov soakcounter, a
	mov a, soakminutes
	add a, #1
	da a
	mov soakminutes, a
	cjne a, #60H, M4
	mov soakminutes, #0
	
M4:

	jb KEY.2, M5
	jnb KEY.2, $
	mov a, soakcounter
	add a, #3
	mov soakcounter, a
	mov a, soakseconds
	add a, #5
	da a
	mov soakseconds, a
	cjne a, #60H, M5

M5:

	jb KEY.1, M6
	jnb KEY.1, $
	mov a, soakcounter
	add a, #1
	mov soakcounter, a
	mov a, soakseconds
	add a, #1
	da a
	mov soakseconds, a
	cjne a, #60H, M6
	mov soakseconds, #0

M6:
	
	lcall LCD_DISPLAY_SOAK_TIME
	jb SWA.1, soak
	ret
	
;______________________________________________________________________________

ramptopeak:

	mov x, peaktemp
	lcall hex2bcd2
	lcall Display
	
	jb KEY.3, M7
	jnb KEY.3, $
	
	mov a, peaktemp
	add a, #10
	mov peaktemp, a
	cjne a, #255, M7
	mov peaktemp, #0
	
M7:

	jb KEY.2, M8
	jnb KEY.2, $
	
	mov a, peaktemp
	add a, #3
	mov peaktemp, a
	cjne a, #255, M8
	mov peaktemp, #0

M8:

	jb KEY.1, M9
	jnb KEY.1, $
	
	mov a, peaktemp
	add a, #1
	mov peaktemp, a
	cjne a, #255, M9
	mov peaktemp, #0

M9:
	
	mov x, peaktemp
	lcall LCD_DISPLAY_RAMP_PEAK
	jb SWA.2, ramptopeak
	ret
	
;______________________________________________________________________________

reflowtime:
	jb KEY.3, M10
	jnb KEY.3, $
	
	mov a, reflowcounter
	add a, #60
	mov reflowcounter, a
	
	mov a, reflowminutes
	add a, #1
	mov reflowminutes, a
	cjne a, #60H, M10
	mov reflowminutes, #0
	
M10:

	jb KEY.2, M11
	jnb KEY.2, $
	
	mov a, reflowcounter
	add a, #3
	mov reflowcounter, a
	
	mov a, reflowseconds
	add a, #5
	da a
	mov reflowseconds, a
	cjne a, #60H, M11
	mov reflowseconds, #0

M11:

	jb KEY.1, M12
	jnb KEY.1, $
	
	mov a, reflowcounter
	add a, #1
	mov reflowcounter, a
	
	
	mov a, reflowseconds
	add a, #1
	da a
	mov reflowseconds, a
	cjne a, #60H, M12
	mov reflowseconds, #0

M12:
	
	lcall LCD_DISPLAY_REFLOW2
	jb SWA.3, reflowtime
	ret

cooltemp:
	mov x, coolingtemp
	lcall hex2bcd3
	lcall Display
	
	jb KEY.3, M13
	jnb KEY.3, $
	
	mov a, coolingtemp
	add a, #10
	mov coolingtemp, a
	cjne a, #255, M13
	mov coolingtemp, #0
	
M13:

	jb KEY.2, M14
	jnb KEY.2, $
	
	mov a, coolingtemp
	add a, #3
	mov coolingtemp, a
	cjne a, #255, M14
	mov coolingtemp, #0

M14:

	jb KEY.1, M15
	jnb KEY.1, $
	
	mov a, coolingtemp
	add a, #1
	mov coolingtemp, a
	cjne a, #255, M15
	mov coolingtemp, #0

M15:
	
	mov x, coolingtemp
	lcall LCD_DISPLAY_COOL
	jb SWA.4, cooltemp
	ret


	
;INITIALIZE ISR_____________________________________________________________________

Init_Timer0:	
	mov TMOD,  #00000001B ; GATE=0, C/T*=0, M1=0, M0=1: 16-bit timer
	clr TR0 ; Disable timer 0
	clr TF0
    mov TH0, #high(TIMER0_RELOAD)
    mov TL0, #low(TIMER0_RELOAD)
    setb TR0 ; Enable timer 0
    setb ET0 ; Enable timer 0 interrupt
    ret

Init_Timer1:	
	mov TMOD,  #000000001B ; GATE=0, C/T*=0, M1=0, M0=1: 16-bit timer
	clr TR1 ; Disable timer 0
	clr TF1
    mov TH1, #high(TIMER1_RELOAD)
    mov TL1, #low(TIMER1_RELOAD)
    
    clr SCLK
    
    setb TR1 ; Enable timer 1
    setb ET1 ; Enable timer 1 interrupt
    mov SCON, #52H
    ret
    

   	
;BUZZER IMPLEMENTATION_____________________________________________________________    
    
; when "toring" is set, the buzzer will go six times	
	
sixring:
	mov a, #1
	mov ring, a
	cpl LEDG.1
	cpl LEDG.2
	
	lcall ISRWaitQuarterSec
	
	mov a, #0
	mov ring, a
	cpl LEDG.1
	cpl LEDG.2
	
	lcall ISRWaitQuarterSec
	
	mov a, #1
	mov ring, a
	cpl LEDG.1
	cpl LEDG.2
	
	lcall ISRWaitQuarterSec
	
	mov a, #0
	mov ring, a
	cpl LEDG.1
	cpl LEDG.2
	
	lcall ISRWaitQuarterSec
	
	mov a, #1
	mov ring, a
	cpl LEDG.1
	cpl LEDG.2
	
	lcall ISRWaitQuarterSec
	
	mov a, #0
	mov ring, a
	cpl LEDG.1
	cpl LEDG.2
	
	lcall ISRWaitQuarterSec
	
	mov a, #1
	mov ring, a
	cpl LEDG.1
	cpl LEDG.2
	
	lcall ISRWaitQuarterSec
	
	mov a, #0
	mov ring, a
	cpl LEDG.1
	cpl LEDG.2
	
	lcall ISRWaitQuarterSec
	
	mov a, #1
	mov ring, a
	cpl LEDG.1
	cpl LEDG.2
	
	lcall ISRWaitQuarterSec
	
	mov a, #0
	mov ring, a
	cpl LEDG.1
	cpl LEDG.2
	
	lcall ISRWaitQuarterSec
	
	mov a, #1
	mov ring, a
	cpl LEDG.1
	cpl LEDG.2
	
	lcall ISRWaitQuarterSec
	
	mov a, #0
	mov ring, a
	cpl LEDG.1
	cpl LEDG.2
	
	ret
	
onering:

	mov a, #1
	mov ring, a
	cpl LEDG.1
	cpl LEDG.2
	
	lcall ISRWaitQuarterSec
	
	mov a, #0
	mov ring, a
	cpl LEDG.1
	cpl LEDG.2
	
	ret
	
tworing:

	mov a, #1
	mov ring, a
	cpl LEDG.1
	cpl LEDG.2
	
	lcall ISRWaitQuarterSec
	
	mov a, #0
	mov ring, a
	cpl LEDG.1
	cpl LEDG.2
	
	lcall ISRWaitQuarterSec
	
	mov a, #1
	mov ring, a
	cpl LEDG.1
	cpl LEDG.2
	
	lcall ISRWaitQuarterSec
	
	mov a, #0
	mov ring, a
	cpl LEDG.1
	cpl LEDG.2
	
	ret

;WAIT FUNCTIONS________________________________________________________________

ISRWaitQuarterSec:
	mov R2, #45
L3:	mov R1, #250
L2:	mov R0, #250
L1:	djnz R0, L1 ; 3 machine cycles-> 3*30ns*250=22.5us
	djnz R1, L2 ; 22.5us*250=5.62ms
	djnz R2, L2 ; 5.62ms*90=0.5s (approximately)
	ret

WaitOneSec:	
	mov R2, #180
L6:	mov R1, #250
L5:	mov R0, #250
L4:	djnz R0, L4 ; 3 machine cycles-> 3*30ns*250=22.5us
	djnz R1, L5 ; 22.5us*250=5.62ms
	djnz R2, L5 ; 5.62ms*90=0.5s (approximately)
	ret
	
Wait_HalfSec:	
	mov R2, #90
L9:	mov R1, #250
L8:	mov R0, #250
L7:	djnz R0, L7 ; 3 machine cycles-> 3*30ns*250=22.5us
	djnz R1, L8 ; 22.5us*250=5.62ms
	djnz R2, L8 ; 5.62ms*90=0.5s (approximately)
	ret

WaitHalfSec:			;;another one for different code	
	mov R2, #90
L12:mov R1, #250
L11:mov R0, #250
L10:djnz R0, L10 ; 3 machine cycles-> 3*30ns*250=22.5us
	djnz R1, L11 ; 22.5us*250=5.62ms
	djnz R2, L11 ; 5.62ms*90=0.5s (approximately)
	ret

Wait40us:
	mov R0, #149
X1: 
	nop
	nop
	nop
	nop
	nop
	nop
	djnz R0, X1 ; 9 machine cycles-> 9*30ns*149=40us
    ret

;LCD DISPLAY TEMP________________________________________________________________

LCD_command: ; display LCD
	mov	LCD_DATA, a
	clr	LCD_RS
	nop
	nop
	setb LCD_EN ; Enable pulse should be at least 230 ns
	nop
	nop
	nop
	nop
	nop
	nop
	clr	LCD_EN
	ljmp Wait40us

LCD_put:
	mov	LCD_DATA, A
	setb LCD_RS
	nop
	nop
	setb LCD_EN ; Enable pulse should be at least 230 ns
	nop
	nop
	nop
	nop
	nop
	nop
	clr	LCD_EN
	ljmp Wait40us
	
LCD_DISPLAY_RAMP:
	lcall Wait40us
	djnz R1, LCD_DISPLAY_RAMP

	; Move to first column of first row	
	mov a, #80H
	lcall LCD_command
		
	; Display letter A
	mov a, #'S'
	lcall LCD_put
	
	mov a, #'e'
	lcall LCD_put
	
	mov a, #'t'
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #'R'
	lcall LCD_put
	
	mov a, #'a'
	lcall LCD_put
	
	mov a, #'m'
	lcall LCD_put
	
	mov a, #'p'
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #'t'
	lcall LCD_put
	
	mov a, #'o'
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put		
	
	mov a, #'S'
	lcall LCD_put
	
	mov a, #'o'
	lcall LCD_put
	
	mov a, #'a'
	lcall LCD_put
	
	mov a, #'k'
	lcall LCD_put
	
	mov a, #'d'					;; EVERYTHING FROM HERE UNTIL...
	lcall LCD_put
	
	mov a, #'e'
	lcall LCD_put
	
	mov a, #'f'
	lcall LCD_put
	
	mov a, #'g'
	lcall LCD_put
	
	mov a, #'h'
	lcall LCD_put
	
	mov a, #'i'
	lcall LCD_put
	
	mov a, #'j'
	lcall LCD_put
	
	mov a, #'k'
	lcall LCD_put
	
	mov a, #'l'
	lcall LCD_put
	
	mov a, #'m'
	lcall LCD_put
	
	mov a, #'n'
	lcall LCD_put
	
	mov a, #'o'
	lcall LCD_put
	
	mov a, #'p'
	lcall LCD_put
	
	mov a, #'q'
	lcall LCD_put
	
	mov a, #'r'
	lcall LCD_put
	
	mov a, #'s'
	lcall LCD_put
	
	mov a, #'t'
	lcall LCD_put
	
	mov a, #'u'
	lcall LCD_put
	
	mov a, #'v'
	lcall LCD_put
	
	mov a, #'w'
	lcall LCD_put
	
	mov a, #'x'
	lcall LCD_put
	
	mov a, #'y'
	lcall LCD_put
	
	mov a, #'z'
	lcall LCD_put
	
	mov a, #'y'
	lcall LCD_put			;; HERE IS NOT VISIBLE!
	
	mov dptr, #asciiLookup
	
	mov a, bcd1+1
	anl a, #1111B
	movc a, @a+dptr
	lcall lcd_put
	
	mov a, bcd1+0
	anl a, #11110000B
	rr a
	rr a
	rr a
	rr a
	movc a, @a+dptr
	lcall LCD_put
	
	mov a, bcd1+0
	anl a, #1111B
	movc a, @a+dptr
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #'C'
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	ret
	
LCD_DISPLAY_SOAK_TIME:
	lcall Wait40us
	djnz R1, LCD_DISPLAY_SOAK_TIME

	; Move to first column of first row	
	mov a, #80H
	lcall LCD_command
		
	; Display letter A
	mov a, #'S'
	lcall LCD_put
	
	mov a, #'e'
	lcall LCD_put
	
	mov a, #'t'
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #'S'
	lcall LCD_put
	
	mov a, #'o'
	lcall LCD_put
	
	mov a, #'a'
	lcall LCD_put
	
	mov a, #'k'
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #'T'
	lcall LCD_put
	
	mov a, #'i'
	lcall LCD_put
	
	mov a, #'m'
	lcall LCD_put		
	
	mov a, #'e'
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #'d'					;; EVERYTHING FROM HERE UNTIL...
	lcall LCD_put
	
	mov a, #'e'
	lcall LCD_put
	
	mov a, #'f'
	lcall LCD_put
	
	mov a, #'g'
	lcall LCD_put
	
	mov a, #'h'
	lcall LCD_put
	
	mov a, #'i'
	lcall LCD_put
	
	mov a, #'j'
	lcall LCD_put
	
	mov a, #'k'
	lcall LCD_put
	
	mov a, #'l'
	lcall LCD_put
	
	mov a, #'m'
	lcall LCD_put
	
	mov a, #'n'
	lcall LCD_put
	
	mov a, #'o'
	lcall LCD_put
	
	mov a, #'p'
	lcall LCD_put
	
	mov a, #'q'
	lcall LCD_put
	
	mov a, #'r'
	lcall LCD_put
	
	mov a, #'s'
	lcall LCD_put
	
	mov a, #'t'
	lcall LCD_put
	
	mov a, #'u'
	lcall LCD_put
	
	mov a, #'v'
	lcall LCD_put
	
	mov a, #'w'
	lcall LCD_put
	
	mov a, #'x'
	lcall LCD_put
	
	mov a, #'y'
	lcall LCD_put
	
	mov a, #'z'
	lcall LCD_put
	
	mov a, #'y'
	lcall LCD_put			;; HERE IS NOT VISIBLE!
	
	mov dptr, #asciiLookup
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, soakminutes
	anl a, #11110000B
	rr a
	rr a
	rr a
	rr a
	movc a, @a+dptr
	lcall LCD_put
	
	mov a, soakminutes
	anl a, #1111B
	movc a, @a+dptr
	lcall LCD_put
	
	mov a, #':'
	lcall LCD_put
	
	mov a, soakseconds
	anl a, #11110000B
	rr a
	rr a
	rr a
	rr a
	movc a, @a+dptr
	lcall LCD_put
	
	mov a, soakseconds
	anl a, #1111B
	movc a, @a+dptr
	lcall LCD_put
	
	ret
	
LCD_DISPLAY_RAMP_PEAK:
	lcall Wait40us
	djnz R1, LCD_DISPLAY_RAMP_PEAK

	; Move to first column of first row	
	mov a, #80H
	lcall LCD_command
		
	; Display letter A
	mov a, #'S'
	lcall LCD_put
	
	mov a, #'e'
	lcall LCD_put
	
	mov a, #'t'
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #'P'
	lcall LCD_put
	
	mov a, #'e'
	lcall LCD_put
	
	mov a, #'a'
	lcall LCD_put
	
	mov a, #'k'
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #'T'
	lcall LCD_put
	
	mov a, #'e'
	lcall LCD_put
	
	mov a, #'m'
	lcall LCD_put		
	
	mov a, #'p'
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #'d'					;; EVERYTHING FROM HERE UNTIL...
	lcall LCD_put
	
	mov a, #'e'
	lcall LCD_put
	
	mov a, #'f'
	lcall LCD_put
	
	mov a, #'g'
	lcall LCD_put
	
	mov a, #'h'
	lcall LCD_put
	
	mov a, #'i'
	lcall LCD_put
	
	mov a, #'j'
	lcall LCD_put
	
	mov a, #'k'
	lcall LCD_put
	
	mov a, #'l'
	lcall LCD_put
	
	mov a, #'m'
	lcall LCD_put
	
	mov a, #'n'
	lcall LCD_put
	
	mov a, #'o'
	lcall LCD_put
	
	mov a, #'p'
	lcall LCD_put
	
	mov a, #'q'
	lcall LCD_put
	
	mov a, #'r'
	lcall LCD_put
	
	mov a, #'s'
	lcall LCD_put
	
	mov a, #'t'
	lcall LCD_put
	
	mov a, #'u'
	lcall LCD_put
	
	mov a, #'v'
	lcall LCD_put
	
	mov a, #'w'
	lcall LCD_put
	
	mov a, #'x'
	lcall LCD_put
	
	mov a, #'y'
	lcall LCD_put
	
	mov a, #'z'
	lcall LCD_put
	
	mov a, #'y'
	lcall LCD_put			;; HERE IS NOT VISIBLE!
	
	mov dptr, #asciiLookup
	
	mov a, bcd2+1
	anl a, #1111B
	movc a, @a+dptr
	lcall lcd_put
	
	mov a, bcd2+0
	anl a, #11110000B
	rr a
	rr a
	rr a
	rr a
	movc a, @a+dptr
	lcall LCD_put
	
	mov a, bcd2+0
	anl a, #1111B
	movc a, @a+dptr
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #'C'
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	ret
	
LCD_DISPLAY_REFLOW2:
	lcall Wait40us
	djnz R1, LCD_DISPLAY_REFLOW2

	; Move to first column of first row	
	mov a, #80H
	lcall LCD_command
		
	; Display letter A
	mov a, #'S'
	lcall LCD_put
	
	mov a, #'e'
	lcall LCD_put
	
	mov a, #'t'
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #'R'
	lcall LCD_put
	
	mov a, #'e'
	lcall LCD_put
	
	mov a, #'f'
	lcall LCD_put
	
	mov a, #'o'
	lcall LCD_put
	
	mov a, #'w'
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #'T'
	lcall LCD_put
	
	mov a, #'i'
	lcall LCD_put		
	
	mov a, #'m'
	lcall LCD_put
	
	mov a, #'e'
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '					;; EVERYTHING FROM HERE UNTIL...
	lcall LCD_put
	
	mov a, #'e'
	lcall LCD_put
	
	mov a, #'f'
	lcall LCD_put
	
	mov a, #'g'
	lcall LCD_put
	
	mov a, #'h'
	lcall LCD_put
	
	mov a, #'i'
	lcall LCD_put
	
	mov a, #'j'
	lcall LCD_put
	
	mov a, #'k'
	lcall LCD_put
	
	mov a, #'l'
	lcall LCD_put
	
	mov a, #'m'
	lcall LCD_put
	
	mov a, #'n'
	lcall LCD_put
	
	mov a, #'o'
	lcall LCD_put
	
	mov a, #'p'
	lcall LCD_put
	
	mov a, #'q'
	lcall LCD_put
	
	mov a, #'r'
	lcall LCD_put
	
	mov a, #'s'
	lcall LCD_put
	
	mov a, #'t'
	lcall LCD_put
	
	mov a, #'u'
	lcall LCD_put
	
	mov a, #'v'
	lcall LCD_put
	
	mov a, #'w'
	lcall LCD_put
	
	mov a, #'x'
	lcall LCD_put
	
	mov a, #'y'
	lcall LCD_put
	
	mov a, #'z'
	lcall LCD_put
	
	mov a, #'y'
	lcall LCD_put			;; HERE IS NOT VISIBLE!
	
	mov dptr, #asciiLookup
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, reflowminutes
	anl a, #11110000B
	rr a
	rr a
	rr a
	rr a
	movc a, @a+dptr
	lcall LCD_put
	
	mov a, reflowminutes
	anl a, #1111B
	movc a, @a+dptr
	lcall LCD_put
	
	mov a, #':'
	lcall LCD_put
	
	mov a, reflowseconds
	anl a, #11110000B
	rr a
	rr a
	rr a
	rr a
	movc a, @a+dptr
	lcall LCD_put
	
	mov a, reflowseconds
	anl a, #1111B
	movc a, @a+dptr
	lcall LCD_put
	
	ret

LCD_DISPLAY_COOL:
	lcall Wait40us
	djnz R1, LCD_DISPLAY_COOL

	; Move to first column of first row	
	mov a, #80H
	lcall LCD_command
		
	; Display letter A
	mov a, #'S'
	lcall LCD_put
	
	mov a, #'e'
	lcall LCD_put
	
	mov a, #'t'
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #'C'
	lcall LCD_put
	
	mov a, #'o'
	lcall LCD_put
	
	mov a, #'o'
	lcall LCD_put
	
	mov a, #'l'
	lcall LCD_put
	
	mov a, #'i'
	lcall LCD_put
	
	mov a, #'n'
	lcall LCD_put
	
	mov a, #'g'
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put		
	
	mov a, #'T'
	lcall LCD_put
	
	mov a, #'e'
	lcall LCD_put
	
	mov a, #'m'
	lcall LCD_put
	
	mov a, #'p'
	lcall LCD_put
	
	mov a, #'d'					;; EVERYTHING FROM HERE UNTIL...
	lcall LCD_put
	
	mov a, #'e'
	lcall LCD_put
	
	mov a, #'f'
	lcall LCD_put
	
	mov a, #'g'
	lcall LCD_put
	
	mov a, #'h'
	lcall LCD_put
	
	mov a, #'i'
	lcall LCD_put
	
	mov a, #'j'
	lcall LCD_put
	
	mov a, #'k'
	lcall LCD_put
	
	mov a, #'l'
	lcall LCD_put
	
	mov a, #'m'
	lcall LCD_put
	
	mov a, #'n'
	lcall LCD_put
	
	mov a, #'o'
	lcall LCD_put
	
	mov a, #'p'
	lcall LCD_put
	
	mov a, #'q'
	lcall LCD_put
	
	mov a, #'r'
	lcall LCD_put
	
	mov a, #'s'
	lcall LCD_put
	
	mov a, #'t'
	lcall LCD_put
	
	mov a, #'u'
	lcall LCD_put
	
	mov a, #'v'
	lcall LCD_put
	
	mov a, #'w'
	lcall LCD_put
	
	mov a, #'x'
	lcall LCD_put
	
	mov a, #'y'
	lcall LCD_put
	
	mov a, #'z'
	lcall LCD_put
	
	mov a, #'y'
	lcall LCD_put			;; HERE IS NOT VISIBLE!
	
	mov dptr, #asciiLookup
	
	mov a, bcd3+1
	anl a, #1111B
	movc a, @a+dptr
	lcall lcd_put
	
	mov a, bcd3+0
	anl a, #11110000B
	rr a
	rr a
	rr a
	rr a
	movc a, @a+dptr
	lcall LCD_put
	
	mov a, bcd3+0
	anl a, #1111B
	movc a, @a+dptr
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #'C'
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	ret
	
	    
LCD_DISPLAY_IDLE:
	lcall Wait40us
	djnz R1, LCD_DISPLAY_IDLE

	; Move to first column of first row	
	mov a, #80H
	lcall LCD_command
		
	; Display letter A
	mov a, #'I'
	lcall LCD_put
	
	mov a, #'d'
	lcall LCD_put
	
	mov a, #'l'
	lcall LCD_put
	
	mov a, #'e'
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put		
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #'d'					;; EVERYTHING FROM HERE UNTIL...
	lcall LCD_put
	
	mov a, #'e'
	lcall LCD_put
	
	mov a, #'f'
	lcall LCD_put
	
	mov a, #'g'
	lcall LCD_put
	
	mov a, #'h'
	lcall LCD_put
	
	mov a, #'i'
	lcall LCD_put
	
	mov a, #'j'
	lcall LCD_put
	
	mov a, #'k'
	lcall LCD_put
	
	mov a, #'l'
	lcall LCD_put
	
	mov a, #'m'
	lcall LCD_put
	
	mov a, #'n'
	lcall LCD_put
	
	mov a, #'o'
	lcall LCD_put
	
	mov a, #'p'
	lcall LCD_put
	
	mov a, #'q'
	lcall LCD_put
	
	mov a, #'r'
	lcall LCD_put
	
	mov a, #'s'
	lcall LCD_put
	
	mov a, #'t'
	lcall LCD_put
	
	mov a, #'u'
	lcall LCD_put
	
	mov a, #'v'
	lcall LCD_put
	
	mov a, #'w'
	lcall LCD_put
	
	mov a, #'x'
	lcall LCD_put
	
	mov a, #'y'
	lcall LCD_put
	
	mov a, #'z'
	lcall LCD_put
	
	mov a, #'y'
	lcall LCD_put			;; HERE IS NOT VISIBLE!
	
	mov dptr, #asciiLookup
	
	mov a, bcd+1
	anl a, #1111B
	movc a, @a+dptr
	lcall lcd_put
	
	mov a, bcd+0
	anl a, #11110000B
	rr a
	rr a
	rr a
	rr a
	movc a, @a+dptr
	lcall LCD_put
	
	mov a, bcd+0
	anl a, #1111B
	movc a, @a+dptr
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #'C'
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, minutes
	anl a, #11110000B
	rr a
	rr a
	rr a
	rr a
	movc a, @a+dptr
	lcall LCD_put
	
	mov a, minutes
	anl a, #1111B
	movc a, @a+dptr
	lcall LCD_put
	
	mov a, #':'
	lcall LCD_put
	
	mov a, seconds
	anl a, #11110000B
	rr a
	rr a
	rr a
	rr a
	movc a, @a+dptr
	lcall LCD_put
	
	mov a, seconds
	anl a, #1111B
	movc a, @a+dptr
	lcall LCD_put
	
	ret
	
LCD_DISPLAY_RAMP_TO_SOAK:

	lcall Wait40us
	djnz R1, LCD_DISPLAY_RAMP_TO_SOAK

	; Move to first column of first row	
	mov a, #80H
	lcall LCD_command
		
	; Display letter A
	mov a, #'R'
	lcall LCD_put
	
	mov a, #'a'
	lcall LCD_put
	
	mov a, #'m'
	lcall LCD_put
	
	mov a, #'p'
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #'t'
	lcall LCD_put
	
	mov a, #'o'
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #'S'
	lcall LCD_put
	
	mov a, #'o'
	lcall LCD_put
	
	mov a, #'a'
	lcall LCD_put
	
	mov a, #'k'
	lcall LCD_put		
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #'d'					;; EVERYTHING FROM HERE UNTIL...
	lcall LCD_put
	
	mov a, #'e'
	lcall LCD_put
	
	mov a, #'f'
	lcall LCD_put
	
	mov a, #'g'
	lcall LCD_put
	
	mov a, #'h'
	lcall LCD_put
	
	mov a, #'i'
	lcall LCD_put
	
	mov a, #'j'
	lcall LCD_put
	
	mov a, #'k'
	lcall LCD_put
	
	mov a, #'l'
	lcall LCD_put
	
	mov a, #'m'
	lcall LCD_put
	
	mov a, #'n'
	lcall LCD_put
	
	mov a, #'o'
	lcall LCD_put
	
	mov a, #'p'
	lcall LCD_put
	
	mov a, #'q'
	lcall LCD_put
	
	mov a, #'r'
	lcall LCD_put
	
	mov a, #'s'
	lcall LCD_put
	
	mov a, #'t'
	lcall LCD_put
	
	mov a, #'u'
	lcall LCD_put
	
	mov a, #'v'
	lcall LCD_put
	
	mov a, #'w'
	lcall LCD_put
	
	mov a, #'x'
	lcall LCD_put
	
	mov a, #'y'
	lcall LCD_put
	
	mov a, #'z'
	lcall LCD_put
	
	mov a, #'y'
	lcall LCD_put			;; HERE IS NOT VISIBLE!
	
	mov dptr, #asciiLookup
	
	mov a, bcd+1
	anl a, #1111B
	movc a, @a+dptr
	lcall lcd_put
	
	mov a, bcd+0
	anl a, #11110000B
	rr a
	rr a
	rr a
	rr a
	movc a, @a+dptr
	lcall LCD_put
	
	mov a, bcd+0
	anl a, #1111B
	movc a, @a+dptr
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #'C'
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, minutes
	anl a, #11110000B
	rr a
	rr a
	rr a
	rr a
	movc a, @a+dptr
	lcall LCD_put
	
	mov a, minutes
	anl a, #1111B
	movc a, @a+dptr
	lcall LCD_put
	
	mov a, #':'
	lcall LCD_put
	
	mov a, seconds
	anl a, #11110000B
	rr a
	rr a
	rr a
	rr a
	movc a, @a+dptr
	lcall LCD_put
	
	mov a, seconds
	anl a, #1111B
	movc a, @a+dptr
	lcall LCD_put
	
	ret
	
LCD_DISPLAY_SOAK:

	lcall Wait40us
	djnz R1, LCD_DISPLAY_SOAK
	
	; Move to first column of first row	
	mov a, #80H
	lcall LCD_command

	mov a, #'S'
	lcall LCD_put
	
	mov a, #'o'
	lcall LCD_put
	
	mov a, #'a'
	lcall LCD_put
	
	mov a, #'k'
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put		
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #'d'					;; EVERYTHING FROM HERE UNTIL...
	lcall LCD_put
	
	mov a, #'e'
	lcall LCD_put
	
	mov a, #'f'
	lcall LCD_put
	
	mov a, #'g'
	lcall LCD_put
	
	mov a, #'h'
	lcall LCD_put
	
	mov a, #'i'
	lcall LCD_put
	
	mov a, #'j'
	lcall LCD_put
	
	mov a, #'k'
	lcall LCD_put
	
	mov a, #'l'
	lcall LCD_put
	
	mov a, #'m'
	lcall LCD_put
	
	mov a, #'n'
	lcall LCD_put
	
	mov a, #'o'
	lcall LCD_put
	
	mov a, #'p'
	lcall LCD_put
	
	mov a, #'q'
	lcall LCD_put
	
	mov a, #'r'
	lcall LCD_put
	
	mov a, #'s'
	lcall LCD_put
	
	mov a, #'t'
	lcall LCD_put
	
	mov a, #'u'
	lcall LCD_put
	
	mov a, #'v'
	lcall LCD_put
	
	mov a, #'w'
	lcall LCD_put
	
	mov a, #'x'
	lcall LCD_put
	
	mov a, #'y'
	lcall LCD_put
	
	mov a, #'z'
	lcall LCD_put
	
	mov a, #'y'
	lcall LCD_put			;; HERE IS NOT VISIBLE!
	
	mov dptr, #asciiLookup
	
	mov a, bcd+1
	anl a, #1111B
	movc a, @a+dptr
	lcall lcd_put
	
	mov a, bcd+0
	anl a, #11110000B
	rr a
	rr a
	rr a
	rr a
	movc a, @a+dptr
	lcall LCD_put
	
	mov a, bcd+0
	anl a, #1111B
	movc a, @a+dptr
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #'C'
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, minutes
	anl a, #11110000B
	rr a
	rr a
	rr a
	rr a
	movc a, @a+dptr
	lcall LCD_put
	
	mov a, minutes
	anl a, #1111B
	movc a, @a+dptr
	lcall LCD_put
	
	mov a, #':'
	lcall LCD_put
	
	mov a, seconds
	anl a, #11110000B
	rr a
	rr a
	rr a
	rr a
	movc a, @a+dptr
	lcall LCD_put
	
	mov a, seconds
	anl a, #1111B
	movc a, @a+dptr
	lcall LCD_put
	
	ret
	
	
LCD_DISPLAY_RAMP_TO_PEAK:

	lcall Wait40us
	djnz R1, LCD_DISPLAY_RAMP_TO_PEAK

	; Move to first column of first row	
	mov a, #80H
	lcall LCD_command
		
		; Display letter A
	mov a, #'R'
	lcall LCD_put
	
	mov a, #'a'
	lcall LCD_put
	
	mov a, #'m'
	lcall LCD_put
	
	mov a, #'p'
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #'t'
	lcall LCD_put
	
	mov a, #'o'
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #'P'
	lcall LCD_put
	
	mov a, #'e'
	lcall LCD_put
	
	mov a, #'a'
	lcall LCD_put
	
	mov a, #'k'
	lcall LCD_put		
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #'d'					;; EVERYTHING FROM HERE UNTIL...
	lcall LCD_put
	
	mov a, #'e'
	lcall LCD_put
	
	mov a, #'f'
	lcall LCD_put
	
	mov a, #'g'
	lcall LCD_put
	
	mov a, #'h'
	lcall LCD_put
	
	mov a, #'i'
	lcall LCD_put
	
	mov a, #'j'
	lcall LCD_put
	
	mov a, #'k'
	lcall LCD_put
	
	mov a, #'l'
	lcall LCD_put
	
	mov a, #'m'
	lcall LCD_put
	
	mov a, #'n'
	lcall LCD_put
	
	mov a, #'o'
	lcall LCD_put
	
	mov a, #'p'
	lcall LCD_put
	
	mov a, #'q'
	lcall LCD_put
	
	mov a, #'r'
	lcall LCD_put
	
	mov a, #'s'
	lcall LCD_put
	
	mov a, #'t'
	lcall LCD_put
	
	mov a, #'u'
	lcall LCD_put
	
	mov a, #'v'
	lcall LCD_put
	
	mov a, #'w'
	lcall LCD_put
	
	mov a, #'x'
	lcall LCD_put
	
	mov a, #'y'
	lcall LCD_put
	
	mov a, #'z'
	lcall LCD_put
	
	mov a, #'y'
	lcall LCD_put			;; HERE IS NOT VISIBLE!
	
	mov dptr, #asciiLookup
	
	mov a, bcd+1
	anl a, #1111B
	movc a, @a+dptr
	lcall lcd_put
	
	mov a, bcd+0
	anl a, #11110000B
	rr a
	rr a
	rr a
	rr a
	movc a, @a+dptr
	lcall LCD_put
	
	mov a, bcd+0
	anl a, #1111B
	movc a, @a+dptr
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #'C'
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, minutes
	anl a, #11110000B
	rr a
	rr a
	rr a
	rr a
	movc a, @a+dptr
	lcall LCD_put
	
	mov a, minutes
	anl a, #1111B
	movc a, @a+dptr
	lcall LCD_put
	
	mov a, #':'
	lcall LCD_put
	
	mov a, seconds
	anl a, #11110000B
	rr a
	rr a
	rr a
	rr a
	movc a, @a+dptr
	lcall LCD_put
	
	mov a, seconds
	anl a, #1111B
	movc a, @a+dptr
	lcall LCD_put
	
	ret
	
LCD_DISPLAY_REFLOW:

	lcall Wait40us
	djnz R1, LCD_DISPLAY_REFLOW

	; Move to first column of first row	
	mov a, #80H
	lcall LCD_command
		
		; Display letter A
	mov a, #'R'
	lcall LCD_put
	
	mov a, #'e'
	lcall LCD_put
	
	mov a, #'f'
	lcall LCD_put
	
	mov a, #'l'
	lcall LCD_put
	
	mov a, #'o'
	lcall LCD_put
	
	mov a, #'w'
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put		
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #'d'					;; EVERYTHING FROM HERE UNTIL...
	lcall LCD_put
	
	mov a, #'e'
	lcall LCD_put
	
	mov a, #'f'
	lcall LCD_put
	
	mov a, #'g'
	lcall LCD_put
	
	mov a, #'h'
	lcall LCD_put
	
	mov a, #'i'
	lcall LCD_put
	
	mov a, #'j'
	lcall LCD_put
	
	mov a, #'k'
	lcall LCD_put
	
	mov a, #'l'
	lcall LCD_put
	
	mov a, #'m'
	lcall LCD_put
	
	mov a, #'n'
	lcall LCD_put
	
	mov a, #'o'
	lcall LCD_put
	
	mov a, #'p'
	lcall LCD_put
	
	mov a, #'q'
	lcall LCD_put
	
	mov a, #'r'
	lcall LCD_put
	
	mov a, #'s'
	lcall LCD_put
	
	mov a, #'t'
	lcall LCD_put
	
	mov a, #'u'
	lcall LCD_put
	
	mov a, #'v'
	lcall LCD_put
	
	mov a, #'w'
	lcall LCD_put
	
	mov a, #'x'
	lcall LCD_put
	
	mov a, #'y'
	lcall LCD_put
	
	mov a, #'z'
	lcall LCD_put
	
	mov a, #'y'
	lcall LCD_put			;; HERE IS NOT VISIBLE!
	
	mov dptr, #asciiLookup
	
	mov a, bcd+1
	anl a, #1111B
	movc a, @a+dptr
	lcall lcd_put
	
	mov a, bcd+0
	anl a, #11110000B
	rr a
	rr a
	rr a
	rr a
	movc a, @a+dptr
	lcall LCD_put
	
	mov a, bcd+0
	anl a, #1111B
	movc a, @a+dptr
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #'C'
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, minutes
	anl a, #11110000B
	rr a
	rr a
	rr a
	rr a
	movc a, @a+dptr
	lcall LCD_put
	
	mov a, minutes
	anl a, #1111B
	movc a, @a+dptr
	lcall LCD_put
	
	mov a, #':'
	lcall LCD_put
	
	mov a, seconds
	anl a, #11110000B
	rr a
	rr a
	rr a
	rr a
	movc a, @a+dptr
	lcall LCD_put
	
	mov a, seconds
	anl a, #1111B
	movc a, @a+dptr
	lcall LCD_put
	
	ret
	
LCD_DISPLAY_COOLING:

	lcall Wait40us
	djnz R1, LCD_DISPLAY_COOLING

	; Move to first column of first row	
	mov a, #80H
	lcall LCD_command
		
		; Display letter A
	mov a, #'C'
	lcall LCD_put
	
	mov a, #'o'
	lcall LCD_put
	
	mov a, #'o'
	lcall LCD_put
	
	mov a, #'l'
	lcall LCD_put
	
	mov a, #'i'
	lcall LCD_put
	
	mov a, #'n'
	lcall LCD_put
	
	mov a, #'g'
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put		
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #'d'					;; EVERYTHING FROM HERE UNTIL...
	lcall LCD_put
	
	mov a, #'e'
	lcall LCD_put
	
	mov a, #'f'
	lcall LCD_put
	
	mov a, #'g'
	lcall LCD_put
	
	mov a, #'h'
	lcall LCD_put
	
	mov a, #'i'
	lcall LCD_put
	
	mov a, #'j'
	lcall LCD_put
	
	mov a, #'k'
	lcall LCD_put
	
	mov a, #'l'
	lcall LCD_put
	
	mov a, #'m'
	lcall LCD_put
	
	mov a, #'n'
	lcall LCD_put
	
	mov a, #'o'
	lcall LCD_put
	
	mov a, #'p'
	lcall LCD_put
	
	mov a, #'q'
	lcall LCD_put
	
	mov a, #'r'
	lcall LCD_put
	
	mov a, #'s'
	lcall LCD_put
	
	mov a, #'t'
	lcall LCD_put
	
	mov a, #'u'
	lcall LCD_put
	
	mov a, #'v'
	lcall LCD_put
	
	mov a, #'w'
	lcall LCD_put
	
	mov a, #'x'
	lcall LCD_put
	
	mov a, #'y'
	lcall LCD_put
	
	mov a, #'z'
	lcall LCD_put
	
	mov a, #'y'
	lcall LCD_put			;; HERE IS NOT VISIBLE!
	
	mov dptr, #asciiLookup
	
	mov a, bcd+1
	anl a, #1111B
	movc a, @a+dptr
	lcall lcd_put
	
	mov a, bcd+0
	anl a, #11110000B
	rr a
	rr a
	rr a
	rr a
	movc a, @a+dptr
	lcall LCD_put
	
	mov a, bcd+0
	anl a, #1111B
	movc a, @a+dptr
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #'C'
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, minutes
	anl a, #11110000B
	rr a
	rr a
	rr a
	rr a
	movc a, @a+dptr
	lcall LCD_put
	
	mov a, minutes
	anl a, #1111B
	movc a, @a+dptr
	lcall LCD_put
	
	mov a, #':'
	lcall LCD_put
	
	mov a, seconds
	anl a, #11110000B
	rr a
	rr a
	rr a
	rr a
	movc a, @a+dptr
	lcall LCD_put
	
	mov a, seconds
	anl a, #1111B
	movc a, @a+dptr
	lcall LCD_put
	ret
	
LCD_DISPLAY_CONTENTS_COOLED:

	lcall Wait40us
	djnz R1, LCD_DISPLAY_CONTENTS_COOLED

	; Move to first column of first row	
	mov a, #80H
	lcall LCD_command
		
		; Display letter A
	mov a, #'C'
	lcall LCD_put
	
	mov a, #'o'
	lcall LCD_put
	
	mov a, #'n'
	lcall LCD_put
	
	mov a, #'t'
	lcall LCD_put
	
	mov a, #'e'
	lcall LCD_put
	
	mov a, #'n'
	lcall LCD_put
	
	mov a, #'t'
	lcall LCD_put
	
	mov a, #'s'
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #'C'
	lcall LCD_put
	
	mov a, #'o'
	lcall LCD_put
	
	mov a, #'o'
	lcall LCD_put		
	
	mov a, #'l'
	lcall LCD_put
	
	mov a, #'e'
	lcall LCD_put
	
	mov a, #'d'
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #'d'					;; EVERYTHING FROM HERE UNTIL...
	lcall LCD_put
	
	mov a, #'e'
	lcall LCD_put
	
	mov a, #'f'
	lcall LCD_put
	
	mov a, #'g'
	lcall LCD_put
	
	mov a, #'h'
	lcall LCD_put
	
	mov a, #'i'
	lcall LCD_put
	
	mov a, #'j'
	lcall LCD_put
	
	mov a, #'k'
	lcall LCD_put
	
	mov a, #'l'
	lcall LCD_put
	
	mov a, #'m'
	lcall LCD_put
	
	mov a, #'n'
	lcall LCD_put
	
	mov a, #'o'
	lcall LCD_put
	
	mov a, #'p'
	lcall LCD_put
	
	mov a, #'q'
	lcall LCD_put
	
	mov a, #'r'
	lcall LCD_put
	
	mov a, #'s'
	lcall LCD_put
	
	mov a, #'t'
	lcall LCD_put
	
	mov a, #'u'
	lcall LCD_put
	
	mov a, #'v'
	lcall LCD_put
	
	mov a, #'w'
	lcall LCD_put
	
	mov a, #'x'
	lcall LCD_put
	
	mov a, #'y'
	lcall LCD_put
	
	mov a, #'z'
	lcall LCD_put
	
	mov a, #'y'
	lcall LCD_put			;; HERE IS NOT VISIBLE!
	
	mov dptr, #asciiLookup
	
	mov a, bcd+1
	anl a, #1111B
	movc a, @a+dptr
	lcall lcd_put
	
	mov a, bcd+0
	anl a, #11110000B
	rr a
	rr a
	rr a
	rr a
	movc a, @a+dptr
	lcall LCD_put
	
	mov a, bcd+0
	anl a, #1111B
	movc a, @a+dptr
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #'C'
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, seconds
	anl a, #11110000B
	rr a
	rr a
	rr a
	rr a
	movc a, @a+dptr
	lcall LCD_put
	
	mov a, seconds
	anl a, #1111B
	movc a, @a+dptr
	lcall LCD_put
	
	ret
	
; SSR FUNCTIONS_________________________________________________________________

Load_X MAC
	mov x+0, #low (%0 % 0x10000) 
	mov x+1, #high(%0 % 0x10000) 
	mov x+2, #low (%0 / 0x10000) 
	mov x+3, #high(%0 / 0x10000) 
ENDMAC

; TEMPSENSOR FUNCTIONS__________________________________________________________

END