; Reflow Oven: Group D1

; Manolito Pimentel
; Stephen Hu
; Adam Wong
; Connie Ma
; Kaibo Ma
; Daniel Kim

$MODDE2

org 0000H
   ljmp MyProgram

   
dseg at 30h
x:    		ds 2
y:    		ds 2
z:			ds 2
bcd:  		ds 3
op:			ds 1
temp 		ds 1
sec 		ds 1
pwm 		ds 1

bseg
mf:		dbit 1		

FREQ   EQU 33333333
BAUD   EQU 115200
T2LOAD EQU 65536-(FREQ/(32*BAUD))

$include(TempSensor.asm)
$include(ISR.asm)

CSEG

MISO   		EQU  P0.0 
MOSI   		EQU  P0.1 
SCLK   		EQU  P0.2
CE_ADC 		EQU  P0.3
CE_EE  		EQU  P0.4
CE_RTC 		EQU  P0.5
CLK 		EQU 33333333
FREQ_0 		EQU 50
FREQ_1 		EQU 500
TIMER0_RELOAD 	EQU 65536-(CLK/(12*2*FREQ_0))
TIMER1_RELOAD 	EQU 65536-(CLK/(12*FREQ_1))

myLUT:
    DB 0C0H, 0F9H, 0A4H, 0B0H, 099H        ; 0 TO 4
    DB 092H, 082H, 0F8H, 080H, 090H        ; 4 TO 9
    DB 0FFH	; all segments off

; DE2 SETUP____________________________________________________________________________________
    
MyProgram:
    MOV SP, #7FH
    mov LEDRA, #0
    mov LEDRB, #0
    mov LEDRC, #0
    mov LEDG, #0
    
    orl P0MOD, #00111000b 		; ASSUME P0.0 IS TURN ON SWITCH

	setb CE_ADC
	setb CE_EE
	clr  CE_RTC 				; RTC CE is active high

	lcall INIT_SPI    

; FOREVER LOOP________________________________________________________________________________

forever:
    
    mov b, #0  					; Read channel 0
	lcall Read_ADC_Channel
	
	lcall InitSerialPort
	
;see TempSensor.asm for temp conversion: temperature is stored in x: then converted to
;ascii and trasmitted serially
	lcall convertTemp

	
; TO-DO: FIX ERRORS WITH NEGATIVES:
	
	Load_y(0)
	lcall x_lteq_y
	mov a, mf
	jz skipzero
	mov x+0, #0
	mov x+1, #0

skipzero:
	
	lcall hex2bcd
    lcall hyperterminal
	lcall display
	
	lcall waitonesec				

    sjmp forever   


END
