$MODDE2

org 0000H
	ljmp MyProgram

MISO   EQU  P0.0 ; this one has the output from ADC
MOSI   EQU  P0.1 
SCLK   EQU  P0.2
CE_ADC EQU  P0.3

CE_EE  EQU  P0.4
CE_RTC EQU  P0.5 


FREQ   EQU 33333333
BAUD   EQU 115200
T2LOAD EQU 65536-(FREQ/(32*BAUD))

DSEG at 30H
hotTemp:  ds 2
coldTemp: ds 2
totalTemp:ds 2
x:        ds 2
y:        ds 2
z:		  ds 2
bcd:	  ds 6

;DSEG AT 90H
;hotTemp: ds 2
;coldTemp: ds 2

BSEG
mf:     dbit 1

CSEG

$include(math16.asm)


; Look-up table for 7-seg displays
myLUT:
    DB 0C0H, 0F9H, 0A4H, 0B0H, 099H        ; 0 TO 4
    DB 092H, 082H, 0F8H, 080H, 090H        ; 4 TO 9

myLUT1:
	DB '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' 

myLUT2:
	DB '\n'

myLUT3:
	DB '\r'



INIT_SPI:
    orl P0MOD, #00000110b ; Set SCLK, MOSI as outputs
    anl P0MOD, #11111110b ; Set MISO as input
    clr SCLK              ; For mode (0,0) SCLK is zero
    
    clr TR2 ; Disable timer 2
	mov T2CON, #30H ; RCLK=1, TCLK=1 
	mov RCAP2H, #high(T2LOAD)  
	mov RCAP2L, #low(T2LOAD)
	setb TR2 ; Enable timer 2
	mov SCON, #52H
	
	ret
	
DO_SPI_G:
	push acc
    mov R1, #0            ; Received byte stored in R1
    mov R2, #8            ; Loop counter (8-bits)
DO_SPI_G_LOOP:
    mov a, R0             ; Byte to write is in R0
    rlc a                 ; Carry flag has bit to write
    mov R0, a
    mov MOSI, c
    setb SCLK             ; Transmit
    mov c, MISO           ; Read received bit
    mov a, R1             ; Save received bit in R1
    rlc a
    mov R1, a
    clr SCLK
    djnz R2, DO_SPI_G_LOOP
    pop acc
    ret

Delay:
	mov R3, #5
Delay_loop:
	djnz R3, Delay_loop
	ret


; Channel to read passed in register b
Read_ADC_Channel:
	clr CE_ADC
	mov R0, #00000001B ; Start bit:1
	lcall DO_SPI_G
	
	mov a, b ; clears A AND CLEARS B
	swap a
	anl a, #0F0H ; A STILL ZERO
	setb acc.7 ; Single mode (bit 7). A IS 1 NOW
	
	mov R0, a ;  Select channel R0 HAS ONE NOW
	lcall DO_SPI_G
	mov a, R1          ; R1 contains bits 8 and 9
	anl a, #03H ; ONLY GET LEAST 2 SIG BITS
	mov R7, a ; R7 HAS TWO MOST SIG BITS FROM 10 BIT ADC MCP
	
	mov R0, #55H ; It doesn't matter what we transmit...
	lcall DO_SPI_G
	mov a, R1    ; R1 contains bits 0 to 7
	mov R6, a ; WE DIDN'T DO THE ANL THING ABOVE SO WE GET ALL THE BITS
	; END OF THIS IS R6 HAS BITS 0 TO 7, R7 HAS BITS 8 AND 9
	setb CE_ADC ; STOP THIS AND REPEAT TO GET DA NEW VAL DOE
	ret
	

SendString:
    CLR A
    MOVC A, @A+DPTR
    JZ SSDone
    LCALL putchar
    INC DPTR
    SJMP SendString
SSDone:
    ret


;need to call putchar 10 times. send digit one at a time, MSB first
send_number:
	push acc
	clr a
	
	mov dptr, #myLUT1
	
	;took out rest of digits
	
	;least significant digits
	mov a, bcd+1
	anl a, #0fh
	jz	noHundred
	movc a, @a+dptr
	;orl a, #30h ; Convert to ASCII
	lcall putchar

noHundred:
	mov a, bcd+0
	swap a
	anl a, #0fh
	movc a, @a+dptr
	;orl a, #30h ; Convert to ASCII
	lcall putchar
	
	mov a, bcd+0
	anl a, #0fh
	movc a, @a+dptr
	;orl a, #30h ; Convert to ASCII
	lcall putchar
	
	;linebreak
	mov dptr, #myLUT2
	mov a, #0
	movc a, @a+dptr
	;orl a, #30h
	lcall putchar
	
	;align left
	;mov dptr, #myLUT3
	;mov a, #0
	;movc a, @a+dptr
	;orl a, #30h
	;lcall putchar
	
	pop acc
	ret

putchar:
    JNB TI, putchar
    CLR TI
    MOV SBUF, a
    RET
    
WaitHalfSec:
	mov R2, #90
L3: mov R1, #250
L2: mov R0, #250
L1: djnz R0, L1
	djnz R1, L2
	djnz R2, L3
	ret    

Get_Temperature_Hot:
	mov b, #0 ; Thermocouple is connected to channel 0
	push x
	;push acc
	lcall Read_ADC_Channel
	
	mov x+1, R7
	mov x+0, R6
	
	;mov y, #2 ; FOR SOME REASON THIS IS OVERFLOWING AND HAVING PROBLEMS NOW EVEN DOING ANY OF
	;lcall div16 ; x = x/y
	;mov a, x
	;rrc a
	

	;rlc
	;mov y, #12
	;;;;load_y(15)
	;;;;lcall mul16
	
	;mov y, #5
	;;;;load_y(45)
	;;;;lcall div16 
	;mov x, a
	
	;load_y(247)
	;;;;load_y(7)
	;load_y(108)
	;;;;lcall sub16
	; try using add16 instead
	
	;rlc
	;mov y, #12
	load_y(22)
	lcall mul16
	
	;mov y, #5
	load_y(51)
	lcall div16 
	;mov x, a
	
	;load_y(247)
	;load_y(4)
	load_y(136)
	lcall sub16
	; try using add16 instead
	
	
	;mov y, #10 ; NOTE THAT TEMP IS 10X FACTOR GREATER THIS IS GOOD
	;lcall div16 ; x = x/yTHIS IS GOOD
	
	;mov y, #10
	;lcall div16 ; x = x/y
	
	;mov y, #48 ;THIS IS GOOD
	;lcall mul16 ; x = x/y THIS IS GOOD
	mov hotTemp, x 
	
	;lcall hex2bcd
	;pop acc
	pop x
	
	ret    
	

Get_Temperature_Cold:
	mov b, #1 ; LM335 temperature sensor is connected to channel 1 (will read from channel 1 now)
	push x
	lcall Read_ADC_Channel
	;lcall InitSerialPort
	
	;mov x+0, c
	mov x+1, R7
	mov x+0, R6
	

	
	
	
	
	; The temperature can be calculated as (ADC*500/1024)-273 (may overflow 16 bit operations)
	; or (ADC*250/512)-273 (may overflow 16 bit operations)
	; or (ADC*125/256)-273 (may overflow 16 bit operations)
	; or (ADC*62/256)+(ADC*63/256)-273 (Does not overflow 16 bit operations!)
	
	Load_y(62)
	lcall mul16
	mov R4, x+1

	mov x+1, R7
	mov x+0, R6

	Load_y(63)
	lcall mul16
	mov R5, x+1
	
	mov x+0, R4
	mov x+1, #0
	mov y+0, R5
	mov y+1, #0
	lcall add16
	
	Load_y(278) ; this is calibrated more accurately (note that it is 10x factor)
	lcall sub16
	
	
	
	
	;mov R4, x+1

	;mov x+1, R7
	;mov x+0, R6


	;mov R5, x+1
	
	;mov x+0, R4
	;mov x+1, #0
	;mov y+0, R5
	;mov y+1, #0
	;mov LEDRA, x
	mov coldTemp, x

	
	;lcall hex2bcd
	pop x
	
	ret
	

Get_Temperature_Total:
	push x
	push acc
	push totalTemp
	
	;lcall Get_Temperature_Hot
	;lcall Get_Temperature_Cold

	mov a, #0
	mov a, hotTemp ; hot temp is weird right now in get_temp_total, is fine by itself
	;mov a, coldTemp
	add a, coldTemp
	mov totaltemp, a
	mov x, totalTemp
	
	;load_y(10)
	;lcall div16
	
	
	;mov R3, LOW(hotTemp)
	;mov R4, high(hotTemp)
	;add R3, LOW(coldTemp)
	;add R4, high(coldTemp)
	;mov LOW(totalTemp), R3
	;mov high(totalTemp), R4
	;mov x+0, LOW(totalTemp)
	;mov x+1, HIGH(totalTemp)
	
	lcall hex2bcd
	pop totalTemp
	pop acc
	pop x
	ret
	

Display_temp:
	mov dptr, #myLUT
	; Display Digit 0
    mov A, bcd+0
    anl a, #0fh
    movc A, @A+dptr
    mov HEX0, A
	; Display Digit 1
    mov A, bcd+0
    swap a
    anl a, #0fh
    movc A, @A+dptr
    mov HEX1, A
    ; Third digit needs help
    mov A, bcd+1
    anl a, #0fh
    movc A, @A+dptr
    mov HEX2, A
    ret    
    

MyProgram:
	mov sp, #07FH
	clr a
	mov LEDG,  a
	mov LEDRA, a
	mov LEDRB, a
	mov LEDRC, a
	orl P0MOD, #00111000b ; make all CEs outputs

	setb CE_ADC
	setb CE_EE
	clr  CE_RTC ; RTC CE is active high

	lcall INIT_SPI
	
    LCALL SendString
	
Forever:
	;mov b, #0  ; Read channel 0
	mov x+0, #0
	mov x+1, #0
	mov bcd+0, #0
	mov bcd+1, #0
	mov bcd+2, #0
	mov bcd+3, #0
	mov bcd+4, #0
	mov bcd+5, #0
	mov bcd+6, #0
	mov y, #0
	;lcall Read_ADC_Channel
	
	lcall Get_Temperature_Hot
	lcall Get_Temperature_Cold
	lcall Get_Temperature_Total
	;mov x+2, #0
	
	;mov x+0, R6 ;move bits 0-7 to x
;	mov x+1, R7 ;move bits 8 and 9 to x

	lcall Delay
	
	
	
	;can't divide by 2 - value from ADC is 1.8 times analog. 
	;multiply by 18
	;divide by 10
	
	
	;covert to bcd
	;lcall hex2bcd ;bcd holds the values 40-bits

	lcall Display_temp
	lcall send_number

	
	;push  AR2
	;push  AR1
	;push  AR0
	lcall WaitHalfSec
	lcall WaitHalfSec
	;pop AR0
	;pop AR1
	;pop AR2
	
	;mov LEDRB, R7
	;mov LEDRA, R6
	

	
	lcall Delay

	
	sjmp Forever
	
END
