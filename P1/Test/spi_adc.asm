$MODDE2
org 0000H
   ljmp MyProgram

MISO   EQU  P0.0 
MOSI   EQU  P0.1 
SCLK   EQU  P0.2
CE_ADC EQU  P0.3
CE_EE  EQU  P0.4
CE_RTC EQU  P0.5 

FREQ   EQU 33333333
BAUD   EQU 115200
T2LOAD EQU 65536-(FREQ/(32*BAUD))

myLUT:
    DB '30H', '31H', '32H', '33H', '34H', '35H', '36H', '37H', '38H', '39H', '\r', '\n', 0        ; 4 TO 9

INIT_SPI:
    orl P0MOD, #00000110b ; Set SCLK, MOSI as outputs
    anl P0MOD, #11111110b ; Set MISO as input
    clr SCLK              ; For mode (0,0) SCLK is zero
    
    
    clr TR2 ; Disable timer 2 ;
	mov T2CON, #30H ; RCLK=1, TCLK=1  ;
	mov RCAP2H, #high(T2LOAD)   ;
	mov RCAP2L, #low(T2LOAD) ;
	setb TR2 ; Enable timer 2 ;
	mov SCON, #52H ;
	
	
	ret
	
DO_SPI_G:
	push acc
    mov R1, #0            ; Received byte stored in R1
    mov R2, #8            ; Loop counter (8-bits)
DO_SPI_G_LOOP:
    mov a, R0             ; Byte to write is in R0
    rlc a                 ; Carry flag has bit to write
    mov R0, a
    mov MOSI, c
    setb SCLK             ; Transmit
    mov c, MISO           ; Read received bit
    mov a, R1             ; Save received bit in R1
    rlc a
    mov R1, a
    clr SCLK
    djnz R2, DO_SPI_G_LOOP
    pop acc
    ret

Delay:
	mov R3, #20
Delay_loop:
	djnz R3, Delay_loop
	ret


; Channel to read passed in register b
Read_ADC_Channel:
	clr CE_ADC
	mov R0, #00000001B ; Start bit:1
	lcall DO_SPI_G
	
	mov a, b
	swap a
	anl a, #0F0H
	setb acc.7 ; Single mode (bit 7).
	
	mov R0, a ;  Select channel
	lcall DO_SPI_G
	mov a, R1          ; R1 contains bits 8 and 9
	anl a, #03H
	mov R7, a
	
	mov R0, #55H ; It doesn't matter what we transmit...
	lcall DO_SPI_G
	mov a, R1    ; R1 contains bits 0 to 7
	mov R6, a
	setb CE_ADC
	ret
	
MyProgram:
	mov sp, #07FH
	clr a
	mov LEDG,  a
	mov LEDRA, a
	mov LEDRB, a
	mov LEDRC, a
	orl P0MOD, #00111000b ; make all CEs outputs

	setb CE_ADC
	setb CE_EE
	clr  CE_RTC ; RTC CE is active high

	lcall INIT_SPI
	
	
	;LCALL InitSerialPort ;
	lcall Read_ADC_Channel ; brand new
	;MOV a, R6
    MOV DPTR, #myLut ;
    LCALL SendString ;
	
	
	
Forever:
	mov b, #0  ; Read channel 0
	lcall Read_ADC_Channel
	
	mov LEDRB, R7
	mov LEDRA, R6
	
	lcall Delay
	
	sjmp Forever
	
putchar:
    JNB TI, putchar
    CLR TI
    MOV SBUF, a
    RET

; all new from here below

SendString:
    CLR A
    MOVC A, @A+DPTR
    JZ SSDone
    LCALL putchar
    INC DPTR
    SJMP SendString
SSDone:
    ret
 
Hello_World:
    DB  'Hello, World!', '\r', '\n', 0

;MyProgram:
 ;   MOV SP, #7FH
  ;  mov LEDRA, #0
   ; mov LEDRB, #0
    ;mov LEDRC, #0
 ;   mov LEDG, #0
    
 ;   LCALL InitSerialPort
  ;  MOV DPTR, #Hello_World
   ; LCALL SendString
	
END
