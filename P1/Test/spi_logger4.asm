; spi_logger2.asm: This program uses a MCP3004, 25LC320, and DS1306 ICs
; to implement a temperature data logger.  This version uses the LCD to
; display date and time.  Uses 7-segment displays to show temperature.

$MODDE2
org 0000H
   ljmp MyProgram
   
org 0003H
   ljmp Ext0_ISR

FREQ   EQU 33333333
BAUD   EQU 115200
T2LOAD EQU 65536-(FREQ/(32*BAUD))

; Microchip 25LC320 Slave EEPROM Parameters and Instruction Set
EEPROM_CMD_WRSR   EQU  01H 
EEPROM_CMD_WRITE  EQU  02H 
EEPROM_CMD_READ   EQU  03H 
EEPROM_CMD_WRDI   EQU  04H 
EEPROM_CMD_RDSR   EQU  05H
EEPROM_CMD_WREN   EQU  06H 

MISO   EQU  P0.0 
MOSI   EQU  P0.1 
SCLK   EQU  P0.2
CE_ADC EQU  P0.3
CE_EE  EQU  P0.4
CE_RTC EQU  P0.5 

DSEG at 30H
hours:    ds 1
minutes:  ds 1
seconds:  ds 1
day:      ds 1
month:    ds 1
year:     ds 1
x:        ds 2
y:        ds 2
bcd:	  ds 3
old_temp: ds 1
new_temp: ds 1

BSEG
mf:     dbit 1

CSEG

$include(math16.asm)

; Look-up table for 7-seg displays
myLUT:
    DB 0C0H, 0F9H, 0A4H, 0B0H, 099H        ; 0 TO 4
    DB 092H, 082H, 0F8H, 080H, 090H        ; 4 TO 9

Ext0_ISR:
	jb swa.1, do_nothing
		
	push psw
	push acc
	push dpl
	push dph
	push b

	mov PSW, #00001000B ; Select register bank 1!

	lcall Get_Temperature

	mov dpl, #02H
	lcall RTC_Read
	mov hours, a
	mov dpl, #01H
	lcall RTC_Read
	mov minutes, a
	mov dpl, #00H
	lcall RTC_Read
	mov seconds, a

	mov dpl, #06H
	lcall RTC_Read
	mov year, a
	mov dpl, #05H
	lcall RTC_Read
	mov month, a
	mov dpl, #04H
	lcall RTC_Read
	mov day, a
	
	clr c
	mov a, old_temp
	subb a, new_temp
	jz display_time_and_date
	lcall save_date_time_temp
	mov old_temp, new_temp

display_time_and_date:
	
	lcall display_time
	lcall display_date

all_done:
	pop b
	pop dph
	pop dpl
	pop acc
	pop psw
do_nothing:	
	reti


INIT_SPI:
    orl P0MOD, #00000110b ; Set SCLK, MOSI as outputs
    anl P0MOD, #11111110b ; Set MISO as input
    clr SCLK              ; For mode (0,0) SCLK is zero
	ret
	
DO_SPI_G:
	push acc
    mov R1, #0            ; Received byte stored in R1
    mov R2, #8            ; Loop counter (8-bits)
DO_SPI_G_LOOP:
    mov a, R0             ; Byte to write is in R0
    rlc a                 ; Carry flag has bit to write
    mov R0, a
    mov MOSI, c
    setb SCLK             ; Transmit
    nop                   ; Output form RTC needs a bit of extra time!
    nop
    mov c, MISO           ; Read received bit
    mov a, R1             ; Save received bit in R1
    rlc a
    mov R1, a
    clr SCLK
    djnz R2, DO_SPI_G_LOOP
    pop acc
    ret

; dpl has the address, b has the data
RTC_Write:
	setb CE_RTC
	mov	a,#0x80
	orl	a, dpl
	mov	R0, a
	lcall DO_SPI_G
	mov	R0, b
	lcall DO_SPI_G
	clr	CE_RTC
	ret

; dpl has the address, value returned in accumulator
RTC_Read:
	setb CE_RTC
	mov	a,#0x7F
	anl	a, dpl
	mov	R0,a
	lcall DO_SPI_G
	mov	R0,#0x00 ; Any value will do
	lcall DO_SPI_G
	clr	CE_RTC
	mov	a, R1
	ret

DS1306_Write mac
	mov dpl, #%0
	mov b, #%1
	lcall RTC_Write
endmac

Set_time mac
	mov dpl, #82H
	mov b, #%0
	lcall RTC_Write
	mov dpl, #81H
	mov b, #%1
	lcall RTC_Write
	mov dpl, #80H
	mov b, #%2
	lcall RTC_Write
endmac

Set_date mac
	mov dpl, #86H
	mov b, #%0
	lcall RTC_Write
	mov dpl, #85H
	mov b, #%1
	lcall RTC_Write
	mov dpl, #84H
	mov b, #%2
	lcall RTC_Write
endmac

display_time:
	; Move to first column of second row	
	mov a, #0C0H
	lcall LCD_command

	mov a, hours
	swap a
	anl a, #0fh
	orl a, #30h
	lcall LCD_put
	mov a, hours
	anl a, #0fh
	orl a, #30h
	lcall LCD_put

	mov a, #':'
	lcall LCD_put

	mov a, minutes
	swap a
	anl a, #0fh
	orl a, #30h
	lcall LCD_put
	mov a, minutes
	anl a, #0fh
	orl a, #30h
	lcall LCD_put

	mov a, #':'
	lcall LCD_put
	
	mov a, seconds
	swap a
	anl a, #0fh
	orl a, #30h
	lcall LCD_put
	mov a, seconds
	anl a, #0fh
	orl a, #30h
	lcall LCD_put

	ret

display_date:
	
	; Move to first column of first row	
	mov a, #80H
	lcall LCD_command

	mov a, day
	swap a
	anl a, #0fh
	orl a, #30h
	lcall LCD_put
	mov a, day
	anl a, #0fh
	orl a, #30h
	lcall LCD_put

	mov a, #'/'
	lcall LCD_put

	mov a, month
	swap a
	anl a, #0fh
	orl a, #30h
	lcall LCD_put
	mov a, month
	anl a, #0fh
	orl a, #30h
	lcall LCD_put

	mov a, #'/'
	lcall LCD_put


	mov a, #'2'
	lcall LCD_put
	mov a, #'0'
	lcall LCD_put
	mov a, year
	swap a
	anl a, #0fh
	orl a, #30h
	lcall LCD_put
	mov a, year
	anl a, #0fh
	orl a, #30h
	lcall LCD_put

	ret

settime:
	lcall display_time
	jnb swa.1, settime_done

	jb key.1, settime_L1
	jnb key.1, $
	mov a, seconds
	add a, #1
	da a
	mov seconds, a
	cjne a, #60H, settime_L1
	mov seconds, #0

settime_L1:
	jb key.2, settime_L2
	jnb key.2, $
	mov a, minutes
	add a, #1
	da a
	mov minutes, a
	cjne a, #60H, settime_L2
	mov minutes, #0

settime_L2:
	jb key.3, settime_L3
	jnb key.3, $
	mov a, hours
	add a, #1
	da a
	mov hours, a
	cjne a, #24H, settime_L3
	mov hours, #0

settime_L3:
	ljmp settime
	
settime_done:
	mov dpl, #82H
	mov b, hours
	lcall RTC_Write
	mov dpl, #81H
	mov b, minutes
	lcall RTC_Write
	mov dpl, #80H
	mov b, seconds
	lcall RTC_Write
	ret

setdate:
	lcall display_date
	jnb swa.1, setdate_done

	jb key.3, setdate_L1
	jnb key.3, $
	mov a, day
	add a, #1
	da a
	mov day, a
	cjne a, #32H, setdate_L1
	mov day, #1

setdate_L1:
	jb key.2, setdate_L2
	jnb key.2, $
	mov a, month
	add a, #1
	da a
	mov month, a
	cjne a, #13H, setdate_L2
	mov month, #1

setdate_L2:
	jb key.1, setdate_L3
	jnb key.1, $
	mov a, year
	add a, #1
	da a
	mov year, a
	cjne a, #99H, setdate_L3
	mov year, #0

setdate_L3:
	ljmp setdate
	
setdate_done:
	mov dpl, #86H
	mov b, year
	lcall RTC_Write
	mov dpl, #85H
	mov b, month
	lcall RTC_Write
	mov dpl, #84H
	mov b, day
	lcall RTC_Write
	ret

Display_temp:
	mov HEX2, #0FFH
	mov x+0, new_temp
	mov x+1, #0
	mov a, new_temp
	jnb acc.7, temp_is_positive
	Load_x(0)
	mov y+0, new_temp
	mov y+1, #0ffh
	lcall sub16
	mov HEX2, #10111111B
	
temp_is_positive:
	lcall hex2bcd	
	mov dptr, #myLUT
	; Display Digit 0
    mov A, bcd+0
    anl a, #0fh
    movc A, @A+dptr
    mov HEX0, A
	; Display Digit 1
    mov A, bcd+0
    swap a
    anl a, #0fh
    movc A, @A+dptr
    mov HEX1, A
    ret

; Channel to read passed in register b
Read_ADC_Channel:
	clr CE_ADC
	mov R0, #00000001B ; Start bit:1
	lcall DO_SPI_G
	
	mov a, b
	swap a
	anl a, #0F0H
	setb acc.7 ; Single mode (bit 7).
	
	mov R0, a ;  Select channel
	lcall DO_SPI_G
	mov a, R1          ; R1 contains bits 8 and 9
	anl a, #03H
	mov R7, a
	
	mov R0, #55H ; It doesn't matter what we transmit...
	lcall DO_SPI_G
	mov a, R1    ; R1 contains bits 0 to 7
	mov R6, a
	setb CE_ADC
	ret

Get_Temperature:
	mov b, #0 ; LM335 temperature sensor is connected to channel 0
	lcall Read_ADC_Channel
	
	mov x+1, R7
	mov x+0, R6
	
	; The temperature can be calculated as (ADC*500/1024)-273 (may overflow 16 bit operations)
	; or (ADC*250/512)-273 (may overflow 16 bit operations)
	; or (ADC*125/256)-273 (may overflow 16 bit operations)
	; or (ADC*62/256)+(ADC*63/256)-273 (Does not overflow 16 bit operations!)
	
	Load_y(62)
	lcall mul16
	mov R4, x+1

	mov x+1, R7
	mov x+0, R6

	Load_y(63)
	lcall mul16
	mov R5, x+1
	
	mov x+0, R4
	mov x+1, #0
	mov y+0, R5
	mov y+1, #0
	lcall add16
	
	Load_y(273)
	lcall sub16
	
	mov new_temp, x+0
	lcall Display_temp
	
	ret

InitSerialPort:
	; Configure serial port and baud rate
	clr TR2 ; Disable timer 2
	mov T2CON, #30H ; RCLK=1, TCLK=1 
	mov RCAP2H, #high(T2LOAD)  
	mov RCAP2L, #low(T2LOAD)
	setb TR2 ; Enable timer 2
	mov SCON, #52H
	ret

putchar:
    JNB TI, putchar
    CLR TI
    MOV SBUF, a
    RET

SendString:
    CLR A
    MOVC A, @A+DPTR
    JZ SSDone
    LCALL putchar
    INC DPTR
    SJMP SendString
SSDone:
    ret

send_number:
	push acc
	swap a
	anl a, #0fh
	orl a, #30h ; Convert to ASCII
	lcall putchar
	pop acc
	anl a, #0fh
	orl a, #30h ; Convert to ASCII
	lcall putchar
	ret

Delay:
	mov R3, #20
Delay_loop:
	djnz R3, Delay_loop
	ret
	
EEPROM_Write: ; DPTR: address, Acc: value to write
	; Step 1: Set the Write Enable Latch to 1
	clr CE_EE ; Activate EEPROM
	mov R0, #EEPROM_CMD_WREN
	lcall DO_SPI_G
	setb CE_EE
	lcall Delay
	; Step 2: Send the WRITE command
	clr CE_EE
	mov R0, #EEPROM_CMD_WRITE
	lcall DO_SPI_G
	; Step 3: Send the EEPROM destination address (MSB first)
	mov R0, DPH
	lcall DO_SPI_G
	mov R0, DPL
	lcall DO_SPI_G
	; Step 4: Send the byte to write
	mov R0, A
	lcall DO_SPI_G
	setb CE_EE ; Deactivate EEPROM after write
	; Step 5: Poll on the Write In Progress (WIP) bit in the Status Register
EEPROM_Write_Loop:
	lcall Delay
	clr CE_EE
	mov R0, #EEPROM_CMD_RDSR ; Send the Read Status Register command
	lcall DO_SPI_G
	mov R0, #0
	lcall DO_SPI_G ; Dummy write to output serial clock
	setb CE_EE     ; Deactivate EEPROM after read
	mov a, R1
	anl a, #0x01
	jnz EEPROM_Write_Loop
	ret

EEPROM_Read: ; DPTR: Address, Acc: Value read
	; Step 1: Send the READ command
	clr CE_EE ; Activate EEPROM
	mov R0, #EEPROM_CMD_READ
	lcall DO_SPI_G
	
	; Step2: Send the EEPROM destination address (MSB first)
	mov R0, DPH
	lcall DO_SPI_G
	mov R0, DPL
	lcall DO_SPI_G
	
	; Step3: Read the value returned
	mov R0, #0 ; Dummy write to output serial clock
	lcall DO_SPI_G
	setb CE_EE ; Deactivate EEPROM
	lcall Delay
	
	mov a, R1
	ret

save_date_time_temp:
	; The current memory location in the EEPROM is stored in the
	; non-volatile ram of the RTC
	mov dpl, #21H
	lcall RTC_Read
	mov dph, a
	mov dpl, #20H
	lcall RTC_Read
	mov dpl, a
	
	; Make sure we don't try to write over EEPROM capacity (4096 bytes)
	clr c
	mov a, dpl
	subb a, #low(4095-7)
	mov a, dph
	subb a, #high(4095-7)
	jc it_is_good_to_save
	ret ; Memory is full

it_is_good_to_save:
	mov a, year
	lcall EEPROM_Write
	inc dptr
	mov a, month
	lcall EEPROM_Write
	inc dptr
	mov a, day
	lcall EEPROM_Write
	inc dptr
	mov a, hours
	lcall EEPROM_Write
	inc dptr
	mov a, minutes
	lcall EEPROM_Write
	inc dptr
	mov a, seconds
	lcall EEPROM_Write
	inc dptr
	mov a, new_temp
	lcall EEPROM_Write
	inc dptr
	
	; Save the new memory location in EEPROM to the RTC non-volatile area
	mov b, dpl
	mov dpl, #20H
	lcall RTC_Write
	mov b, dph
	mov dpl, #21H
	lcall RTC_Write
	
	ret
	
send_log:
	clr EA ; No interrupts while sending our data log
	
	; Read the current address in the EEPROM from the RTC non-volatile area
	; save it to [R7, R6] for future use.
	mov dpl, #21H
	lcall RTC_Read
	mov R7, a
	mov dpl, #20H
	lcall RTC_Read
	mov R6, a
	
	mov dptr, #0
	
send_log_loop:
	clr c
	mov a, dpl
	subb a, R6
	mov a, dph
	subb a, R7
	jc do_send_log_dump
	setb EA ; Enable interrupts before returning
	ret
	
do_send_log_dump:
	mov a, #'2'
	lcall putchar
	mov a, #'0'
	lcall putchar
	
	lcall EEPROM_Read
	inc dptr
	lcall send_number
	mov a, #'/'
	lcall putchar

	lcall EEPROM_Read
	inc dptr
	lcall send_number
	mov a, #'/'
	lcall putchar

	lcall EEPROM_Read
	inc dptr
	lcall send_number
	mov a, #' '
	lcall putchar

	lcall EEPROM_Read
	inc dptr
	lcall send_number
	mov a, #':'
	lcall putchar

	lcall EEPROM_Read
	inc dptr
	lcall send_number
	mov a, #':'
	lcall putchar

	lcall EEPROM_Read
	inc dptr
	lcall send_number
	mov a, #' '
	lcall putchar
	
	lcall EEPROM_Read
	inc dptr

	mov x+0, a
	mov x+1, #0
	jnb acc.7, temp_is_positive2
	Load_x(0)
	mov y+0, a
	mov y+1, #0ffh
	lcall sub16
	mov a, #'-'
	lcall putchar
	
temp_is_positive2:
	lcall hex2bcd
	mov a, bcd+0	
	lcall send_number
	
	mov a, #'C'
	lcall putchar
	mov a, #'\n'
	lcall putchar
	mov a, #'\r'
	lcall putchar
	
	ljmp send_log_loop


Wait40us:
	mov R0, #149
Wait40us_L0: 
	nop
	nop
	nop
	nop
	nop
	nop
	djnz R0, Wait40us_L0 ; 9 machine cycles-> 9*30ns*149=40us
    ret

LCD_command:
	mov	LCD_DATA, A
	clr	LCD_RS
	nop
	nop
	setb LCD_EN ; Enable pulse should be at least 230 ns
	nop
	nop
	nop
	nop
	nop
	nop
	clr	LCD_EN
	ljmp Wait40us

LCD_put:
	mov	LCD_DATA, A
	setb LCD_RS
	nop
	nop
	setb LCD_EN ; Enable pulse should be at least 230 ns
	nop
	nop
	nop
	nop
	nop
	nop
	clr	LCD_EN
	ljmp Wait40us

LCD_Init:
    ; Turn LCD on, and wait a bit.
    setb LCD_ON
    clr LCD_EN  ; Default state of enable must be zero
    lcall Wait40us
    
    mov LCD_MOD, #0xff ; Use LCD_DATA as output port
    clr LCD_RW ;  Only writing to the LCD in this code.
	
	mov a, #0ch ; Display on command
	lcall LCD_command
	mov a, #38H ; 8-bits interface, 2 lines, 5x7 characters
	lcall LCD_command
	mov a, #01H ; Clear screen (Warning, very slow command!)
	lcall LCD_command
    
    ; Delay loop needed for 'clear screen' command above (1.6ms at least!)
    mov R1, #40
Clr_loop:
	lcall Wait40us
	djnz R1, Clr_loop
	ret

MyProgram:
	mov sp, #07FH
	clr a
	mov LEDG,  a
	mov LEDRA, a
	mov LEDRB, a
	mov LEDRC, a
	
	setb IT0 ; External 0 interrupt is edge trigered
	setb EX0 ; Enable external 0 interrupt
	setb EA  ; Enable all interrupts
	
	orl P0MOD, #00111000b ; make all CEs outputs

	setb CE_ADC
	setb CE_EE
	clr  CE_RTC ; RTC CE is active high

	lcall INIT_SPI
    lcall InitSerialPort
    lcall LCD_Init
    
    mov old_temp,#0

	DS1306_Write(0x8f, 0x04); Enable writing to the DS1306.  Also, enable 1 Hz output.
	DS1306_Write(0x91, 0xa5); Enable trickle charger with one diode and 2k
	
Forever:
	jb RI, do_serial
	jb swa.1, do_set
	jnb key.1, Clear_Log
	jnb key.3, Do_send_log
	sjmp Forever
	
do_set:
	jb swa.0, do_set2
	lcall settime
	sjmp Forever
do_set2:
	lcall setdate
	sjmp Forever
	
Clear_log:
	clr EA
	jnb key.1, Clear_Log
	; Set the begining of our log to zero
	mov b, #0
	mov dpl, #20H
	lcall RTC_Write
	mov b, #0
	mov dpl, #21H
	lcall RTC_Write
	setb EA
	sjmp Forever

Do_send_log:
	jnb key.3, Do_send_log
	lcall send_log	
	sjmp Forever
	
do_serial:
	clr RI
	mov b, SBUF
	
	mov a, b
	clr c
	subb a, #'c'
	jz Clear_log

	mov a, b
	clr c
	subb a, #'C'
	jz Clear_log

	mov a, b
	clr c
	subb a, #'s'
	jz Do_send_log

	mov a, b
	clr c
	subb a, #'S'
	jz Do_send_log

	sjmp Forever

END
