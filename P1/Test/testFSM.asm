$MODDE2

org 0000H
   ljmp testProgram

dseg at 30h
temp: 		ds 1
sec: 		ds 1
pwm: 		ds 1
state:		ds 1

CSEG
testProgram:
    MOV SP, 	#7FH
    mov LEDRA, 	#0
    mov LEDRB, 	#0
    mov LEDRC, 	#0
    mov LEDG, 	#0
	mov state, 	#0
	mov pwm, 	#0
	mov sec,	#0
	mov temp, 	#0
	
forever:

	mov a, state
	jnb KEY.1, callreset
	jb KEY.1, State0
;------------------IDLE-------------------;
State0: 
	cjne a, #0, State1
	mov LEDRA,#0
	cpl LEDRA.0
	mov pwm, #0 ;set power to 0%
	jb KEY.3, state0_done
	jnb KEY.3, $ ; wait for key release
	mov state, #1	
State0_done:

	ljmp forever

;--------------RAMP TO SOAK---------------;	
state1:
	cjne a, #1, state2
	mov LEDRA,#0
	cpl LEDRA.1
	mov pwm, #100 ;set power lvl to 100%
	mov sec, #0
	mov a, #150
	clr c
	subb a, temp 
	jnc state1_done
	mov state, #2 ;when temperature gets to 150 celcius state changes to next state
state1_done:
	ljmp forever
	
;--------------PREHEAT/SOAK--------------;
state2:	
	cjne a, #2, state3
	mov LEDRA,#0
	cpl LEDRA.2
	mov pwm, #20 ;power lvl : 20%
	mov a, #60 
	clr c
	subb a, sec
	jnc state2_done ; when time hits 60 secs it goes to state 3
	mov state, #3

state2_done:
	ljmp forever

callstate0:
	ljmp state0
callreset:
	ljmp reset
;---------------RAMP TO PEAK-------------;	
state3:
	cjne a, #3, state4
	mov LEDRA,#0
	cpl LEDRA.3
	mov pwm, #100 ; POWER LVL OVER 100%
	mov a, #220
	clr c
	subb a, temp ;When temp gets to 220 it goes to next state (REFLOW STATE)
	jnc state3_done
	mov state, #4

state3_done:
	ljmp forever
	
;----------------REFLOW------------------;
state4:
	cjne a, #4, state5
	mov LEDRA,#0
	cpl LEDRA.4
	mov pwm, #20 ;power 20%
	mov a, #45
	clr c
	subb a, sec
	jnc state4_done ;jumps to next state in 45 seconds
	mov state, #5

state4_done:
	ljmp forever
	
;----------------COOLING-----------------;
state5:	
	cjne a, #5, state6
	mov LEDRA,#0
	cpl LEDRA.5
	mov pwm, #0 ;power 0%
	mov a, #60
	clr c
	subb a, temp
	jnc state5_done ;jump to next state when temp is 60 celcius
	mov state, #6

state5_done:
	ljmp forever

;----------------COOL ENOUGH--------------;
state6:
	cjne a, #6, callstate0
	mov LEDRA,#0
	cpl LEDRA.6
;;	lcall COOL_ENOUGH_BUZZERRRRRRR ;implement buzzer with ISR.asm;
	mov pwm, #0 ;power 0%
	mov a, #10
	clr c
	subb a, sec
	jnc state6_done ;wait 10 seconds and then go to next state
	mov state, #0
state6_done:
	ljmp forever
reset: ;resets the state to 0
	mov LEDRA ,#0
	cpl LEDRA.7
	mov state, #0
	ljmp forever

END
