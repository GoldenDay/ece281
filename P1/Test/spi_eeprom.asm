$MODDE2
org 0000H
   ljmp MyProgram

MISO   EQU  P0.0 
MOSI   EQU  P0.1 
SCLK   EQU  P0.2
CE_ADC EQU  P0.3
CE_EE  EQU  P0.4
CE_RTC EQU  P0.5 

; Microchip 25LC320 Slave EEPROM Parameters and Instruction Set
EEPROM_CMD_WRSR   EQU  01H 
EEPROM_CMD_WRITE  EQU  02H 
EEPROM_CMD_READ   EQU  03H 
EEPROM_CMD_WRDI   EQU  04H 
EEPROM_CMD_RDSR   EQU  05H
EEPROM_CMD_WREN   EQU  06H 

INIT_SPI:
    orl P0MOD, #00000110b ; Set SCLK, MOSI as outputs
    anl P0MOD, #11111110b ; Set MISO as input
    clr SCLK              ; For mode (0,0) SCLK is zero
	ret
	
DO_SPI_G:
	push acc
    mov R1, #0            ; Received byte stored in R1
    mov R2, #8            ; Loop counter (8-bits)
DO_SPI_G_LOOP:
    mov a, R0             ; Byte to write is in R0
    rlc a                 ; Carry flag has bit to write
    mov R0, a
    mov MOSI, c
    setb SCLK             ; Transmit
    mov c, MISO           ; Read received bit
    mov a, R1             ; Save received bit in R1
    rlc a
    mov R1, a
    clr SCLK
    djnz R2, DO_SPI_G_LOOP
    pop acc
    ret

Delay:
	mov R3, #20
Delay_loop:
	djnz R3, Delay_loop
	ret
	
EEPROM_Write: ; DPTR: address, Acc: value to write
	; Step 1: Set the Write Enable Latch to 1
	clr CE_EE ; Activate EEPROM
	mov R0, #EEPROM_CMD_WREN
	lcall DO_SPI_G
	setb CE_EE
	lcall Delay
	; Step 2: Send the WRITE command
	clr CE_EE
	mov R0, #EEPROM_CMD_WRITE
	lcall DO_SPI_G
	; Step 3: Send the EEPROM destination address (MSB first)
	mov R0, DPH
	lcall DO_SPI_G
	mov R0, DPL
	lcall DO_SPI_G
	; Step 4: Send the byte to write
	mov R0, A
	lcall DO_SPI_G
	setb CE_EE ; Deactivate EEPROM after write
	; Step 5: Poll on the Write In Progress (WIP) bit in the Status Register
EEPROM_Write_Loop:
	lcall Delay
	clr CE_EE
	mov R0, #EEPROM_CMD_RDSR ; Send the Read Status Register command
	lcall DO_SPI_G
	mov R0, #0
	lcall DO_SPI_G ; Dummy write to output serial clock
	setb CE_EE     ; Deactivate EEPROM after read
	mov a, R1
	anl a, #0x01
	jnz EEPROM_Write_Loop
	ret

EEPROM_Read: ; DPTR: Address, Acc: Value read

	; Step 1: Send the READ command
	clr CE_EE ; Activate EEPROM
	mov R0, #EEPROM_CMD_READ
	lcall DO_SPI_G
	
	; Step2: Send the EEPROM destination address (MSB first)
	mov R0, DPH
	lcall DO_SPI_G
	mov R0, DPL
	lcall DO_SPI_G
	
	; Step3: Read the value returned
	mov R0, #0 ; Dummy write to output serial clock
	lcall DO_SPI_G
	setb CE_EE ; Deactivate EEPROM
	lcall Delay
	
	mov a, R1
	ret

MyProgram:
	mov sp, #07FH
	clr a
	mov LEDG,  a
	mov LEDRA, a
	mov LEDRB, a
	mov LEDRC, a
	orl P0MOD, #00111000b ; make all CEs outputs

	setb CE_ADC
	setb CE_EE
	clr  CE_RTC ; RTC CE is active high

	lcall INIT_SPI
	
	mov dptr, #007BH
	mov a, #055H
	lcall EEPROM_Write; Test: write 55H at address 007BH
	lcall EEPROM_Read; Read back and compare
	
	cjne a, #055H, Did_not_work
	setb LEDG.7 ; It worked!
	sjmp Forever

Did_not_work:	
	setb LEDRA.0
	
Forever:
	sjmp Forever
	
END
