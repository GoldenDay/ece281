$MODDE2
org 0000H
   ljmp MyProgram
   
DSEG at 30H
x:   ds 2
y:   ds 2
z:	 ds 2
bcd: ds 3


BSEG
mf: dbit 1

SCLK EQU P0.2 
MOSI EQU P0.1 
MISO EQU P0.0 

FREQ   EQU 33333333
BAUD   EQU 115200
T2LOAD EQU 65536-(FREQ/(32*BAUD))
SS EQU P0.3

CSEG
myLUT:
    DB 0C0H, 0F9H, 0A4H, 0B0H, 099H        ; 0 TO 4
    DB 092H, 082H, 0F8H, 080H, 090H        ; 4 TO 9

myLUT2:
	DB  '\n'
	DB	'\r'

myLUT3:
	DB '0','1','2','3','4','5','6','7','8','9'

Load_y MAC
	mov y+0, #low (%0) 
	mov y+1, #high(%0) 
ENDMAC
Display:
	mov dptr, #myLUT
	; Display Digit 0
    mov A, bcd+0
    anl a, #0fh
    movc A, @A+dptr
    mov HEX0, A
	; Display Digit 1
    mov A, bcd+0
    swap a
    anl a, #0fh
    movc A, @A+dptr
    mov HEX1, A
    ret
    
WaitOneSec: ; waits half second
	mov R2, #180
L3:	mov R1, #250
L2:	mov R0, #250
L1:	djnz R0, L1
	djnz R1, L2
	djnz R2, L3
	ret

Delay:
	mov R3, #20
Delay_loop:
	djnz R3, Delay_loop
	ret	
; Configure the serial port and baud rate using timer 2
InitSerialPort:
	clr TR2 ; Disable timer 2
	mov T2CON, #30H ; RCLK=1, TCLK=1 
	mov RCAP2H, #high(T2LOAD)  
	mov RCAP2L, #low(T2LOAD)
	setb TR2 ; Enable timer 2
	mov SCON, #52H
	ret

; Send a character through the serial port
putchar:
    JNB TI, putchar
    CLR TI
    MOV SBUF, a
    RET

; Send a constant-zero-terminated string through the serial port
SendString:
    CLR A
    MOVC A, @A+DPTR
    JZ SSDone
    LCALL putchar
    INC DPTR
    SJMP SendString
SSDone:
    ret
send_number:
	lcall hex2bcd
	mov dptr, #myLUT3
	mov A, bcd+2
    swap a
	anl a, #0fh
	;orl a, #30h ; Convert to ASCII
	movc A, @A+dptr
	lcall putchar

	mov A, bcd+2
	anl a, #0fh
;	orl a, #30h ; Convert to ASCII
	movc A, @A+dptr
	lcall putchar	
	
	mov A, bcd+1
	swap a
	anl a, #0fh
	;orl a, #30h ; Convert to ASCII
	movc A, @A+dptr
	lcall putchar

	mov A, bcd+1
	anl a, #0fh
;	orl a, #30h ; Convert to ASCII
	movc A, @A+dptr
	lcall putchar
	
	mov A, bcd
	swap a
	anl a, #0fh
;	orl a, #30h ; Convert to ASCII
	movc A, @A+dptr
	lcall putchar

    mov A, bcd 
    anl A, #0FH 
    movc A, @A+dptr
	lcall putchar 
	
	mov dptr, #myLUT2
	mov a, #0
	movc a, @a+dptr
	lcall putchar
	mov a, #1
	movc a, @a+dptr
	lcall putchar
	ret
Convert:
	    mov b, #0  ; Read channel 0
	lcall Read_ADC_Channel
	lcall InitSerialPort
    ; The temperature can be calculated as (ADC*500/1024)-273 (may overflow 16 bit operations)
	; or (ADC*250/512)-273 (may overflow 16 bit operations)
	; or (ADC*125/256)-273 (may overflow 16 bit operations)
	; or (ADC*62/256)+(ADC*63/256)-273 (Does not overflow 16 bit operations!)
	
    mov r0, #0
    
    mov x+1, r7
    mov x+0, r6
    
    Load_y(62)
	lcall mul16
	mov R4, x+1

	mov x+1, R7
	mov x+0, R6

	Load_y(63)
	lcall mul16
	mov R5, x+1
	
	mov x+0, R4
	mov x+1, #0
	mov y+0, R5
	mov y+1, #0
	lcall add16
	
	Load_y(273)
	lcall sub16
	
	mov z+0, x+0
	mov z+1, x+1
	ret
MyProgram:
    MOV SP, #7FH
    mov LEDRA, #0
    mov LEDRB, #0
    mov LEDRC, #0
    mov LEDG, #0
    orl P0MOD, #00001000b
    clr mf
    setb SS
	lcall INIT_SPI

Forever: 
	lcall Convert
	clr a
	mov x+2, a
	mov x+3, a
	clr SS
	mov R0, #00000001B ; Start bit:1
	lcall DO_SPI_G
	mov R0, #10000000B ; Single ended, read channel 0
	lcall DO_SPI_G
	mov a, R1 ; R1 contains bits 8 and 9
	anl a, #03H
	mov LEDRB, a
	mov x+1, a
	mov R0, #05H ; It doesn't matter what we transmit...
	lcall DO_SPI_G
	mov LEDRA, R1 ; R1 contains bits 0 to 7
	mov x+0, R1
	mov y, #10
	lcall div16
	mov y, #55
	lcall sub16
	lcall check_neg
	setb SS
	lcall send_number
	lcall Display
	lcall WaitOneSec
	lcall Delay
    SJMP Forever
check_neg:
	mov y, #100
	lcall x_gt_y
	jb mf, neg_loop
	mov HEX2, #11111111B
	ret
neg_loop:
	clr a
	mov x, a
	mov HEX2, #10111111B
	clr mf
	ret
	
Read_ADC_Channel:
	clr SS
	mov R0, #00000001B ; Start bit:1
	lcall DO_SPI_G
	
	mov a, b
	swap a
	anl a, #0F0H
	setb acc.7 ; Single mode (bit 7).
	
	mov R0, a ;  Select channel
	lcall DO_SPI_G
	mov a, R1          ; R1 contains bits 8 and 9
	anl a, #03H
	mov R7, a
	
	mov R0, #55H ; It doesn't matter what we transmit...
	lcall DO_SPI_G
	mov a, R1    ; R1 contains bits 0 to 7
	mov R6, a
	setb SS
	ret


INIT_SPI:
	orl P0MOD, #00001110b ; Set SCLK, MOSI as outputs
	anl P0MOD, #11111110b ; Set MISO as input
	clr SCLK ; For mode (0,0) SCLK is zero
	ret
DO_SPI_G:
	push acc
	mov R1, #0 ; Received byte stored in R1
	mov R2, #8 ; Loop counter (8-bits)
DO_SPI_G_LOOP:
	mov a, R0 ; Byte to write is in R0
	rlc a ; Carry flag has bit to write
	mov R0, a
	mov MOSI, c
	setb SCLK ; Transmit
	mov c, MISO ; Read received bit
	mov a, R1 ; Save received bit in R1
	rlc a
	mov R1, a
	clr SCLK
	djnz R2, DO_SPI_G_LOOP
	pop acc
	ret

;----------------------------------------------------
; Converts the 16-bit hex number in 'x' to a 
; 5-digit packed BCD in 'bcd' using the
; double-dabble algorithm.
;---------------------------------------------------
hex2bcd:
	push acc
	push psw
	push AR0
	
	clr a
	mov bcd+0, a ; Initialize BCD to 00-00-00 
	mov bcd+1, a
	mov bcd+2, a
	mov r0, #16  ; Loop counter.

hex2bcd_L0:
	; Shift binary left	
	mov a, x+1
	mov c, acc.7 ; This way x remains unchanged!
	mov a, x+0
	rlc a
	mov x+0, a
	mov a, x+1
	rlc a
	mov x+1, a
    
	; Perform bcd + bcd + carry using BCD arithmetic
	mov a, bcd+0
	addc a, bcd+0
	da a
	mov bcd+0, a
	mov a, bcd+1
	addc a, bcd+1
	da a
	mov bcd+1, a
	mov a, bcd+2
	addc a, bcd+2
	da a
	mov bcd+2, a

	djnz r0, hex2bcd_L0

	pop AR0
	pop psw
	pop acc
	ret
	
;------------------------------------------------
; bcd2hex:
; Converts the 5-digit packed BCD in 'bcd' to a 
; 16-bit hex number in 'x'
;------------------------------------------------

rrc_and_correct:
    rrc a
    push psw  ; Save carry (it is changed by the add instruction)
    jnb acc.7, nocor1
    add a, #(100H-30H) ; subtract 3 from packed BCD MSD
nocor1:
    jnb acc.3, nocor2
    add a, #(100H-03H) ; subtract 3 from packed BCD LSD
nocor2:
    pop psw   ; Restore carry
    ret

bcd2hex:
	push acc
	push psw
	push AR0
	push bcd+0
	push bcd+1
	push bcd+2
	mov R0, #16 ;Loop counter.
	
bcd2hex_L0:
    ; Divide BCD by two
    clr c
    mov a, bcd+2
    lcall rrc_and_correct
    mov bcd+2, a
    mov a, bcd+1
    lcall rrc_and_correct
    mov bcd+1, a
    mov a, bcd+0
    lcall rrc_and_correct
    mov bcd+0, a
    ;Shift R0-R1 right through carry
    mov a, x+1
    rrc a
    mov x+1, a
    mov a, x+0
    rrc a
    mov x+0, a
    djnz R0, bcd2hex_L0
    
    pop bcd+2
    pop bcd+1
    pop bcd+0
	pop AR0
	pop psw
	pop acc
	ret
;------------------------------------------------
; x = x - y
;------------------------------------------------
sub16:
	push acc
	push psw
	clr c
	mov a, x+0
	subb a, y+0
	mov x+0, a
	mov a, x+1
	subb a, y+1
	mov x+1, a
	pop psw
	pop acc
	ret

;------------------------------------------------
; x = x + y
;------------------------------------------------
add16:
	push acc
	push psw
	mov a, x+0
	add a, y+0
	mov x+0, a
	mov a, x+1
	addc a, y+1
	mov x+1, a
	pop psw
	pop acc
	ret
;------------------------------------------------
; x = x * y
;------------------------------------------------
mul16:
	push acc
	push b
	push psw
	push AR0
	push AR1
		
	; R0 = x+0 * y+0
	; R1 = x+1 * y+0 + x+0 * y+1
	
	; Byte 0
	mov	a,x+0
	mov	b,y+0
	mul	ab		; x+0 * y+0
	mov	R0,a
	mov	R1,b
	
	; Byte 1
	mov	a,x+1
	mov	b,y+0
	mul	ab		; x+1 * y+0
	add	a,R1
	mov	R1,a
	clr	a
	addc a,b
	mov	R2,a
	
	mov	a,x+0
	mov	b,y+1
	mul	ab		; x+0 * y+1
	add	a,R1
	mov	R1,a
	
	mov	x+1,R1
	mov	x+0,R0

	pop AR1
	pop AR0
	pop psw
	pop b
	pop acc
	
	ret
;------------------------------------------------
; x = x / y
; This subroutine uses the 'paper-and-pencil' 
; method described in page 139 of 'Using the
; MCS-51 microcontroller' by Han-Way Huang.
;------------------------------------------------
div16:
	push acc
	push psw
	push AR0
	push AR1
	push AR2
	
	mov	R2,#16 ; Loop counter
	clr	a
	mov	R0,a
	mov	R1,a
	
div16_loop:
	; Shift the 32-bit of [R1, R0, x+1, x+0] left:
	clr c
	; First shift x:
	mov	a,x+0
	rlc a
	mov	x+0,a
	mov	a,x+1
	rlc	a
	mov	x+1,a
	; Then shift [R1,R0]:
	mov	a,R0
	rlc	a 
	mov	R0,a
	mov	a,R1
	rlc	a
	mov	R1,a
	
	; [R1, R0] - y
	clr c	     
	mov	a,R0
	subb a,y+0
	mov	a,R1
	subb a,y+1
	
	jc	div16_minus		; temp >= y?
	
	; -> yes;  [R1, R0] -= y;
	; clr c ; carry is always zero here because of the jc above!
	mov	a,R0
	subb a,y+0 
	mov	R0,a
	mov	a,R1
	subb a,y+1
	mov	R1,a
	
	; Set the least significant bit of x to 1
	orl	x+0,#1
	
div16_minus:
	djnz R2, div16_loop	; -> no
	
div16_exit:

	pop AR2
	pop AR1
	pop AR0
	pop psw
	pop acc
	
	ret

;------------------------------------------------
; mf=1 if x < y
;------------------------------------------------
x_lt_y:
	push acc
	push psw
	clr c
	mov a, z+0
	subb a, y+0
	mov a, z+1
	subb a, y+1
	mov mf, c
	pop psw
	pop acc
	ret

;------------------------------------------------
; mf=1 if x > y
;------------------------------------------------
x_gt_y:
	push acc
	push psw
	clr c
	mov a, y+0
	subb a, x+0
	mov a, y+1
	subb a, x+1
	mov mf, c
	pop psw
	pop acc
	ret

;------------------------------------------------
; mf=1 if x = y
;------------------------------------------------
x_eq_y:
	push acc
	push psw
	clr mf
	clr c
	mov a, y+0
	subb a, x+0
	jnz x_eq_y_done
	mov a, y+1
	subb a, x+1
	jnz x_eq_y_done
	setb mf
x_eq_y_done:
	pop psw
	pop acc
	ret

;------------------------------------------------
; mf=1 if x >= y
;------------------------------------------------
x_gteq_y:
	lcall x_eq_y
	jb mf, x_gteq_y_done
	ljmp x_gt_y
x_gteq_y_done:
	ret

;------------------------------------------------
; mf=1 if x <= y
;------------------------------------------------
x_lteq_y:
	lcall x_eq_y
	jb mf, x_lteq_y_done
	ljmp x_lt_y
x_lteq_y_done:
	ret	
xchg_xy:
	mov a, x+0
	xch a, y+0
	mov x+0, a
	mov a, x+1
	xch a, y+1
	mov x+1, a
	ret
END