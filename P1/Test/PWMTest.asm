$MODDE2

;TEST

org 0000H
	ljmp myprogram
	
CSEG

myprogram:
	mov SP, #7FH
	mov LEDRA,#0
	mov LEDRB,#0
	mov LEDRC,#0
	mov LEDG,#0
	mov P0MOD, #00000011B ; P0.0 outputs
	clr P0.0
	
M0:
	setb LEDRA.0
	clr P0.0
	lcall WaitHalfSec
	lcall WaitHalfSec	
	lcall WaitHalfSec
	lcall WaitHalfSec
	setb P0.0
	clr LEDRA.0
	lcall WaitHalfSec
	lcall WaitHalfSec
	lcall WaitHalfSec	
	lcall WaitHalfSec
	lcall WaitHalfSec
	lcall WaitHalfSec
	lcall WaitHalfSec	
	lcall WaitHalfSec
	lcall WaitHalfSec
	lcall WaitHalfSec
	lcall WaitHalfSec	
	lcall WaitHalfSec
	lcall WaitHalfSec
	sjmp M0



;For a 33.33MHz clock, one cycle takes 30ns
WaitHalfSec:
	mov R2, #90
L3: mov R1, #250
L2: mov R0, #250
L1: djnz R0, L1
	djnz R1, L2
	djnz R2, L3
	ret
	
END