$MODDE2
org 0000H
   ljmp MyProgram

MISO   EQU  P0.0 
MOSI   EQU  P0.1 
SCLK   EQU  P0.2
CE_ADC EQU  P0.3
CE_EE  EQU  P0.4
CE_RTC EQU  P0.5 

DSEG at 30H
hours:    ds 1
minutes:  ds 1
seconds:  ds 1
day:      ds 1
month:    ds 1
year:     ds 1


CSEG
; Look-up table for 7-seg displays
myLUT:
    DB 0C0H, 0F9H, 0A4H, 0B0H, 099H        ; 0 TO 4
    DB 092H, 082H, 0F8H, 080H, 090H        ; 4 TO 9

INIT_SPI:
    orl P0MOD, #00000110b ; Set SCLK, MOSI as outputs
    anl P0MOD, #11111110b ; Set MISO as input
    clr SCLK              ; For mode (0,0) SCLK is zero
	ret
	
DO_SPI_G:
	push acc
    mov R1, #0            ; Received byte stored in R1
    mov R2, #8            ; Loop counter (8-bits)
DO_SPI_G_LOOP:
    mov a, R0             ; Byte to write is in R0
    rlc a                 ; Carry flag has bit to write
    mov R0, a
    mov MOSI, c
    setb SCLK             ; Transmit
    nop                   ; Output form RTC needs a bit of extra time!
    nop
    nop
    mov c, MISO           ; Read received bit
    mov a, R1             ; Save received bit in R1
    rlc a
    mov R1, a
    clr SCLK
    djnz R2, DO_SPI_G_LOOP
    pop acc
    ret

; dpl has the address, b has the data
RTC_Write:
	setb CE_RTC
	mov	a,#0x80
	orl	a, dpl
	mov	R0, a
	lcall DO_SPI_G
	mov	R0, b
	lcall DO_SPI_G
	clr	CE_RTC
	ret

; dpl has the address, value returned in accumulator
RTC_Read:
	setb CE_RTC
	mov	a,#0x7F
	anl	a, dpl
	mov	R0,a
	lcall DO_SPI_G
	mov	R0,#0x00 ; Any value will do
	lcall DO_SPI_G
	clr	CE_RTC
	mov	a, R1
	ret

DS1306_Write mac
	mov dpl, #%0
	mov b, #%1
	lcall RTC_Write
endmac

Set_time mac
	mov dpl, #82H
	mov b, #%0
	lcall RTC_Write
	mov dpl, #81H
	mov b, #%1
	lcall RTC_Write
	mov dpl, #80H
	mov b, #%2
	lcall RTC_Write
endmac

Set_date mac
	mov dpl, #86H
	mov b, #%0
	lcall RTC_Write
	mov dpl, #85H
	mov b, #%1
	lcall RTC_Write
	mov dpl, #84H
	mov b, #%2
	lcall RTC_Write
endmac

display_time:
	mov dpl, #02H
	lcall RTC_Read
	mov hours, a
	mov dpl, #01H
	lcall RTC_Read
	mov minutes, a
	mov dpl, #00H
	lcall RTC_Read
	mov seconds, a
	
	mov dptr, #myLUT

	mov HEX0, #0ffh
	mov HEX1, #0ffh
	mov a, seconds
	anl a, #0fh
	movc a, @a+dptr
	mov HEX2, a
	mov a, seconds
	swap a
	anl a, #0fh
	movc a, @a+dptr
	mov HEX3, a
	
	mov a, minutes
	anl a, #0fh
	movc a, @a+dptr
	mov HEX4, a
	mov a, minutes
	swap a
	anl a, #0fh
	movc a, @a+dptr
	mov HEX5, a

	mov a, hours
	anl a, #0fh
	movc a, @a+dptr
	mov HEX6, a
	mov a, hours
	swap a
	anl a, #0fh
	movc a, @a+dptr
	mov HEX7, a

	ret

display_date:
	mov dpl, #06H
	lcall RTC_Read
	mov year, a
	mov dpl, #05H
	lcall RTC_Read
	mov month, a
	mov dpl, #04H
	lcall RTC_Read
	mov day, a
	
	mov dptr, #myLUT

	mov a, year
	anl a, #0fh
	movc a, @a+dptr
	mov HEX0, a
	mov a, year
	swap a
	anl a, #0fh
	movc a, @a+dptr
	mov HEX1, a
	mov HEX2, #0C0H
	mov HEX3, #0A4H
	
	mov a, month
	anl a, #0fh
	movc a, @a+dptr
	mov HEX4, a
	mov a, month
	swap a
	anl a, #0fh
	movc a, @a+dptr
	mov HEX5, a

	mov a, day
	anl a, #0fh
	movc a, @a+dptr
	mov HEX6, a
	mov a, day
	swap a
	anl a, #0fh
	movc a, @a+dptr
	mov HEX7, a

	ret

MyProgram:
	mov sp, #07FH
	clr a
	mov LEDG,  a
	mov LEDRA, a
	mov LEDRB, a
	mov LEDRC, a
	orl P0MOD, #00111000b ; make all CEs outputs

	setb CE_ADC
	setb CE_EE
	clr  CE_RTC ; RTC CE is active high

	lcall INIT_SPI

	DS1306_Write(0x8f, 0x04); Enable writing to the DS1306.  Also, enable 1 Hz output.
	DS1306_Write(0x91, 0xa5); Enable trickle charger with one diode and 2k
	
	Set_time(11H,42H,00H)
	Set_date(13H,11H,25H)
	
Forever:
	jb swa.0, do_date
	lcall display_time
	sjmp Forever
do_date:
	lcall display_date
	sjmp Forever
	
END
