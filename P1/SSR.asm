; SSR.asm: Assembly code to operate the oven via SSR box
; Performs PWM by generating a Square Wave out of P0.0 at a duty cycle
; determined by the value in 'pwm'. Px.x high should activate SSR, low turns it off
; NOTE: may need to change the pin if it's in use already


$MODDE2

TIMER2_RELOAD EQU 10000 ; frequency isn't important for PWM... (I think)

org 0000H
	ljmp myprogram
	
org 002BH
	ljmp ISR_timer2
	
DSEG at 30H
pwm: ds 1
pulseCount: ds 1

CSEG

Load_X MAC
	mov x+0, #low (%0 % 0x10000) 
	mov x+1, #high(%0 % 0x10000) 
	mov x+2, #low (%0 / 0x10000) 
	mov x+3, #high(%0 / 0x10000) 
ENDMAC	

; =============================================
; The magic happens here:
; Px.x HIGH turns the oven on. See block diagram
; =============================================
ISR_timer2:
	;sets P0.0 HIGH/LOW at different intervals for PWM.
	;dutyCycle = HighTime/(HighTime+LowTime) * 100%
	push acc
	mov a, #100
	cjne a, pwm, not100

_100:
	setb P0.0	;keep P0.0 high for 100% duty cycle
	mov pulseCount, #0 ;reset pulseCount if pwm changed before 20% cycle finished
	sjmp retFromISR
	
not100:	
	mov a, #0
	cjne a, pwm, _20
	
_0:
	clr P0.0	;keep P0.0 low for 0% duty cycle
	mov pulseCount, #0
	sjmp retFromISR
	
	
_20: 		
	;20% = (1 HIGH)/(4 LOW + 1 HIGH). High, Low, Low, Low, Low, High, ... 
	mov a, pulseCount
	cjne a, #0, setLow ;first pusle should be High, 2nd to 5th Low
setHigh:
	setb P0.0
	inc pulseCount
	sjmp retFromISR
setLow:
	clr P0.0		
	inc pulseCount
	mov a, pulseCount
	cjne a, #5, retFromISR
resetCycle:
	mov pulseCount, #0	;End of 1 period. Reset the cycle
	
	
retFromISR:
	pop acc
	reti
	
myprogram:
	mov SP, #7FH ;set stack pointer, clear LEDs
	mov LEDRA,#0
	mov LEDRB,#0
	mov LEDRC,#0
	mov LEDG,#0
	
	mov P0MOD, #00000011B ; P0.0 outputs
	clr P0.0 ;P0.0 low should keep oven off
	
	;TEST:
	;SET PWM VALUE HERE
	mov pwm, #20
	
	
	mov pulseCount, #0 ;for counting number of pulses. Needed in _20% for PWM

    mov T2CON, #00H ; Autoreload is enabled, work as a timer
    clr TR2
    clr TF2
    ; Set up timer 2 reload
    mov RCAP2H,#high(TIMER2_RELOAD)
    mov RCAP2L,#low(TIMER2_RELOAD)
    setb TR2
    setb ET2
     
    setb EA  ; Enable all interrupts


;=====================================================
;Wanted to set differenty duty cycles with buttons but it doesn't work

M0:
	jb KEY.3, M1
	jnb KEY.3, $
	mov pwm, #100	;set the keys to change duty cycle

M1:
	jb KEY.2, M2
	jnb KEY.2, $
	mov pwm, #20
M2:
	jb KEY.1, M3
	jnb KEY.1, $
	mov pwm, #0

M3:  
	ljmp M0
;======================================================

;========================
;      SUBROUTINES
;========================

;For a 33.33MHz clock, one cycle takes 30ns
WaitHalfSec:
	mov R2, #90
L3: mov R1, #250
L2: mov R0, #250
L1: djnz R0, L1
	djnz R1, L2
	djnz R2, L3
	ret

END