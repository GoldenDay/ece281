; Reflow Oven: Group D1

; Manolito Pimentel
; Stephen Hu
; Adam Wong
; Connie Ma
; Kaibo Ma
; Daniel Kim

$MODDE2

org 0000H
   ljmp MyProgram

org 000BH
	ljmp ISR_timer0
	
org 001BH
	ljmp ISR_timer1
	

   
dseg at 30h
x:    			ds 2
y:    			ds 2
z:				ds 2
bcd:  			ds 3
bcd1:			ds 3
bcd2:			ds 3
bcd3:			ds 3
op:				ds 1
temp: 			ds 2
sec: 			ds 1
pwm: 			ds 1
count10ms:		ds 1
seconds:   		ds 1
minutes:   		ds 1
hours:     		ds 1
ap:				ds 1
hourset:		ds 1
minuteset:		ds 1
secondset:		ds 1
apset:			ds 1
ring:			ds 1
count:			ds 1
toring:			ds 1
state:			ds 1
start:			ds 1
pulseCount:		ds 1
rampsoaktemp:	ds 3
soakseconds:	ds 1
soakminutes:	ds 1
peaktemp:		ds 3
reflowseconds:	ds 1
reflowminutes:	ds 1 ; get rid of this
coolingtemp:	ds 3
hotTemp:  ds 2
coldTemp: ds 2
totalTemp:ds 2
inStateSec: ds 1
inStateMin: ds 1
inState10msCnt: ds 1



bseg
mf:			dbit 1		



$include(math16mason.asm)
$include(functions.asm)

CSEG

MISO   			EQU  	P0.0 
MOSI   			EQU  	P0.1 
SCLK   			EQU  	P0.2
CE_ADC 			EQU  	P0.3
CE_EE  			EQU  	P0.4
CE_RTC 			EQU  	P0.5
CLK 			EQU 	33333333
FREQ_0 			EQU 	50
FREQ_1 			EQU 	500
TIMER0_RELOAD 	EQU 	65536-(CLK/(12*2*FREQ_0))
TIMER1_RELOAD 	EQU 	65536-(CLK/(12*FREQ_1))
TIMER2_RELOAD	EQU		10000 ; frequency isn't important for PWM... (I think)
FREQ 			EQU 	33333333
BAUD   			EQU 	115200
T2LOAD 			EQU 	65536-(FREQ/(32*BAUD))

myLUT:
    DB 0C0H, 0F9H, 0A4H, 0B0H, 099H        ; 0 TO 4
    DB 092H, 082H, 0F8H, 080H, 090H        ; 4 TO 9
    DB 0FFH	; all segments off
   
myLUT1:
	DB '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' 

myLUT2:
	DB '\n'

myLUT3:
	DB '\r'

asciiLookup:
	DB '0'
	DB '1'
	DB '2'
	DB '3'
	DB '4'
	DB '5'
	DB '6'
	DB '7'
	DB '8'
	DB '9'
	
Display:
	mov dptr, #myLUT
	; Display Digit 0
    mov A, bcd+0
    anl a, #0fh
    movc A, @A+dptr

	; Display Digit 1
    mov A, bcd+0
    swap a
    anl a, #0fh
    movc A, @A+dptr

	; Display Digit 2
    mov A, bcd+1
    anl a, #0fh
    movc A, @A+dptr

    ret

; DE2 SETUP____________________________________________________________________________________
    
MyProgram:
    MOV SP, 	#7FH
    mov LEDRA, 	#0
    mov LEDRB, 	#0
    mov LEDRC, 	#0
    mov LEDG, 	#0
    mov state, 	#0
	mov pwm, 	#0
	mov sec,	#0
	mov temp, 	#0
		
; PIN ASSIGNMENTS:
		;P0.0 = MISO [INPUT]	
		;P0.1 = MOSI [OUTPUT]
		;P0.2 = SCLK [OUTPUT]
		;P0.3 = CE_ADC [OUTPUT]
		;P0.4 = CE_EE [OUTPUT]
		;P0.5 = CE_RTC [OUTPUT]
		;P0.6 = PWM [OUTPUT]
		
		;P1.0 = Buzzer
		;P1.1 = Start button		
    
    mov P0MOD, 	#01111110B		 
    mov P1MOD, 	#00000001B 		
    
    setb CE_ADC
    setb CE_EE
    clr CE_RTC  

    mov a, #0
    mov toring, a
    mov ring, a
    
	clr P0.6 			;P0.6 low should keep oven off 
	mov pwm, #0		 	;0% power at start
	mov pulseCount, #0 	;for counting number of pulses. Needed in _20% for PWM

	
	lcall Init_Timer0
	lcall Init_Timer1 
	
	
	
	clr SCLK              ; For mode (0,0) SCLK is zero
    
    clr TR2 ; Disable timer 2
	mov T2CON, #30H ; RCLK=1, TCLK=1 
	mov RCAP2H, #high(T2LOAD)  
	mov RCAP2L, #low(T2LOAD)
	setb TR2 ; Enable timer 2
	mov SCON, #52H
	
	
	
	setb EA  ; Enable all interrupts 
;	cpl LEDG.0	
;	lcall SendString
;	cpl LEDG.1
;	mov temp, #25
;	mov x, temp
	lcall hex2bcd
	lcall Display
	
	mov rampsoaktemp, #150
	mov x, rampsoaktemp
	lcall hex2bcd1
	lcall Display
	
	mov peaktemp, #220
	mov x, peaktemp
	lcall hex2bcd2
	lcall Display
	
	mov coolingtemp, #60
	mov x, coolingtemp
	lcall hex2bcd3
	lcall Display
	
	mov start, #0
	
	mov seconds, #0
	mov minutes, #0
	
	mov soakseconds, #0
	mov soakminutes, #1H
	
	mov reflowseconds, #45H
	mov reflowminutes, #0
	
	
	;lcd setup________________________________________________________
	
	setb LCD_ON
    clr LCD_EN  ; Default state of enable must be zero
    lcall Wait40us
    
    mov LCD_MOD, #0xff ; Use LCD_DATA as output port
    clr LCD_RW ;  Only writing to the LCD in this code.
    
    mov a, #0ch ; Display on command
	lcall LCD_command
	mov a, #38H ; 8-bits interface, 2 lines, 5x7 characters
	lcall LCD_command
	mov a, #01H ; Clear screen (Warning, very slow command!)
	lcall LCD_command
	
	lcall Clr_loop

	; Move to first column of first row	
	mov a, #80H
	lcall LCD_command
	
	
; FOREVER LOOP________________________________________________________________________________

forever:
	
	lcall GETTEMPERATURE
	;lcall Display_temp
	setb LEDG.0
;EXTRAFEATURE LEDS OF AWESOMENESS;
	lcall TempuratureLED

;FSM______________________________________________________________

	mov a, state
	
;------------------IDLE-------------------;
State0: 
	cjne a, #0, State1
	lcall TempuratureLED
	lcall LCD_DISPLAY_IDLE

	jnb SWA.0, continue1 ;ramp to soak temp
	jb SWA.0, ramptosoakJMP
continue1:
	jnb SWA.1, continue2	;soak time
	jb SWA.1, soakJMP
continue2:
	jnb SWA.2, continue3	;ramp to peak temp 
	jb SWA.2, ramptopeakJMP
continue3:
	jnb SWA.3, continue4 ;reflow time
	jb SWA.3, reflowtimeJMP
continue4:
	jnb SWA.4, continue5	;cool temp
	jb SWA.4, cooltempJMP
continue5:
	ljmp continueIdle

	
ramptosoakJMP:
	lcall ramptosoak

soakJMP:
	lcall soak

ramptopeakJMP:
	lcall ramptopeak

reflowtimeJMP:
	lcall reflowtime

cooltempJMP:
	lcall cooltemp

continueIdle:	
;	mov LEDRA,#0
;	cpl LEDRA.0
	mov pwm, #0 ;set power to 0%
	jb KEY.3, state0_done
	jnb KEY.3, $ ; wait for key release
	mov inStateSec, #0
	mov inStateMin, #0
	mov state, #1
	lcall onering	
State0_done:

	ljmp forever

;--------------RAMP TO SOAK---------------;	
state1:
	cjne a, #1, state2
	lcall TempuratureLED
	lcall LCD_DISPLAY_RAMP_TO_SOAK
	inc start
	
	jb KEY.2, noreset
	mov state, #0
	mov seconds, #0
	mov minutes, #0
	mov start, #0
	mov inStateSec, #0
	mov inStateMin, #0
	lcall tworing
noreset:

	jnb SWA.1 , test1
	mov state, #2
	lcall onering
test1:
	
;	mov LEDRA,#0
;	cpl LEDRA.1
	mov seconds, #0
	mov minutes, #0
	mov pwm, #100 ;set power lvl to 100%
	mov a, rampsoaktemp
	clr c
	subb a, temp 
	jnc state1_done
	mov inStateSec, #0
	mov inStateMin, #0
	mov state, #2 ;when temperature gets to 150 celcius state changes to next state
	
state1_done:
	ljmp forever
	
;--------------PREHEAT/SOAK--------------;
state2:	
	cjne a, #2, state3
	lcall TempuratureLED
	lcall LCD_DISPLAY_SOAK
	
	jb KEY.2, noreset2
	mov state, #0
	mov seconds, #0
	mov minutes, #0
	mov start, #0
	mov inStateSec, #0
	mov inStateMin, #0
	lcall tworing
noreset2:

	jnb SWA.2, test2
	mov state, #3
	lcall onering
test2:
	
;	mov LEDRA,#0
;	cpl LEDRA.2
	mov seconds, #0
	mov minutes, #0
	mov pwm, #20 ;power lvl : 20%
	mov a, soakminutes 
	clr c
	subb a, secf
	jnc state2_done ; when time hits 60 secs it goes to state 3
	mov inStateSec, #0
	mov inStateMin, #0
	mov state, #3

state2_done:
	ljmp forever

;---------------RAMP TO PEAK-------------;	
state3:
	cjne a, #3, state4
	lcall TempuratureLED
	lcall LCD_DISPLAY_RAMP_TO_PEAK
	
	jb KEY.2, noreset3
	mov state, #0
	mov seconds, #0
	mov minutes, #0
	mov start, #0
	mov inStateSec, #0
	mov inStateMin, #0
	lcall tworing
noreset3:

	jnb SWA.3, test3
	mov state, #4
	lcall onering
test3:
	
;	mov LEDRA,#0
;	cpl LEDRA.3
	mov seconds, #0
	mov minutes, #0
	mov pwm, #100 ; POWER LVL OVER 100%
	mov a, peaktemp
	clr c
	subb a, temp ;When temp gets to 220 it goes to next state (REFLOW STATE)
	jnc state3_done
	mov inStateSec, #0
	mov inStateMin, #0
	mov state, #4

state3_done:
	ljmp forever
	
;----------------REFLOW------------------;
state4:
	cjne a, #4, state5
	lcall TempuratureLED
	lcall LCD_DISPLAY_REFLOW
	
	jb KEY.2, noreset4
	mov state, #0
	mov seconds, #0
	mov minutes, #0
	mov inStateSec, #0
	mov inStateMin, #0
	mov start, #0
	lcall tworing
noreset4:

	jnb SWA.4, test4
	mov state, #5
	lcall onering
test4:
	
;	mov LEDRA,#0
;	cpl LEDRA.4

	mov pwm, #20 ;power 20%
	mov a, reflowtime
	clr c
	subb a, seconds
	jnc state4_done ;jumps to next state in 45 seconds
	mov inStateSec, #0
	mov inStateMin, #0
	mov state, #5

state4_done:
	ljmp forever
	
;----------------COOLING-----------------;
state5:	
	cjne a, #5, state6
	lcall TempuratureLED
	lcall LCD_DISPLAY_COOLING
	
	jnb KEY.2, noreset5
	mov state, #0
	mov seconds, #0
	mov minutes, #0
	mov inStateSec, #0
	mov inStateMin, #0
	mov start, #0
	lcall tworing
noreset5:

	jb SWA.5, test5
	mov state, #6
test5:
	
;	mov LEDRA,#0
;	cpl LEDRA.5
	mov pwm, #0 ;power 0%
	mov a, coolingtemp
	clr c
	subb a, temp
	jnc state5_done ;jump to next state when temp is 60 celcius
	mov inStateSec, #0
	mov inStateMin, #0
	lcall sixring
	mov state, #6

state5_done:
	ljmp forever

;----------------COOL ENOUGH--------------;
state6:
	cjne a, #6, state6
	lcall TempuratureLED
	lcall LCD_DISPLAY_CONTENTS_COOLED
	
	jnb KEY.2, noreset6
	mov state, #0
	mov seconds, #0
	mov minutes, #0
	mov inStateSec, #0
	mov inStateMin, #0
	mov start, #0
	lcall tworing
noreset6:
	
;	mov LEDRA,#0
;	cpl LEDRA.6
	mov pwm, #0 ;power 0%
	mov a, #10
	clr c
	
state6_done:
	ljmp forever
	

;END OF FSM_____________________________________________________
	
	

;ISR0: TIME________________________________________________________________________________

ISR_Timer0:
	; Reload the timer
    mov TH0, #high(TIMER0_RELOAD)
    mov TL0, #low(TIMER0_RELOAD)
   

	
    
    ; Save used register into the stack
    push psw
    push acc
    push dph
    push dpl
    

    
    mov a, start
    jz skipIncrement
    
	; increment the counter and check if a second has passed
stilldisplay:

    inc count10ms
    mov a, count10ms
    cjne A, #100, updateInStateTime
    mov count10ms, #0
    
    mov a, seconds
 ;   cpl LEDRA.5
    add a, #1
    da a
    mov seconds, a
    cjne A, #60H, updateInStateTime
    mov seconds, #0

    mov a, minutes
    add a, #1
    da a
    mov minutes, a
    cjne A, #60H, updateInStateTime
    mov minutes, #0	
	
updateInStateTime:	
	
	inc inState10msCnt
    mov a, inState10msCnt
    cjne A, #100, ISR_Timer0_L0
    mov count10ms, #0

 	mov a, inStateSec
 ;   cpl LEDRA.5
    add a, #1
    da a
    mov inStateSec, a
    cjne A, #60H, ISR_Timer0_L0
    mov inStateSec, #0

    mov a, inStateMin
    add a, #1
    da a
    mov inStateMin, a
    cjne A, #60H, ISR_Timer0_L0
    mov inStateMin, #0		
	
	
skipIncrement:
    
ISR_Timer0_L0:

	; Restore used registers
	pop dpl
	pop dph
	pop acc
	pop psw    	
	
	reti
	
;ISR1: BUZZER + PWM______________________________________________________________________
	
ISR_timer1:
	push acc

	mov a, #1
	cjne a, ring, noring
	cpl P0.7
	mov TH1, #high(TIMER1_RELOAD)
	mov TL1, #low(TIMER1_RELOAD)

noring:
;--PWM
	mov a, #100
	cjne a, pwm, not100
_100:
	setb P0.6	;keep P0.6 high for 100% duty cycle
	mov pulseCount, #0 ;reset pulseCount if pwm changed before 20% cycle finished
	sjmp retFromISR
	
not100:	
	mov a, #0
	cjne a, pwm, _20
	
_0:
	clr P0.6	;keep P0.6 low for 0% duty cycle
	mov pulseCount, #0
	sjmp retFromISR
	
	
_20: 		
	;20% = (1 HIGH)/(4 LOW + 1 HIGH). High, Low, Low, Low, Low, High, ... 
	mov a, pulseCount
	cjne a, #0, setLow ;first pusle should be High, 2nd to 5th Low
setHigh:
	setb P0.6
	inc pulseCount
	sjmp retFromISR
setLow:
	clr P0.6		
	inc pulseCount
	mov a, pulseCount
	cjne a, #5, retFromISR
resetCycle:
	mov pulseCount, #0	;End of 1 period. Reset the cycle


retFromISR:
	pop acc
	reti
	

END
