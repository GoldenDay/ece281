    

	
Wait40us:
	mov R0, #149
	X1: 
	nop
	nop
	nop
	nop
	nop
	nop
	djnz R0, X1 ; 9 machine cycles-> 9*30ns*149=40us
    ret

LCD_command: ; display LCD
	mov	LCD_DATA, a
	clr	LCD_RS
	nop
	nop
	setb LCD_EN ; Enable pulse should be at least 230 ns
	nop
	nop
	nop
	nop
	nop
	nop
	clr	LCD_EN
	ljmp Wait40us

LCD_put:
	mov	LCD_DATA, A
	setb LCD_RS
	nop
	nop
	setb LCD_EN ; Enable pulse should be at least 230 ns
	nop
	nop
	nop
	nop
	nop
	nop
	clr	LCD_EN
	ljmp Wait40us
	    
    
LCD_DISPLAY_IDLE:
	lcall Wait40us
	djnz R1, LCD_DISPLAY_ALM_ON

	; Move to first column of first row	
	mov a, #80H
	lcall LCD_command
		
	; Display letter A
	mov a, #'I'
	lcall LCD_put
	
	mov a, #'d'
	lcall LCD_put
	
	mov a, #'l'
	lcall LCD_put
	
	mov a, #'e'
	lcall LCD_put
	
	ret
	
LCD_DISPLAY_RAMP_TO_SOAK:

	lcall Wait40us
	djnz R1, LCD_DISPLAY_ALM_OFF

	; Move to first column of first row	
	mov a, #80H
	lcall LCD_command
		
	; Display letter A
	mov a, #'R'
	lcall LCD_put
	
	mov a, #'a'
	lcall LCD_put
	
	mov a, #'m'
	lcall LCD_put
	
	mov a, #'p'
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #'t'
	lcall LCD_put
	
	mov a, #'o'
	lcall LCD_put
	
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #'S'
	lcall LCD_put
	
	mov a, #'o'
	lcall LCD_put
	
	mov a, #'a'
	lcall LCD_put
	
	mov a, #'k'
	lcall LCD_put
	;lcall Wait_Delay
	
LCD_DISPLAY_SOAK:

	lcall Wait40us
	djnz R1, LCD_DISPLAY_SNOOZE

	; Move to first column of first row	
	mov a, #80H
	lcall LCD_command
		
	; Display letter A
	mov a, #'S'
	lcall LCD_put
	
	mov a, #'o'
	lcall LCD_put
	
	mov a, #'a'
	lcall LCD_put
	
	mov a, #'k'
	lcall LCD_put
	
	ret
	
	
LCD_DISPLAY_RAMP_TO_PEAK:

	lcall Wait40us
	djnz R1, LCD_DISPLAY_TURNED_OFF

	; Move to first column of first row	
	mov a, #80H
	lcall LCD_command
		
	; Display letter A
	mov a, #'R'
	lcall LCD_put
	
	mov a, #'a'
	lcall LCD_put
	
	mov a, #'m'
	lcall LCD_put
	
	mov a, #'p'
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #'t'
	lcall LCD_put
	
	mov a, #'o'
	lcall LCD_put
	
	
	mov a, #'P'
	lcall LCD_put
	
	mov a, #'e'
	lcall LCD_put
	
	mov a, #'a'
	lcall LCD_put
	
	mov a, #'k'
	lcall LCD_put
	
	ret
	
LCD_DISPLAY_REFLOW:

	lcall Wait40us
	djnz R1, LCD_DISPLAY_TURNED_OFF

	; Move to first column of first row	
	mov a, #80H
	lcall LCD_command
		
	; Display letter A
	mov a, #'R'
	lcall LCD_put
	
	mov a, #'e'
	lcall LCD_put
	
	mov a, #'f'
	lcall LCD_put
	
	mov a, #'l'
	lcall LCD_put
	
	mov a, #'o'
	lcall LCD_put
	
	mov a, #'w'
	lcall LCD_put

	ret
	
LCD_DISPLAY_COOLING:

	lcall Wait40us
	djnz R1, LCD_DISPLAY_TURNED_OFF

	; Move to first column of first row	
	mov a, #80H
	lcall LCD_command
		
	; Display letter A
	mov a, #'C'
	lcall LCD_put
	
	mov a, #'o'
	lcall LCD_put
	
	mov a, #'o'
	lcall LCD_put
	
	mov a, #'l'
	lcall LCD_put
	
	mov a, #'i'
	lcall LCD_put
	
	mov a, #'n'
	lcall LCD_put
	
	mov a, #'g'
	lcall LCD_put
	
	ret
	
LCD_DISPLAY_CONTENTS_COOLED:

	lcall Wait40us
	djnz R1, LCD_DISPLAY_TURNED_OFF

	; Move to first column of first row	
	mov a, #80H
	lcall LCD_command
		
	; Display letter A
	mov a, #'C'
	lcall LCD_put
	
	mov a, #'o'
	lcall LCD_put
	
	mov a, #'n'
	lcall LCD_put
	
	mov a, #'t'
	lcall LCD_put
	
	mov a, #'e'
	lcall LCD_put
	
	mov a, #'n'
	lcall LCD_put
	
	mov a, #'t'
	lcall LCD_put
	
	
	mov a, #'s'
	lcall LCD_put
	
	mov a, #' '
	lcall LCD_put
	
	mov a, #'C'
	lcall LCD_put
	
	mov a, #'o'
	lcall LCD_put
	
	mov a, #'o'
	lcall LCD_put
	
	mov a, #'l'
	lcall LCD_put
	
	mov a, #'e'
	lcall LCD_put
	
	mov a, #'d'
	lcall LCD_put
	
	lcall Wait_HalfSec
	lcall Wait_delay
	
	ret