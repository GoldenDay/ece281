; TempSensor.asm: code from LAB3

; TO-DO: Error handling with negatives and zero values.
  
; TEMPERATURE SENSOR MAIN________________________________________________________

tempSensor:
	setb CE_ADC
	setb CE_EE
	clr  CE_RTC 				; RTC CE is active high
	lcall INIT_SPI
	mov b, #0  					; Read channel 0
	lcall Read_ADC_Channel	
	lcall InitSerialPort
	lcall convertTemp
	lcall hex2bcd
	lcall transmitSerial
	lcall display
	lcall waitonesec
jmp forever
  
  
    
; TEMPERATURE SENSOR FUNCTIONS___________________________________________________  

convertTemp:
    mov mf, #0
    mov r0, #0
    
    mov x+1, r7
    mov x+0, r6
    
    Load_y(62)
	lcall mul16
	mov R4, x+1

	mov x+1, R7
	mov x+0, R6

	Load_y(63)
	lcall mul16
	mov R5, x+1
	
	mov x+0, R4
	mov x+1, #0
	mov y+0, R5
	mov y+1, #0
	lcall add16
	
	Load_y(273)
	lcall sub16
	
	ret  

WaitHalfSec:
	mov R2, #90
L6:	mov R1, #250
L5:	mov R0, #250
L4:	djnz R0, L4 ; 3 machine cycles-> 3*30ns*250=22.5us
	djnz R1, L5 ; 22.5us*250=5.62ms
	djnz R2, L5 ; 5.62ms*90=0.5s (approximately)
	ret
	
WaitOneSec:
	mov R2, #180
L9:	mov R1, #250
L8:	mov R0, #250
L7:	djnz R0, L7 ; 3 machine cycles-> 3*30ns*250=22.5us
	djnz R1, L8 ; 22.5us*250=5.62ms
	djnz R2, L8 ; 5.62ms*90=0.5s (approximately)
	ret

Load_y MAC
	mov y+0, #low (%0) 
	mov y+1, #high(%0) 
ENDMAC

Display:
	mov dptr, #myLUT
	; Display Digit 0
    mov A, bcd+0
    anl a, #0fh
    movc A, @A+dptr
    mov HEX0, A
	; Display Digit 1
    mov A, bcd+0
    swap a
    anl a, #0fh
    movc A, @A+dptr
    mov HEX1, A
    ret

INIT_SPI:
    orl P0MOD, #00000110b 		; Set SCLK, MOSI as outputs
    anl P0MOD, #11111110b 		; Set MISO as input
    clr SCLK              		; For mode (0,0) SCLK is zero
	ret
	
DO_SPI_G:
	push acc
    mov R1, #0            		; Received byte stored in R1
    mov R2, #8            		; Loop counter (8-bits)
DO_SPI_G_LOOP:
    mov a, R0             		; Byte to write is in R0
    rlc a                 		; Carry flag has bit to write
    mov R0, a
    mov MOSI, c
    setb SCLK            	 	; Transmit
    mov c, MISO           		; Read received bit
    mov a, R1             		; Save received bit in R1
    rlc a
    mov R1, a
    clr SCLK
    djnz R2, DO_SPI_G_LOOP
    pop acc
    ret

Delay:
	mov R3, #20
Delay_loop:
	djnz R3, Delay_loop
	ret

; Configure the serial port and baud rate using timer 2
InitSerialPort:
	clr TR2 ; Disable timer 2
	mov T2CON, #30H ; RCLK=1, TCLK=1 
	mov RCAP2H, #high(T2LOAD)  
	mov RCAP2L, #low(T2LOAD)
	setb TR2 ; Enable timer 2
	mov SCON, #52H
	ret

; Send a character through the serial port
putchar:
    JNB TI, putchar
    CLR TI
    MOV SBUF, a
    RET

; Send a constant-zero-terminated string through the serial port
transmitSerial:
    clr A 
    
    mov dptr, #Numbers
    
	; Display Digit 1
    mov A, bcd+0
    swap a
    anl a, #0fh
    orl a, #30h
    lcall putchar

	; Display Digit 0
    mov A, bcd+0
    anl a, #0fh
    orl a, #30h
    lcall putchar

	mov a, #0
	movc a, @a+dptr
	lcall putchar
	
    ret
    
; Channel to read passed in register b
Read_ADC_Channel:
	clr CE_ADC
	mov R0, #00000001B 			; Start bit:1
	lcall DO_SPI_G
	
	mov a, b
	swap a
	anl a, #0F0H
	setb acc.7 					; Single mode (bit 7).
	
	mov R0, a 					; Select channel
	lcall DO_SPI_G
	mov a, R1          			; R1 contains bits 8 and 9
	anl a, #03H
	mov R7, a
	
	mov R0, #55H 				; It doesn't matter what we transmit...
	lcall DO_SPI_G
	mov a, R1    				; R1 contains bits 0 to 7
	mov R6, a
	setb CE_ADC
	ret
 
Numbers:
	DB  '\n'
	DB	'\r'
    DB  '.' 
    DB  '2 ','\n', '\r' 
    DB  '3 ','\n', '\r' 
    DB  '4 ','\n', '\r' 
    DB  '5 ','\n', '\r' 
    DB  '6 ','\n', '\r' 
    DB  '7 ','\n', '\r' 
    DB  '8 ','\n', '\r' 
    DB  '9 ','\n', '\r'

END
